<?

class Users extends DB {

	var $table = 'users';
	
	public function save( $fields ) {
		
		// verify the email address
		if( !$fields['id'] ) {
			
			if( $this->emailInUse( $fields['email'] ) ) {
				
				$this->error = "This email is already taken.";
				
				return false;
				
			}
			
		}
		
		unset( $fields['password_confirm'] );
			
		if( $fields['password'] ) {
			
			$fields['password'] = md5( $fields['password'] );
			
		} else {
			
			unset( $$fields['password'] );
			
		}
		
		return parent::save( $fields );
		
	}
	
	public function reset_password( $email ) {
		
		$user = $this->retrieve('one','*'," where email='" . $email . "'");
		
		if( $user['id'] ) {
			
			$new_password = AppFunctions::generateRandomPassword();
			
			if( $this->save( array( 'id' => $user['id'], 'password' => $new_password ) ) ) {
				
				$message = "Your password has been reset to: " . $new_password;
			
				AppFunctions::send_mail( $email, $message, "Your Password Has Been Reset", 'no-reply@thereferralpad.com', 'The Referral Pad' );
				 
				return true;
				
			}
			
		}
		
		return false;
		
	}
	
	public function emailInUse( $email ) {
		
		$user = $this->retrieve('one','*'," where email='" . $email . "'");
		
		if( $user['id'] ) {
			
			return true;
			
		}
		
		return false;
		
	}
	
}

?>