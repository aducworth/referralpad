<?

class OfficeLocations extends DB {

	var $table = 'office_locations';
	
	public function getLocationsTooltip( $office_id ) {
		
		$locations = $this->retrieve( 'pair','id, name',' where office_id = ' . $office_id );
		
		return implode( "<br>", $locations );
		
	}
	
}

?>