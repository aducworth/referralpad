<?

class Comments extends DB {

	var $table = 'comments';
	
	function __construct() {
	
		$this->doctors = new Doctors;
		$this->offices = new Offices;
		$this->referrals = new Referrals;
		$this->attachments = new Attachments;
		$this->referral_views = new ReferralViews;
	
		parent::__construct();
	
	}
	
	public function save( $fields ) {
	
		$toreturn = parent::save( $fields );
		
		if( !$fields['id'] ) {
			
			$fields['id'] = mysql_insert_id();
			
		}
		
		// get referral info so we can find out who to email
		$referral_info = $this->referrals->retrieve( 'one','*',' where id = ' . $fields['referral_id'] );
		
		// upload any attachments
		$this->attachments->uploadFiles( $fields['referral_id'], $fields['doctor_id'], $fields['id'] );
		
		// go ahead and save the referral whenever a comment is saved so the modified timestamp is updated		
		$this->referrals->save( array( 'id' => $fields['referral_id'], 'task_status' => 'comment' ) );
		
		// mark as read so this user doesn't see this in unread tasks if this is a read task
		// otherwise keep it as unread, it's confusing
		if( !$this->referral_views->isUnread( $referral_info ) ) {
			
			$this->referral_views->updateView( $fields['referral_id'] );
			
		}
		
		// protocal
		$protocal = ( $_SERVER['HTTPS'] != 'on' )?'http://':'https://';
		
		if( $fields['doctor_id'] == $referral_info['referred_by'] ) {
			
			$doctor_info = $this->doctors->retrieve( 'one','*',' where id = ' . $referral_info['referred_to'] );
			
		} else {
			
			$doctor_info = $this->doctors->retrieve( 'one','*',' where id = ' . $referral_info['referred_by'] );
			
		}
		
		// get office info
		$office_info = $this->offices->retrieve('one','*',' where id = ' . $doctor_info['office_id']);
		
		// send an email to anyone copied on the list
		$body = file_get_contents( $protocal . $_SERVER['SERVER_NAME'] . '/referral_email?ajax=true&id=' . $fields['referral_id'] . '&comment=' . $fields['id'] );
						
		AppFunctions::send_mail( $office_info['email'], $body, 'New Comment via The Referral Pad', 'no-reply@thereferralpad.com', 'The Referral Pad' );
		
		return $toreturn;
		
	}
	
}

?>