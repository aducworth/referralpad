<?

class ReferralViews extends DB {

	var $table = 'referral_views';
	
	function __construct() {
		
		parent::__construct();
		
		if( isset( $_SESSION['logged_in_user']['id'] ) ) {
			
			// go ahead and load last views
			$this->last_viewed = $this->retrieve('pair','referral_id, modified',' where user_id = ' . $_SESSION['logged_in_user']['id']);
			
		}
	
	}
	
	public function updateView( $referral ) {
	
		if( isset( $_SESSION['logged_in_user']['id'] ) ) {
			
			// see if there is a current timestamp
			$current = $this->retrieve('one','*',' where referral_id = ' . $referral . ' and user_id = ' . $_SESSION['logged_in_user']['id']);
			
			// if this isn't already set, load up the information for it
			if( !$current['id'] ) {
				
				$current = array( 'referral_id' => $referral, 'user_id' => $_SESSION['logged_in_user']['id'] );
				
			}
			
			// eitherway, unset this because we're updating it
			unset( $current['modified'] );
			
			return $this->save( $current );
			
		}
		
		return false;
		
	}
	
	public function isUnread( $referral_info ) {
	
		// if there is no record, it's obviously unread or if the last viewed timestamp is older than the last modified
		if( !isset( $this->last_viewed[ $referral_info['id'] ] ) || ( strtotime( $this->last_viewed[ $referral_info['id'] ] ) < strtotime( $referral_info['modified'] ) ) ) {
			
			return true;
			
		} 
		
		return false;
	
	}
	
}