<?

class Invitations extends DB {

	var $table = 'invitations';
	
	public function save( $fields ) {
		
		$fields['from_user'] = $_SESSION['logged_in_user']['id'];
	
		if( $_SESSION['logged_in_user']['office_id'] ) {
			
			$fields['from_office'] = $_SESSION['logged_in_user']['office_id'];
			
			$offices = new Offices;
			
			$office_info = $offices->retrieve('one','*',' where id = ' . $fields['from_office']);
			
		}
		
		$body = "You are invited to send and receive referrals with The Referral Pad. Click <a href='http://www.thereferralpad.com/signup'>here</a> to check it out.";
		
		if( $fields['message'] ) {
			
			$body .= '<br><br>"' . $fields['message'] . '"';
			
		}
		
		// send the inviation email
		AppFunctions::send_mail( $fields['email'], $body, ('Invitation from ' . ($office_info['name']?$office_info['name']:'Referral Pad')), 'no-reply@thereferralpad.com', 'The Referral Pad' );
		
		// save everything
		$toreturn = parent::save( $fields );
		
		return $toreturn;
		
	}
	
}

?>