<?

class Referrals extends DB {

	var $table = 'referrals';
	
	function __construct() {
	
		$this->doctors = new Doctors;
		$this->offices = new Offices;
		$this->attachments = new Attachments;
		$this->referral_views = new ReferralViews;
	
		parent::__construct();
	
	}
	
	public function retrieve( $scope = 'all', $fields = '*', $conditions = '' ) {
		
		// injecting conditions for soft delete
		$new_where = "where ( deleted not like '%|" . $_SESSION['logged_in_user']['id'] . "|%' or deleted is null )";
		
		if( strstr( $conditions, 'where' ) ) {
			
			$conditions = str_replace( 'where',  ( $new_where . " and " ), $conditions );
			
		} else {
			
			$conditions = ( $new_where . $conditions );
			
		}
		
		$data = parent::retrieve( $scope, $fields, $conditions );
				
		return $data;
		
	}
	
	public function remove( $id ) {
		
		$referral = $this->retrieve('one','*'," where id = " . $id);
		
		$delete_info = array_filter( explode( '|', $referral['deleted'] ) );
		
		$delete_info[] = $_SESSION['logged_in_user']['id'];
		
		$referral['deleted'] = ( '|' . implode( '|', $delete_info ) . '|' );
		
		if( $this->save( $referral ) ) {
			
			return true;
		}
	
		return true;
	
	}
	
	public function getUnread( $where ) {
	
		$toreturn = array();
		
		$results = $this->retrieve('all','*',$where . ' order by modified desc');
		
		foreach( $results as $r ) {
			
			if( $this->referral_views->isUnread( $r ) ) {
				
				$toreturn[] = $r;
				
			}
			
		}
		
		//echo( $where );
		
		// set this number in session so we can get it in the menu
		$_SESSION['unread_count'] = count( $toreturn );
		//$_SESSION['unread_count'] = 100;
		
		return $toreturn;
		
	}
	
	public function save( $fields ) {
	
		$action = $fields['submit'];
		unset( $fields['submit'] );
	
		$new_referral = false;
		
		// get current info if there to compare with new info
		if( $fields['id'] ) {
			
			$current = $this->retrieve('one','*',' where id = ' . $fields['id']);
			
		} else {
			
			$fields['patient_id'] = $this->generatePatientID( $fields['patient_name'] );
			
		}
	
		// only save appointment if it's there
		if( $fields['appointment_date'] && $fields['appointment_hour'] && $fields['appointment_minute'] ) {
			
			$fields['appointment'] = date( 'Y-m-d H:i:s', strtotime( $fields['appointment_date'] . ' ' . $fields['appointment_hour'] . ':' .  $fields['appointment_minute'] ) );
			
		}
		
		// if this is completed, set the flag
		if( $action == 'Completed' ) {
		
			$fields['completed'] = 1;
			
		}	
		
		// unset appointment fields
		unset( $fields['appointment_date'] );
		unset( $fields['appointment_hour'] );
		unset( $fields['appointment_minute'] );
		
//		// only save dob if it's there
//		if( $fields['dob_month'] && $fields['dob_day'] && $fields['dob_year'] ) {
//			
//			$dob = $fields['dob_month'] . '/' . $fields['dob_day'] . '/' . $fields['dob_year'];
//			
//			$fields['patient_dob'] = date( 'Y-m-d', strtotime( $dob ) );
//			
//		}	
//		
//		// unset dob fields
//		unset( $fields['dob_month'] );
//		unset( $fields['dob_day'] );
//		unset( $fields['dob_year'] );
		
		// reset dob to one field
		if( $fields['patient_dob'] ) {
			
			$fields['patient_dob'] = date( 'Y-m-d', strtotime( str_replace( ' ', '', $fields['patient_dob'] ) ) );
			
		}
		
		if( is_array( $fields['referral_questions'] ) ) {
			
			$tosave = array();
			
			foreach( $fields['referral_questions'] as $q ) {
				
				if( $q != 0 ) {
					
					$tosave[] = $q;
					
				}
				
			}
			
			$fields['referral_questions'] = ( count( $tosave ) > 0 )?implode( ',', $tosave ):'';
			
			
		}
		
		$toreturn = parent::save( $fields );
		
		if( !$fields['id'] ) {
			
			$fields['id'] = mysql_insert_id();
			
			$new_referral = true;
			
		}
		
		// any time completed is clicked, the view is updated
		if( $action == 'Completed' ) {
			
			$this->referral_views->updateView( $fields['id'] );
			
		}
		
		// take care of any attachments
		if( $fields['referred_by'] ) {
			
			$this->attachments->uploadFiles( $fields['id'], $fields['referred_by'] );
			
		}
		
		// protocal
		$protocal = ( $_SERVER['HTTPS'] != 'on' )?'http://':'https://';
		
		// make sure these fields are there because they are not on comments
		if( $fields['referred_by'] && $fields['referred_to'] ) {
			
			$dr_from = $this->doctors->retrieve('one','*',' where id = ' . $fields['referred_by']);
			$dr_to = $this->doctors->retrieve('one','*',' where id = ' . $fields['referred_to']);
			
			$dr_to_office = $this->offices->retrieve('one','*',' where id = ' . $dr_to['office_id']);
			
		}
				
		// if this is a new one, send the new referral email
		if( $new_referral ) {
			
			//$body = file_get_contents( $protocal . $_SERVER['SERVER_NAME'] . '/referral_email?ajax=true&id=' . $fields['id'] );
						
			//AppFunctions::send_mail( $dr_to_office['email'], $body, ( 'Referral from Dr. ' . $dr_from['fname'] . ' ' . $dr_from['lname'] . ', ' . $dr_from['credentials'] . ' via The Referral Pad'), 'no-reply@thereferralpad.com', 'The Referral Pad' );
						
		} else {
			
			// see if an appointment has been scheduled
//			if( $fields['appointment'] ) {
//				
//				if( !$current['appointment'] ) {
//					
//					$subject = 'Appointment Scheduled via Referral Pad';
//					
//				} else {
//					
//					$subject = 'Appointment Rescheduled via Referral Pad';
//					
//				}
//				
//				$body = file_get_contents( $protocal . $_SERVER['SERVER_NAME'] . '/referral_email?ajax=true&id=' . $fields['id'] );
//						
//				AppFunctions::send_mail( $dr_to['email'], $body, $subject, 'no-reply@thereferralpad.com', 'The Referral Pad' );
//			
//			}
			
		}
		
		// send the patient email any time Completed is clicked
		if( $action == 'Completed' ) {
		
			$body = file_get_contents( $protocal . $_SERVER['SERVER_NAME'] . '/referral_email?ajax=true&id=' . $fields['id'] );
			  		
			AppFunctions::send_mail( $dr_to_office['email'], $body, ( 'Referral from Dr. ' . $dr_from['fname'] . ' ' . $dr_from['lname'] . ', ' . $dr_from['credentials'] . ' via The Referral Pad'), 'no-reply@thereferralpad.com', 'The Referral Pad' );
		
			$body = file_get_contents( $protocal . $_SERVER['SERVER_NAME'] . '/email_patient?ajax=true&id=' . $fields['id'] . '&patient_email=true' );
						
			AppFunctions::send_mail( ( $fields['patient_email'] ), $body, ( 'Referred to Dr. ' . $dr_to['fname'] . ' ' . $dr_to['lname'] . ', ' . $dr_to['credentials'] ), 'no-reply@thereferralpad.com', 'The Referral Pad' );
		
		}	
		
		return $toreturn;
		
	}
	
	public function generatePatientID( $name ) {
		
		$name_parts = explode( ' ', $name );
		
		$first 	= strtoupper( $name_parts[0] );
		$last 	= strtoupper( $name_parts[ count( $name_parts ) - 1 ] );
		$initials = substr( $first, 0, 1 ) . substr( $last, 0, 1 );
		
		$similar_referrals = $this->retrieve('all','id'," where patient_id like '" . $initials . "%' order by id desc");
		$sequence = (count( $similar_referrals )?count( $similar_referrals ):0) + 1;
		
		return  $initials . str_pad($sequence, 8, '0', STR_PAD_LEFT);	
		
	}
	
	public function referralsOverTime() {
		
		$toreturn = array();
		
		$first = $this->retrieve('one','*',' order by created asc');
		$last = $this->retrieve('one','*',' order by created desc');
		$all = $this->retrieve('all','*',' order by created asc');
		
		// make sure there is some data
		if( !$first['id'] || !$last['id'] ) {
			
			return array();
			
		}
		
		$dr_types = new DoctorTypes;
		$type_list = $dr_types->retrieve('pair','id,name','order by name');
		
		$doctors = new Doctors;
		$doctor_types = $doctors->retrieve('pair','id, doctor_type_id');
		
		$starting_time = strtotime( $first['created'] );
		$ending_time = strtotime( $last['created'] );
		
		// initialize the data
		while( $starting_time < $ending_time ) {
		
			$month_year = date( 'F Y', $starting_time );
			
			foreach( $type_list as $tl ) {
				
				$toreturn[ $month_year ][ $tl ] = 0;
				
			}		
 			
			$starting_time = strtotime( '+1 month', $starting_time );
			
		}
		
		foreach( $all as $referral ) {
		
			$dr_type = $type_list[ $doctor_types[ $referral['referred_to'] ] ];
				
			$toreturn[ date( 'F Y', strtotime( $referral['created'] ) ) ][ $dr_type ]++;
			
		}
		
		return $toreturn;
		
	}
	
	
}

?>