<?

class Attachments extends DB {

	var $table = 'attachments';
	
	var $valid_upload_types = array( 'image/png', 'image/jpeg', 'application/pdf' );
	
	var $resizable = array( 'image/png', 'image/jpeg' );
	
	public function  uploadFiles( $referral, $doctor = 0, $comment = 0 ) {
		
		if( is_array( $_FILES['attachments']['name'] ) ) {
			
			foreach( $_FILES['attachments']['name'] as $i => $name ) {
				
				if( is_uploaded_file( $_FILES['attachments']['tmp_name'][$i] ) ) {
				
					$data = array();
				
					if( in_array( $_FILES['attachments']['type'][$i], $this->valid_upload_types ) ) {
						
						$data['name'] = $name;
						$data['filename'] = time() . $this->clean_filename( $name );
						$data['referral_id'] = $referral;
						$data['comment_id'] = $comment;
						$data['doctor_id'] = $doctor;
						$data['user_id'] = $_SESSION['logged_in_user']['id'];
						
						$path = str_replace( array( 'admin', 'app' ), array( '', 'assets' ), getcwd() ) . '/assets/images/uploads/';
						
						move_uploaded_file( $_FILES['attachments']['tmp_name'][$i], $path . 'tmp/' . $data['filename'] ); 
						
						if( in_array( $_FILES['attachments']['type'][$i], $this->resizable ) ) {
						
							$resizeObj = new resize( $path . 'tmp/' . $data['filename'] );
							$resizeObj -> resizeImage( 300, 473, 'landscape' );
							$resizeObj -> saveImage( $path . 'resized/' . $data['filename'], 100 );
											
							$resizeObj = new resize( $path . 'tmp/' . $data['filename'] );
							$resizeObj -> resizeImage( 100, 100, 'crop' );
							$resizeObj -> saveImage( $path . 'thumbnails/' . $data['filename'], 100 );
						
						}
						
						$this->save( $data );
					
					} 
				
				}
			}	
		}

	}
	
	public function listAttachments( $files ) {
	
		$toreturn = '';
		
		if( is_array( $files ) ) {
		
			$toreturn = '<div class="media">';
			
			foreach( $files as $file ) {
			
				//$toreturn .= "<a href='#file-id-" . $file['id'] . "' class='pull-left attachment' data-toggle='modal'><img src='/assets/images/uploads/thumbnails/" . $file['filename'] . "' class='media-object'><span class='remove-attachment icon-trash icon-white'></span></a><div id='file-id-" . $file['id'] . "' class='modal hide fade' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><img src='/assets/images/uploads/tmp/" . $file['filename'] . "'></div>";
				
				if( strstr( $file['filename'], '.pdf' ) ) {
					
					$thumbnail = '/assets/images/pdf-icon.png';
					
				} else {
				
					$thumbnail = '/assets/images/uploads/thumbnails/' . $file['filename'];
					
				}				
				
				$toreturn .= "<a href='/assets/images/uploads/tmp/" . $file['filename'] . "' target='_blank' class='pull-left attachment' data-toggle='modal'><img src='" . $thumbnail . "' class='media-object'></a><div id='file-id-" . $file['id'] . "' class='modal hide fade' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><img src='/assets/images/uploads/tmp/" . $file['filename'] . "'></div>";
				
			}
			
			$toreturn .= '</div>';
		
		}
		
		return $toreturn;
			  	
	}
	
	public function clean_filename( $filename ) {
	
		return preg_replace( "/[^\w\.-]/", "-", strtolower( $filename ) );
		
	}
	
}

?>