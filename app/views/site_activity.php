<h1>Site Activity</h1>

<form class="form-inline" role="form" action='/site_activity'>
	
	<div class="form-group has-feedback">                
	      	<input id="from-date" name="from_date" class="form-control datepicker" type="text" placeholder="From" value="<?=$_GET['from_date'] ?>">
	      	<i class="glyphicon glyphicon-calendar form-control-feedback" style='top: 0;'></i>
	  </div>
	  
	  <div class="form-group has-feedback">                
	      	<input id="to-date" name="to_date" class="form-control datepicker" type="text" placeholder="To" value="<?=$_GET['to_date'] ?>">
	      	<i class="glyphicon glyphicon-calendar form-control-feedback" style='top: 0;'></i>
	  </div>
	  
	  <button class="btn btn-info">Submit</button>

                    
</form>

<? if( count( $controller->referrals ) > 0 ): ?>

	<?
		
		$to = array();
		$from = array();
		
	?>

	<h2><?=count( $controller->referrals ) ?> Referrals</h2>
	
	<div class='row'>

		<div class='col-md-3'><h3>Date</h3></div>
		<div class='col-md-3'><h3>Patient</h3></div>
		<div class='col-md-3'><h3>Referred By</h3></div>
		<div class='col-md-3'><h3>Referred To</h3></div>
	
	</div>
	
	<? foreach( $controller->referrals as $r ): ?>
	
		<?
			$from[ $r['referred_by'] ] = $from[ $r['referred_by'] ]?( $from[ $r['referred_by'] ] + 1 ):1;
			$to[ $r['referred_to'] ] = $to[ $r['referred_to'] ]?( $to[ $r['referred_to'] ] + 1 ):1;
			
		?>

		<div class='row'>

			<div class='col-md-3'><?=date( 'm/d/Y g:ia', strtotime( $r['created'] ) ) ?></div>
			<div class='col-md-3'><?=$r['patient_name'] ?></div>
			<div class='col-md-3'><?=$controller->doctor_list[ $r['referred_by'] ] ?></div>
			<div class='col-md-3'><?=$controller->doctor_list[ $r['referred_to'] ] ?></div>
		
		</div>
	
	<? endforeach; ?>
	
	<? 
		arsort( $to );
		arsort( $from );
		
	?>
	
	<div class='row'>
		
		<div class='col-md-6'>
			
			<h2>Referrals By</h2>
			
			<div class="table-responsive">

			<table class='table table-hover'>
			
				<? foreach( $from as $doctor => $count ): ?>
				
					<tr class='table-striped'>
						<td><?=$controller->doctor_list[ $doctor ] ?></td>
						<td><?=$count ?></td>
					</tr>
				
				<? endforeach; ?>
			
			</table>
			
			</div>
			
		</div>
		
		<div class='col-md-6'>
			
			<h2>Referrals To</h2>
			
			<div class="table-responsive">

			<table class='table table-hover'>
			
				<? foreach( $to as $doctor => $count ): ?>
				
					<tr class='table-striped'>
						<td><?=$controller->doctor_list[ $doctor ] ?></td>
						<td><?=$count ?></td>
					</tr>
				
				<? endforeach; ?>
			
			</table>
			
			</div>
			
		</div>
			
	</div>
	
	

<? else: ?>

	<p>No referrals over this time period.</p>

<? endif; ?>