<h1>Invite 
	
	<? if( $controller->doctor['id'] ): ?>
	
		<?=$controller->doctor['fname'] ?> <?=$controller->doctor['lname'] ?>
		
		<? $label = $controller->doctor['fname']; ?>
		
	<? else: ?>
	
		<? if( $_POST['id'] ): ?>
		
			Another Doctor
		
		<? endif; ?>
	
		<? $label = "Doctor"; ?>
	
	<? endif; ?> 
	
</h1>

<form action='/invite' id='data-form' method='post' enctype="multipart/form-data">

<? if( $_GET['doctor'] ): ?>

	<input type='hidden' id='doctor' name='doctor' value='<?=$_GET['doctor'] ?>'/>

<? endif; ?>

<?=$form->textbox( 'email', array( 'label' => ($label . "'s Email"), 'default' => ($controller->office['email']?$controller->office['email']:''), 'class' => 'required', 'form_group' => true ) ) ?>

<?=$form->textarea( 'message', array( 'label' => 'Personal Message', 'default' => $_POST['message'], 'class' => '', 'form_group' => true ) ) ?>

<p class='action-buttons'>

	<input type='submit' class="btn btn-info pull-left" value='Send'/>

</p>

</form>