<h1><?=$_GET['id']?'Edit':'Add' ?> Patient</h1>

<form action='/patient' id='data-form' method='post' enctype="multipart/form-data">

<? if( $_GET['id'] ): ?>

	<input type='hidden' id='id' name='id' value='<?=$_GET['id'] ?>'/>

<? endif; ?>

<? if( $controller->result['image'] ): ?>
    
	<img src="<?=$controller->site_url ?>/images/uploads/thumbnails/<?=$controller->result['image'] ?>" class="pull-right" style='border-radius: 10px;'/>
    
<? endif; ?>


<?=$form->textbox( 'fname', array( 'label' => 'First Name', 'default' => $controller->result['fname'], 'class' => 'required' ) ) ?>
<?=$form->textbox( 'lname', array( 'label' => 'Last Name', 'default' => $controller->result['lname'], 'class' => 'required' ) ) ?>

<p><label>Image ( Please restrict photos to jpgs. The image will automatically be resized. ):</label><input type='file' name='image' class='<?=$controller->result['image']?'':'required' ?>'/></p>

<?=$form->textbox( 'email', array( 'label' => 'Email', 'default' => $controller->result['email'], 'class' => 'required' ) ) ?>

<?=$form->textbox( 'phone', array( 'label' => 'Phone', 'default' => $controller->result['phone'], 'class' => 'required' ) ) ?>

<p class='action-buttons'>

	<input type='submit' class="btn pull-left" value='Save'/>

	<? if( $_GET['id'] ): ?>

		<button id='delete-patient' class="btn btn-danger pull-right delete-item">Delete</button>
        
    <? else: ?>
    
    	<button id='cancel-patient' class="btn btn-danger pull-right cancel-item">Cancel</button>
        
    <? endif; ?>

</p>

</form>