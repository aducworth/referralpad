	
	<div class='row' style='margin-top: 100px;'>
	
		<div class='col-md-12'>
		
			&copy; The Referral Pad <?=date('Y') ?>
			
		</div>
	
	</div>
	
	</div><!--content--> 
				
    <script type="text/javascript">
		$(document).ready(function(){
		
			$('.datepicker').datepicker();
			
			$('[data-toggle="tooltip"]').tooltip({html:true});
			
			$("#data-form").validate({
			  rules: {
				password_confirm: {
				  equalTo: "#password-id"
				},
				checkbox: { 
					required: 'input[type="checkbox"]:checked'
				}
			  },
			  submitHandler: function(form) {
			  	$('input[type=submit]').prop('disabled','true');
			    form.submit();
			  }
			});
			
			$('#find-npi').on('click', function (e) {
			
				e.preventDefault();
				
			    var $btn = $(this).button('loading');
			    
			    var postData = 'npi=' + $('#npi-id').val();
			    
			    // remove any previous messages
			    $('#npi-search-results').html('');
			    
			    $('.signup-info').remove();
			    
			    $.ajax({
			      url: '/_search_npi',
			      data: postData,
			      type: "POST"
			    }).done(function(data) {
			      //alert( data );
			      
			      if( data == 'false' ) {
				      
				      $('#npi-search-results').html('<div class="alert alert-danger" role="alert">Sorry, we could not verify your npi. Please try again.</div>');
				      
			      } else {
				      
				      if( data == 'local' ) {
					      
					      $('#npi-search-results').html('<div class="alert alert-info" role="alert">Is this your office?</div>');
					      
				      } else {
					      
					      $('#npi-search-results').html('<div class="alert alert-info" role="alert">Good news! We have located your npi information. Please verify the rest of this information, and your account will be set.</div>');
					      
				      }

				      $.ajax({
				        url: '/_signup_info?ajax=true'			   
				      }).done(function(data) {
				        $('#signup-process').append( data ).find('.signup-info').fadeIn('slow');
				      });
				      
			      }
			      
			      
			      $btn.button('reset');
			    });
			    
			    
			  });
			
			
			$('#verify-npi').on('click', function (e) {
			
				e.preventDefault();
				
			    var $btn = $(this).button('loading');
			    
			    var postData = 'npi=' + $('#npi-id').val();
			    
			    // remove any previous messages
			    $('#npi-search-results').html('');
			    
			    $('.signup-info').remove();
			    
			    $.ajax({
			      url: '/_search_npi',
			      data: postData,
			      type: "POST"
			    }).done(function(data) {
			      //alert( data );
			      
			      if( data == 'false' ) {
				      
				      $('#npi-search-results').html('<div class="alert alert-danger" role="alert">Sorry, we could not verify your npi. Please try again.</div>');
				      
			      } else {
				      
				      if( data == 'local' ) {
					      
					      $('#npi-search-results').html('<div class="alert alert-info" role="alert">Good news! We already have your account loaded into The Referral Pad. You will just need to confirm your information, and you will be ready to go.</div>');
					      
				      } else {
					      
					      $('#npi-search-results').html('<div class="alert alert-info" role="alert">Good news! We have located your npi information. Please verify the rest of this information, and your account will be set.</div>');
					      
				      }

				      $.ajax({
				        url: '/_signup_info?ajax=true'			   
				      }).done(function(data) {
				        $('#signup-process').append( data ).find('.signup-info').fadeIn('slow');
				      });
				      
			      }
			      
			      
			      $btn.button('reset');
			    });
			    
			    
			  });
			
			$('.glyphicon-calendar').click(function(e){
			
				$(this).parent().find('input').focus();
				
			});
			
			$('.multi-selector').click(function(e){
			
				e.preventDefault();
			
				var id 			= $(this).attr('id');
				var value 		= $(this).attr('data-value');
				var class_value = id.replace( ('-'+value), '' );
				var hidden_id 	= id.replace( ('-'+value), '' ).replace( 'multi-select-', 'multi-select-value-' );
				
				//alert( value );
				
				if( $(this).hasClass( 'selected' ) ) {
				
					$('#'+hidden_id).val( '' );
					$(this).removeClass( 'selected' ); 
					
				} else {
					
					$('.'+class_value).removeClass( 'selected' );
					$('#'+hidden_id).val( value );
					$(this).addClass( 'selected' );
					
				}
				//alert( $('#'+hidden_id).attr('id') );
			
			});
			
			$('.check-read-task').click(function(e) {
			
				var task_count = $('#task-count').text();
				var refferal_id = $(this).attr('data-value');
				var this_element = $(this);
				var postData = 'id=' + refferal_id;
				
				$.ajax({
				  url: '/_mark_as_read?ajax=true',
				  data: postData,
				  type: "POST"
				}).done(function(data) {
				  		var ajax_result = jQuery.parseJSON( data );
				  
					  if( ( task_count - 1 ) > 0 ) {
						
						 $('#task-count').text( task_count - 1 );
						
						} else {
							
							$('#task-count').hide();
							
						}
						
						 this_element.parent().parent().addClass('hidden');
						 this_element.parent().parent().hide();
						 
						 if( ( $('tr').size() - $('tr.hidden').size() ) <= 1 ) {
							
							$('table').hide();
							$('<p>You are all caught up!</p>').insertAfter('h1');
							
						}
					
				});
				
				
				
				//alert( $(this).attr('data-value') );
				
			});
			
			$('.remove-attachment').click(function(e){
			
				e.preventDefault();
				
				$(this).parent().remove();
			
			});
			
			$('.action-search').change(function() {
			
				$(this).parent().parent('form').submit();
				
			});
			
			$('.doctor-picker a').click(function(e) {
				
				e.preventDefault();
				
				//alert( $(this).attr('data-attr') );
				$(this).parent().parent().find('.selected').removeClass('selected');
				$(this).parent().addClass('selected');
				$(this).parent().parent().find('.doctor-picker-value').val( $(this).attr('data-attr') );
				
				alert( to_update.val() );
				
			});
			
			$('#title-id').change(function() {
				
		      var name = $(this).val();
			  
			  var formatted_name = name.replace(/\./g,'').replace(/,/g,'').replace(/,/g,'-').replace(/"/g,'').replace(/'/g,'').replace(/ /g,'-').replace(/&/g,'and').replace(/!/g,'').replace(/\$/g,'').replace(/\*/g,'').replace(/\//g,'').replace(/@/g,'').replace(/\(/g,'').replace(/\)/g,'').replace(/\[/g,'').replace(/\]/g,'').replace(/%/g,'').replace(/#/g,'');
			  			  
			  if( $('#url-id').val() == '' ) {
				  
				  $('#url-id').val( formatted_name.toLowerCase() );
				  
			  }
			  
			});
			
			$('#doctor_type_id-id').change(function(){
			
				$('.questions-by-doctor-type').hide();
				
				if( $(this).val() ) {
					
					$('#doctor-type-'+$(this).val()).show();
					
				}
				
			});
			
			$('.update-tag').change(function() {
				
			  if( $('#fname-id').val() == '' || $('#lname-id').val() == '' ) {
				  
				  return;
			  }
			  
		      var name = $('#fname-id').val() + '-' + $('#lname-id').val();
			  
			  var formatted_name = name.replace(/\./g,'').replace(/,/g,'').replace(/,/g,'-').replace(/"/g,'').replace(/'/g,'').replace(/ /g,'-').replace(/&/g,'and').replace(/!/g,'').replace(/\$/g,'').replace(/\*/g,'').replace(/\//g,'').replace(/@/g,'').replace(/\(/g,'').replace(/\)/g,'').replace(/\[/g,'').replace(/\]/g,'').replace(/%/g,'').replace(/#/g,'');
			  
			  if( $('#url-id').val() == '' ) {
				  
				  $('#url-id').val( formatted_name.toLowerCase() );
				  
			  }
			  
			});
			
			$('.update-display-order').change(function() {
				
				$.ajax({
					url: '/update_display_order?update_id=' + $(this).attr('id') + '&order=' + $(this).val(),
					context: document.body
				}).done(function() {
					$('.alert').remove();
					$('#main-content').prepend('<div class="alert alert-success">Order Updated</div> ');
				});
			  			  
			});
			
			$('.photo-selector').click(function() {
				
				$('.photo-selector').removeClass('selected-photo');
				$(this).addClass('selected-photo');
				
				$('#photo').val( $(this).attr('id') );
			  			  
			});
			
			$('.add-to-list').click(function(e) {
				
				e.preventDefault();
				
				$( '#' + $(this).attr('data-item') ).toggle('slow');
							  			  
			});
			
			$('.add-list-item').click(function(e) {
				
				e.preventDefault();
				
				var list_area = $(this).parent().parent().attr('id');
				var select_item = list_area.replace('add-list','id');
				
				var url = 'ajax=true'
				
				$( '#' + list_area + ' input' ).each(function( index ) {
				  url += '&' + $(this).attr('id') + '=' + $(this).val();
				});
				
				$.ajax({
					url: '/add_to_list',
					data: url,
					method: 'GET',
					success: function(data) { 
						var result = $.parseJSON(data);
						
						if( result.error ) {
							
							$( '#' + list_area ).prepend( "<div class='alert alert-error'>" + result.error + "</div>" );
							
						} else {
							
							$( '#' + select_item ).html( result.data );
							$( '#' + list_area + ' .alert' ).hide();
							$( '#' + list_area ).hide('slow');
							
						}
					}
				});
				
				//$( '#' + $(this).attr('data-item') ).toggle();
							  			  
			});
			
			$('.delete-item').click(function(e) {
				
				e.preventDefault();
				
				var to_delete = $(this).attr('id').replace('delete-','');
				
				if( confirm( 'Are you sure you want to delete this ' + to_delete + '?' ) ) {
					
					location.href = '/delete?id=' + $('#id').val() + '&model=' + to_delete + 's';
					
				}
			  			  
			});
			
			$('.cancel-item').click(function(e) {
				
				e.preventDefault();
				
				var to_cancel = $(this).attr('id').replace('cancel-','');
				
				location.href = '/' + to_cancel + 's';
			  			  
			});
			
			$('.booking-list-link').click(function(e) {
				
				e.preventDefault();
							  			  
			});
			
			$(function() {
				
				$( "#release_date-id" ).datepicker();
			
			});
			
			// if location changes, re-center map
			$('#get-coordinates').click( function(e) {
			
				e.preventDefault();
			
				geocoder = new google.maps.Geocoder();
				
				var address = $('#address-id').val() + ' ' + $('#address2-id').val() + ', ' + $('#city-id').val() + ', ' + $('#state-id').val() + ' ' + $('#zipcode-id').val();
				
				geocoder.geocode({ 'address': address }, function (results, status) {
					
					if (status == google.maps.GeocoderStatus.OK) {
					
						$('#latitude-id').val( results[0].geometry.location.lat() );
						
						$('#longitude-id').val( results[0].geometry.location.lng() );
						//map.setCenter( results[0].geometry.location );
						
					} else {
						
						alert('Geocode was not successful for the following reason: ' + status);
						
					}
				
				});
			  
		  	});
		  	
//		  	$('.question-selector').click(function(e){
//		  	
//		  		e.preventDefault();
//		  		
//		  		$('.question-selector').removeClass('selected');
//		  		$('.question-selector').parent().css('border','3px #fff solid');
//		  		
//		  		var swatch_click = $(this).find('span.selector-swatch');
//		  		var selected_teeth = $(this).parent().find('span.selected-teeth');		  		
//		  		var color = swatch_click.css("background-color");
//		  		
//		  		if( swatch_click.hasClass('checked-swatch') ) {
//			  		
//			  		swatch_click.removeClass('checked-swatch');
//			  		selected_teeth.html('');			  		
//			  		$('a.tooth').removeClass( swatch_click.attr('class').replace('selector-swatch','') );
//			  		
//		  		} else {
//			  		
//			  		swatch_click.addClass('checked-swatch');
//			  		selected_teeth.html('( All Teeth )');
//			  		$(this).addClass('selected');
//			  		$(this).parent().css('border','3px ' + color + ' solid');
//			  		
//		  		}
//		  		
//		  	});
		  	
		  	$('.tooth').click(function(e){
		  	
		  		e.preventDefault();
		  		
		  		var selected_teeth = [];
		  		
		  		if( $(this).hasClass('selected') ) {
			  		
			  		$(this).removeClass('selected');
			  		
		  		} else {
			  		
			  		$(this).addClass('selected');
			  		
		  		}
		  		
		  		$('.tooth-chart .selected').each(function() {
			  		
			  		selected_teeth[ selected_teeth.length ] = $(this).attr('id');
			  			
		  		});
		  		
		  		$('.tooth-chart .tooth-chart-val').val( selected_teeth.join(',') );
		  				  		
		  	});
						
		});
		
		
	</script>
	
	<? if( $action == 'schedule' || ( $action == 'index' && $_SESSION['logged_in_user']['homepage'] == 'schedule' ) ): ?>
	
	<script>

		$(document).ready(function() {
		
			$('#calendar').fullCalendar({
				editable: true,
				events: [
					<? foreach( $controller->referrals as $r ): ?>
					{
						<? $appt_time = strtotime( $r['appointment'] ); ?>
						
						id: <?=$r['id'] ?>,
						title: '<?=$r['patient_name'] ?>',
						start: new Date(<?=date( 'Y', $appt_time ) ?>, <?=( date( 'm', $appt_time ) - 1 ) ?>, <?=date( 'd', $appt_time ) ?>, <?=date( 'H', $appt_time ) ?>, <?=date( 'i', $appt_time ) ?>),
						url: '/view_referral?id=<?=$r['id'] ?>',
						allDay: false
					},
					<? endforeach; ?>
				]
			});
			
		});
	
	</script>

	<? endif; ?>
	
	<? if( $action == 'schedule_appointment' ): ?>
	
		<?
		
			$current = time();
			$three_months = strtotime( '+3 months' );
			
		?>
	
	<script>

		$(document).ready(function() {
		
			$('#calendar').fullCalendar({
				editable: false,
				events: [
					<? while( $current < $three_months ): ?>
					
						<? if( $controller->result[ date( 'l', $current ) ] ): ?>
						
						{
							id: <?=$current ?>,
							title: "<?=implode( ", ", $controller->result[ date( 'l', $current ) ] ) ?>",
							start: new Date(<?=date( 'Y', $current ) ?>, <?=( date( 'm', $current ) - 1 ) ?>, <?=date( 'd', $current ) ?>, <?=date( 'H', $current ) ?>, <?=date( 'i', $current ) ?>),
							allDay: true
						},
						
						<? endif; ?>
					
						<? $current = strtotime( '+1 day', $current ) ?>
						
					<? endwhile; ?>
				],
				eventClick: function(calEvent, jsEvent, view) {
				
					var postData = 'ajax=true&referral=' + $('#referral').val() + '&doctor=' + $('#doctor').val() + '&day=' + calEvent.start;
				
					$.ajax({
					  url: '/_choose_appointment_day',
					  data: postData,
					  type: "GET"
					}).done(function(data) {
					  //alert( data );
					  	
					  	$('#choose-day').html( data );
					  	
					  	// change the border color just for fun
					  	$(this).css('border-color', 'red');
					});

			        //alert('Event: ' + calEvent.start);
			        //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
			        //alert('Dr: ' + $('#doctor').val());
			
			        
			
			    }
			});
			
		});
	
	</script>

	<? endif; ?>
	
	<? if( $_SERVER['SERVER_NAME'] != 'localhost' ): ?>
	
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-56674050-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
	
	<? endif; ?>
        
  </body>
</html>