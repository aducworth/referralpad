<form action='/doctor' id='data-form' method='post' enctype="multipart/form-data">

<? if( $_GET['id'] ): ?>

	<input type='hidden' id='id' name='id' value='<?=$_GET['id'] ?>'/>

<? endif; ?>

 <div class='row'>
    
    	<div class='col-md-6'>
    	
    		<h1><?=$_GET['id']?'Edit':'Add' ?> Doctor for <?=$controller->office_list[ $_SESSION['logged_in_user']['office_id'] ] ?></h1>
    		
    	</div>

		<? if( $controller->result['image'] ): ?>
		
				<div class='col-md-6'>
		    
					<img src="/assets/images/uploads/thumbnails/<?=$controller->result['image'] ?>" class="img-rounded img-responsive pull-right">
					
				</div>
		    
		<? endif; ?>
		
 </div>
 
 <? if( $_SESSION['logged_in_user']['office_id'] ): ?>

	<input type='hidden' id='office_id' name='office_id' value='<?=$_SESSION['logged_in_user']['office_id'] ?>'/>
	
<? else: ?>

	<?=$form->select( 'office_id', $controller->office_list, array( 'label' => 'Office', 'default' => $controller->result['office_id'], 'empty' => ' ( Choose Office ) ', 'class' => 'required' ) ) ?>
	
<? endif; ?>


<?=$form->textbox( 'fname', array( 'label' => 'First Name', 'default' => $controller->result['fname'], 'class' => 'required' ) ) ?>
<?=$form->textbox( 'lname', array( 'label' => 'Last Name', 'default' => $controller->result['lname'], 'class' => 'required' ) ) ?>
<?=$form->textbox( 'credentials', array( 'label' => 'Credentials', 'default' => $controller->result['credentials'], 'class' => '' ) ) ?>
<?=$form->select( 'tooth_chart', array( 0 => 'No', 1 => 'Yes' ), array( 'label' => 'Tooth Chart', 'default' => $controller->result['tooth_chart'], 'class' => 'required' ) ) ?>
<?=$form->select( 'doctor_type_id', $controller->doctor_type_list, array( 'label' => 'Doctor Type', 'default' => $controller->result['doctor_type_id'], 'empty' => ' ( Choose Doctor Type ) ', 'class' => 'required' ) ) ?>

<?=$form->file_input( 'image', array( 'label' => 'Image', 'help' => '( Please restrict photos to jpgs. The image will automatically be resized. )' ) ) ?>


<? $index = 0; ?>

<? $selected = $controller->result['referral_questions']?$controller->result['referral_questions']:array(); ?>

<? foreach( $controller->referral_question_list as $doctor_type => $questions ): ?>

	<div id='doctor-type-<?=$doctor_type ?>' class='row questions-by-doctor-type' <?=( $controller->result['doctor_type_id'] != $doctor_type )?"style='display: none';":"" ?>>	
	
		<h3><?=$controller->doctor_type_list[ $doctor_type ] . ' Referral Questions' ?> </h3>
		
		<p>Please select all questions for your referral card.</p>
		
		<? foreach( $questions as $id => $question ): ?>
		
			<div class='col-sm-4'>
										
				<?=$form->custom_select( ('referral_questions['.$index.']'), array( $id => $question ), array( 'default' => ( in_array( $id, $selected )?$id:'' ) ) ) ?>
		
			</div>
			
			<? $index++; ?>
		
		<? endforeach; ?>
		
		<? //$form->checkbox( 'referral_questions', $questions, array( 'label' => ( $controller->doctor_type_list[ $doctor_type ] . ' Referral Questions' ), 'default' => ($controller->result['referral_questions']?$controller->result['referral_questions']:array()), 'class' => '' ) ) ?>
		
	</div>

<? endforeach; ?>

<? if( 1 == 2 ): // hide these for now ?> 

	<h2>Hours Available for Appointment</h2>
	
	<div style='padding: 10px 0px 10px 0px; width: 80%'>
	
		<a href="/doctor_hour?doctor_id=<?=$controller->result['id'] ?>" class='btn btn-default'>Add Hours</a>
	
	</div>
	
	<? if( count( $controller->result['doctor_hours'] ) ): ?>
	
		<table id='default-table' class="table table-striped table-condensed">
		
			<thead>
		    	<tr>
		        	<th>Day of Week</th>
		        	<th>Starting Hour</th>
		        	<th>Ending Hour</th>
		            <th>&nbsp;</th>
		        </tr>
		    </thead>
		    
		    <tbody>
		    
		    	<? foreach( $controller->result['doctor_hours'] as $r ): ?>
		                
		        <tr>
		        	<td><?=$r['day_of_week'] ?></td>
		        	<td><?=$controller->appointment_hours[ $r['starting_hour'] ] ?></td>
					<td><?=$controller->appointment_hours[ $r['ending_hour'] ] ?></td>
		            <td><a href='/doctor_hour?id=<?=$r['id'] ?>'>edit</a> - <a href='/delete?id=<?=$r['id'] ?>&model=doctor_hours' onclick="return confirm( 'Are you sure?' )">delete</a></td>
		        </tr>
		        
		        <? endforeach; ?>
		        
		    </tbody>
		
		</table>
	
	<? else: ?>
	
		<p>No hours have been set up for this doctor.</p>
	    
	<? endif; ?>

<? endif; ?>

<p class='action-buttons'>

	<input type='submit' class="btn btn-info pull-left" value='Save'/>

	<? if( $_GET['id'] ): ?>

		<!-- <button id='delete-doctor' class="btn btn-danger pull-right delete-item">Delete</button> -->
        
    <? else: ?>
    
    	<!-- <button id='cancel-doctor' class="btn btn-danger pull-right cancel-item">Cancel</button> -->
        
    <? endif; ?>

</p>

</form>