<form action='/signup' id='data-form' method='post' enctype="multipart/form-data">

	<h1 class='signup'><strong>Referral</strong>Pad</h1>
	
	<div class='row'>
	
		<div class='col-md-12'>
		
			<ol id='signup-process'>
				<li><h2>NPI Verification</h2>
				
					<div class="row">
				
						<div class='col-md-6'>
						
							<p>Please enter your npi number:</p>
						
							<div id='npi-form' class='form-group form-inline'>
							
								<?=$form->textbox( 'npi', array( 'placeholder' => 'NPI', 'default' => '', 'class' => '', 'form_group' => false ) ) ?>
							
								<button id='verify-npi' class='btn btn-primary dark-blue'>Verify</button>
								
							</div>
							
						</div>
						
					</div>
					
					<div class="row">
				
						<div id='npi-search-results' class='col-md-12'></div>
						
					</div>
					
				</li>
				
			</ol>
			
		</div>
		
	</div>

</form>