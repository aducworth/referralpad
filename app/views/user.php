<h1><?=$_GET['id']?'Edit':'Add' ?> User</h1>

<form action='/user' id='data-form' method='post' enctype="multipart/form-data">

<? if( $_GET['id'] ): ?>

	<input type='hidden' id='id' name='id' value='<?=$_GET['id'] ?>'/>

<? endif; ?>

<?=$form->textbox( 'fname', array( 'label' => 'First Name', 'default' => $controller->result['fname'], 'class' => 'required', 'form_group' => true ) ) ?>
<?=$form->textbox( 'lname', array( 'label' => 'Last Name', 'default' => $controller->result['lname'], 'class' => 'required', 'form_group' => true ) ) ?>
<?=$form->textbox( 'email', array( 'label' => 'Email', 'default' => $controller->result['email'], 'class' => 'email', 'form_group' => true ) ) ?>

<? if( $_SESSION['logged_in_user']['admin'] ): ?>

	<?=$form->select( 'admin', $controller->auth->user_types, array( 'label' => 'Type', 'default' => $controller->result['admin'], 'form_group' => true ) ) ?>
	
<? else: ?>

	<input type='hidden' id='admin' name='admin' value='<?=$controller->result['admin'] ?>'/>

<? endif; ?>

<?=$form->select( 'homepage', $controller->auth->homepage_views, array( 'label' => 'Homepage', 'default' => $controller->result['homepage'], 'form_group' => true ) ) ?>

<? if( $_SESSION['logged_in_user']['admin'] ): ?>

	<?=$form->select( 'office_id', $controller->office_list, array( 'label' => 'Office', 'default' => $controller->result['office_id'], 'empty' => ' ( Choose Office ) ', 'form_group' => true ) ) ?>
	
<? else: ?>

	<input type='hidden' id='office_id' name='office_id' value='<?=$controller->result['office_id'] ?>'/>

<? endif; ?>

<?=$form->textbox( 'password', array( 'label' => 'Password', 'class' => ($_GET['id']?'':'required'), 'type' => 'password', 'form_group' => true ) ) ?>
<?=$form->textbox( 'password_confirm', array( 'label' => 'Confirm Password', 'type' => 'password', 'form_group' => true ) ) ?>

<p class='action-buttons'>

	<input type='submit' class="btn btn-info pull-left" value='Save'/>

	<? if( $_GET['id'] ): ?>

		<button id='delete-user' class="btn btn-danger pull-right delete-item">Delete</button>
        
    <? else: ?>
    
    	<button id='cancel-user' class="btn btn-danger pull-right cancel-item">Cancel</button>
        
    <? endif; ?>

</p>

</form>