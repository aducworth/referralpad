

<form class="form-signin" role="form" action='/forgot' method='post'>
	<h1><strong>Referral</strong>Pad</h1>
	
	<fieldset>
		<legend><h2 class="form-signin-heading">Forgot Password</h2></legend>
		<?=$form->textbox( 'email', array( 'label' => false, 'placeholder' => 'Email', 'class' => 'required', 'required' => true, 'autofocus' => true, 'type' => 'email', 'class' => 'required', 'required' => true, 'default' => $_COOKIE['email'] ) ) ?>
		<button class="btn btn-primary btn-block" type="submit">Get Password</button>
		<a href='/login' class='pull-right'>Login</a>
		
	</fieldset>
	
</form>