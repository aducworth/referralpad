<h1>Patients</h1>

<form id='patient-filters' class="form-inline" role="form" action='/patients'>
                    
  	<? //$form->select( 'unread', array( 'unread' => 'Unread Only', 'everything' => 'Everything' ), array( 'label' => false, 'default' => $_GET['unread'] , 'class' => 'action-search', 'form_group' => true ) ) ?>
  	
  	<?=$form->select( 'source', array( '' => 'All Patients', 'from' => 'From My Office', 'to' => 'To My Office' ), array( 'label' => false, 'default' => $_GET['source'] , 'class' => 'action-search', 'form_group' => true ) ) ?>
  
  	<?=$form->select( 'referral_status', $controller->referral_statuses, array( 'label' => false, 'default' => $_GET['referral_status'], 'empty' => ' ( Choose Status ) ', 'class' => 'action-search', 'form_group' => true ) ) ?>
  	
  	<?=$form->select( 'specialist', $controller->doctor_type_list, array( 'label' => false, 'default' => $_GET['specialist'], 'empty' => ' ( Choose Specialist ) ', 'class' => 'action-search', 'form_group' => true ) ) ?>

  	<input type='hidden' name='sort' value='<?=$_GET['sort'] ?>'>
  
  <? //$form->textbox( 'search', array( 'label' => false, 'default' => $_GET['search'], 'class' => '', 'form_group' => true, 'placeholder' => 'Search' ) ) ?>
  
</form>

<? if( count( $controller->results ) ): ?>

	<? $shown = 0; ?>
	
		<div class="table-responsive">
	
		<table class='table table-hover'>
		
			<thead>
				<tr>
					<th><?=$functions->sortable( 'patient_name', 'Name' ) ?></th>
					<th><?=$functions->sortable( 'patient_id', 'ID#' ) ?></th>
					<!-- <th>Task</th> -->
					<th>Referred By</th>
					<th>Referred To</th>
					<th>Status</th>
					<th><?=$functions->sortable( 'appointment', 'Appointment' ) ?></th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			
			<tbody>
    
    	<? foreach( $controller->results as $r ): ?>
    	
    	<? $unread = $controller->referral_views->isUnread( $r ); ?>
    	
    	<? if( $_GET['unread'] != 'unread' || $unread ): ?>
    	
    	<? $shown++; ?>
    	
    		<tr class='table-striped'>
				<td><?=$r['patient_name'] ?></td>
				<td><a href='/view_referral?id=<?=$r['id'] ?>'><?=$r['patient_id'] ?></a></td>
				<!-- <td>Task</td> -->
				<td><?=$controller->doctor_list[ $r['referred_by'] ] ?></td>
				<td><?=$controller->doctor_list[ $r['referred_to'] ] ?></td>
				<td><?=$controller->referral_statuses[ $r['referral_status'] ] ?></td>
				<td>
					<? $appt = $functions->formatDateTime(  $r['appointment'] ); ?>
        
					<? if( $appt ): ?>
					
	            		<?=$appt ?>
	            		
	            	<? else: ?>
		            	
		            		<span>----</span>
	            	
	            	<? endif; ?>
	            </td>
	            <td><a href='/delete?id=<?=$r['id'] ?>&model=referrals&redirect=patients' onclick="return confirm( 'Are you sure you want to delete this referral?' )"><span class="glyphicon glyphicon-trash"</span></a></td>
			</tr>
                        
        <? endif; ?>
        
        <? endforeach; ?>
        
        	</tbody>
        	
        </table>
        
		</div>
        
        <? if( $shown == 0 ): ?>
        
        	<p>There are no unread referrals at this time. To see all past referrals click <a href='/referrals?unread=everything'>here</a>.
        
        <? endif; ?>

<? else: ?>

	<p>No patients are in the system or none match your search.</p>
    
<? endif; ?>