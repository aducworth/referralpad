<? if( count( $controller->results ) ): ?>

<div class="table-responsive">

	<table id='default-table' class="table table-striped table-condensed">
	
		<thead>
	    	<tr>
	    		<th>&nbsp;</th>
	        	<th>Doctor</th>
	        	<th>Specialty</th>
	        	<th>Address</th>
	        	<th>Phone</th>
	        	<th>Favorite</th>
	            <th>&nbsp;</th>
	        </tr>
	    </thead>
	    
	    <tbody>
	
		<? foreach( $controller->results as $r ): ?>
		
			<tr>
	    		<td><?=$functions->getAvatar( $controller->doctor_name_list[ $r['id'] ], $controller->doctor_image_list[ $r['id'] ] ) ?></td>
	        	<td>
	        		<strong><?=$r['fname'] ?> <?=$r['lname'] ?></strong><br>
	        		<?=$controller->office_list[ $r['office_id'] ] ?>
					
				</td>
	        	<td><?=$r['credentials'] ?></td>
	        	<td><?=$controller->office_address_list[ $r['office_id'] ] ?><br><?=$controller->office_email_list[ $r['office_id'] ]?$controller->office_email_list[ $r['office_id'] ]:'' ?></td>
	        	<td><?=$controller->office_phones[ $controller->offices_by_doctor[ $r['id'] ] ]?$controller->office_phones[ $controller->offices_by_doctor[ $r['id'] ] ]:'none' ?></td>
	        	<td><? 
						if( $controller->my_favorites[ $r['id'] ] ) {
			        	
		        			$star_icon = 'glyphicon glyphicon-star';
		        		
						} else {
						
							$star_icon = 'glyphicon glyphicon-star-empty';
							
						} 
						
					 ?>
					
					<a href='#' class='update-favorites btn' data-attr='<?=$r['id'] ?>'><i id='favorite-<?=$r['id'] ?>' class="<?=$star_icon ?>"></i></a></td>
	            <td>
		            
		            <? if( $controller->users_by_office[ $r['office_id'] ] ): ?>
		            
		            	<a href='/referral?doctor=<?=$r['id'] ?>' class='btn btn-info'>Refer Patient</a>
		            
		            <? else: ?>
		            
		            	<a href='/invite?doctor=<?=$r['id'] ?>' class='btn btn-primary'>Invite</a>
		            	
		            <? endif; ?>
		            
		        </td>
	        </tr>
	             
	    <? endforeach; ?>
	    
	    </tbody>
	    
	</table>

</div>

<? else: ?>

	<p>No doctors match your search.</p>
    
<? endif; ?>

<script>

$(document).ready(function(){

		$('.update-favorites').click(function(e){
			
			e.preventDefault();
			
			var doctor_id = $(this).attr('data-attr');
			var postData = 'doctor=' + $(this).attr('data-attr');
			//var icon_class = $('#favorite-'+doctor_id).attr('class');
			
			//alert( icon_class );
			
			$.ajax({
			  url: '/_update_favorites?ajax=true',
			  data: postData,
			  type: "POST"
			}).done(function(data) {
			
			  	if( $('#favorite-'+doctor_id).hasClass('glyphicon-star') ) {
				  	
				  	$('#favorite-'+doctor_id).removeClass('glyphicon-star');
				  	$('#favorite-'+doctor_id).addClass('glyphicon-star-empty');
				  	
			  	} else {
				  	
				  	$('#favorite-'+doctor_id).removeClass('glyphicon-star-empty');
				  	$('#favorite-'+doctor_id).addClass('glyphicon-star');
				  	
			  	}
			  	
			  	// update the favorites grid
			  	$.ajax({
			  	  url: '/_favorites_grid?ajax=true'				  	
			  	}).done(function(data) {
			  	  $('#favorites-grid').html(data);
			  	});
			  	
			});
		
		});


});

</script>