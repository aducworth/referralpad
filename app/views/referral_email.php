<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Referral via Referral Pad</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style>
body {min-width: 320px;}
</style>
</head>
<body style="background-color: #fff"><div style="width:100%!important;font-family:Georgia,'Times New Roman',Times,serif;margin:0;padding:0">
  <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse">
    <tbody>
      <tr>
        <td>
          <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse">
            <tbody>
              <tr>
                <td colspan="3" height="13"></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td style="background: rgb(32, 41, 50); color:#ffffff">
          <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;background: rgb(32, 41, 50);color:#ffffff">
            <tbody>
              <tr>
                <td colspan="3" height="12" style="background: rgb(32, 41, 50);color:#ffffff"></td>
              </tr>
              <tr>
                <td width="24" style="background: rgb(32, 41, 50);color:#ffffff"></td>
                <td style="font-family:Helvetica,Arial,sans-serif;font-size:15px;line-height:18px;background: rgb(32, 41, 50);color:#ffffff">
                  <? $appointment = $functions->formatDateTime( $controller->result['appointment'] ); ?>
                  
                  <? if( $_GET['comment'] ): ?>
                  
                  	<?=$controller->doctor_list[ $controller->comment_info['doctor_id'] ] ?> <strong style="font-weight:bold">commented on a referral</strong>.
                  
                  <? else: ?>
                  
	                  <? if( $appointment ): ?>
	                  
	                  	Appointment scheduled with <strong style="font-weight:bold"> <?=$controller->doctor_list[ $controller->result['referred_to'] ] ?> on <?=$appointment ?></strong>.
	                  
	                  <? else: ?>
	                  
	                  	<?=$controller->doctor_list[ $controller->result['referred_by'] ] ?> <strong style="font-weight:bold">referred you to <?=$controller->doctor_list[ $controller->result['referred_to'] ] ?></strong>.
	                  	
	                  <? endif; ?>
	                  
	              <? endif; ?>
                </td>
                <td width="24" style="background: rgb(32, 41, 50);color:#ffffff"></td>
              </tr>
              <tr>
                <td colspan="3" height="12" style="background: rgb(32, 41, 50);color:#ffffff"></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse">
            <tbody>
              <tr>
                <td colspan="3" height="40"></td>
              </tr>
              <tr>
                <td colspan="3" height="5"></td>
              </tr>
              <tr>
                <td width="24"></td>
                <td style="font-family:Helvetica,Arial,sans-serif;font-size:28px;line-height:34px;font-weight:bold">
                  <a href="http://<?=$_SERVER['SERVER_NAME'] ?>/view_referral?id=<?=$controller->result['id'] ?>" style="color:#3a87ad;" target="_blank"><?=$controller->result['patient_id'] ?></a>
                </td>
                <td width="24"></td>
              </tr>
              <tr>
                <td colspan="3" height="22"></td>
              </tr>
              <tr>
                <td width="24"></td>
                <td>
                  <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse">
                    <tbody>
                      <? if( $appointment ): ?>
                      
                      <tr>
                        <td width="130" valign="top" style="font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:16px;font-weight:bold;text-transform:uppercase;letter-spacing:1px;color:#777777;vertical-align:top;padding-top:3px;padding-bottom:4px">

                          Appointment
                        </td>
                        <td id="creator" valign="top" style="vertical-align:top;line-height:20px;font-size:15px;font-family:Georgia,'Times New Roman',Times,serif;padding-bottom:4px">
                          <?=$appointment ?>
                        </td>
                      </tr>

                      
                      <? else: ?>
                      
                      <tr>
                        <td width="130" valign="top" style="font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:16px;font-weight:bold;text-transform:uppercase;letter-spacing:1px;color:#777777;vertical-align:top;padding-top:3px;padding-bottom:4px">

                          Date
                        </td>
                        <td id="creator" valign="top" style="vertical-align:top;line-height:20px;font-size:15px;font-family:Georgia,'Times New Roman',Times,serif;padding-bottom:4px">
                          <?=date('m/d/Y', strtotime( $controller->result['created'] ) ) ?>
                        </td>
                      </tr>
                      
                      <? endif; ?>
                      
                      <tr>
                        <td width="130" valign="top" style="font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:16px;font-weight:bold;text-transform:uppercase;letter-spacing:1px;color:#777777;vertical-align:top;padding-top:3px;padding-bottom:4px">

                          Referred By
                        </td>
                        <td id="fixer" valign="top" style="vertical-align:top;line-height:20px;font-size:15px;font-family:Georgia,'Times New Roman',Times,serif;padding-bottom:4px">
                          <?=$controller->doctor_list[ $controller->result['referred_by'] ] ?>
                        </td>
                      </tr>
                      <tr>
                        <td width="130" valign="top" style="font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:16px;font-weight:bold;text-transform:uppercase;letter-spacing:1px;color:#777777;vertical-align:top;padding-top:3px;padding-bottom:4px">

                          Referred To
                        </td>
                        <td id="tester"valign="top" style="vertical-align:top;line-height:20px;font-size:15px;font-family:Georgia,'Times New Roman',Times,serif;padding-bottom:4px">
                          <?=$controller->doctor_list[ $controller->result['referred_to'] ] ?><br>
                          
                          <? if( !$_GET['comment'] ): ?>
                          
                          <i><?=$controller->office_info['name'] ?><br>
                          <?=$controller->office_info['address'] ?><br>
                          <?=$controller->office_info['city'] ?>, <?=$controller->office_info['state'] ?> <?=$controller->office_info['zipcode'] ?><br>
                          p: <?=$controller->office_info['phone'] ?><br>
                          e: <?=$controller->office_info['email'] ?></i>
                          
                          <? endif; ?>
                        </td>
                      </tr>
                                   
                    </tbody>
                  </table>
                </td>
                <td width="24"></td>
              </tr>
              <tr>
                <td colspan="3" height="22"></td>
              </tr>
              <tr>
                <td width="24"></td>
                <td>
                  <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse">
                    <tbody>
                      <tr>
                        <td height="1" style="background-color:#d7d7d7;font-size:0px"></td>
                      </tr>
                      <tr>
                        <td height="13"></td>
                      </tr>
                      <tr>
                        <td style="font-family:Georgia,'Times New Roman',Times,serif;font-style:italic;font-size:13px;line-height:20px;color:#777777">
                          This is an email sent from  <a href="http://<?=$_SERVER['SERVER_NAME'] ?>/" style="color:#BBA05E;text-decoration:underline" target="_blank">Referral Pad</a>. <a href="http://<?=$_SERVER['SERVER_NAME'] ?>/" style="color:#BBA05E;text-decoration:underline" target="_blank">Unsubscribe me</a> &nbsp from all emails from Referral Pad.
                        </td>
                      </tr>
                      <tr>
                        <td height="13"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td width="24"></td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</body>