<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Referral Pad - <?=( $action != 'index' )?ucwords( str_replace( '_', ' ', $action ) ):'Dashboard' ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <!-- Bootstrap -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
    <link href="/assets/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="/assets/fullcalendar/fullcalendar.print.css" rel="stylesheet">
    
    <link href="/assets/css/app.css" rel="stylesheet" media="screen">
    <link href="/assets/datepicker/css/datepicker.css" rel="stylesheet" media="screen">
    
    <link href="/assets/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/jquery.ui.1.10.css" />
    <link href="/assets/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="/assets/fullcalendar/fullcalendar.print.css" rel="stylesheet">
    
    <script src="//code.jquery.com/jquery-latest.js"></script>
    <script src="//code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/validate.js"></script>
    <script src="/assets/fullcalendar/fullcalendar.min.js"></script>
    <script src="/assets/datepicker/js/bootstrap-datepicker.js"></script>
    <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script> -->
    
    <script src="/assets/highcharts/js/highcharts.js"></script>
	<script src="/assets/highcharts/js/modules/exporting.js"></script>
    
    
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <!-- <link href="/assets/css/bootstrap-responsive.css" rel="stylesheet"> -->
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
  </head>
  <body>
  
  <div class="navbar navbar-default navbar-fixed-top">
	  
	  <div class="container">
	  
	  <? if( $controller->auth->logged_in() ): ?>
	  
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
	    <a class="navbar-brand" href="/"><strong>Referral</strong>Pad</a>
	  </div>
	  
	  <div class="navbar-collapse collapse navbar-responsive-collapse">
		  
		<? if( !$controller->auth->newUser() ): ?>
		
		    <ul class="nav navbar-nav">
		      
		      <?=$controller->auth->navigation() ?>
		      
		    </ul>
		    
		    <? if( $_SESSION['logged_in_user']['admin'] == 1 && in_array( $_GET['url'], $controller->auth->show_search ) ): ?>
		    
		    	<form class="navbar-form navbar-right" action='/<?=$action ?>'>
	              <div class="form-group has-feedback">                
	              	<input name='search' class="form-control" type="text" placeholder="<?=ucwords( $action ) ?> Search"  value="<?=$_GET['search'] ?>">
	              	<i class="glyphicon glyphicon-search form-control-feedback"></i>
	              </div>
	            </form>
		    
		    <? else: ?>
	        
	            <form class="navbar-form navbar-right" action='/patients'>
	              <div class="form-group has-feedback">                
	              	<input id='patient-search' name='search' class="form-control" type="text" placeholder="Patient ID Search" value="<?=$_GET['search'] ?>">
	              	<i class="glyphicon glyphicon-search form-control-feedback"></i>
	              </div>
	            </form>
	        
	        <? endif; ?>
	        
	        <ul class="nav navbar-nav navbar-right">
	              
	          <?=$controller->auth->navigation( 'account' ) ?>
	          
	        </ul>
        
        <? endif; ?>
                                
	  </div>
	  
	  <? endif; ?>
	  
	  </div>
	  
	</div>
  
    <div id='main-content' class="container">
    
    	<? if( $_GET['search'] ): ?>
    	
    		<div class='row'>
    		
    			<div class="col-md-12">
        
		            <p>
		            	<div class="alert alert-info">
		              
			              <a class="close" data-dismiss="alert" href='/<?=$_GET['url'] ?>'>×</a>  
			              
			              Results for "<?=$_GET['search'] ?>"  
			              
			            </div> 
		            </p>
	            
    			</div>
	            
    		</div>
        
        <? endif; ?>

        <? if( $controller->message ): ?>
        
        	<div class='alert alert-danger'>
            
            	<?=$controller->message ?>
            
            </div>
        
        <? endif; ?>
        
        <? if( $controller->success ): ?>
        
        	<div class='alert alert-success'>
            
            	<?=$controller->success ?>
            
            </div>
        
        <? endif; ?>
        
        <? //print_r( $_SESSION ) ?>