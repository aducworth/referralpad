<form action='/import_doctors' id='data-form' method='post' enctype="multipart/form-data">

	<div class='row'>
	
		<div class='col-md-12'>
		
			<ol id='signup-process'>
				<li><h2>Import NPI for Zipcode</h2>
				
					<div class="row">
				
						<div class='col-md-6'>
						
							<div id='npi-form' class='form-group form-inline'>
							
								<?=$form->textbox( 'zipcode', array( 'placeholder' => 'Zipcode', 'default' => '', 'class' => '', 'form_group' => false ) ) ?>
							
								<button class='btn btn-primary dark-blue'>Import</button>
								
							</div>
							
						</div>
						
					</div>
					
					<div class="row">
				
						<div id='npi-search-results' class='col-md-12'></div>
						
					</div>
					
				</li>
				
			</ol>
			
		</div>
		
	</div>

</form>