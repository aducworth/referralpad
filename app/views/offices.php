<h1>Offices</h1>

<div class='row'>

	<div class='col-md-12'>

		<a href='/office' class='btn btn-default'>Add Office</a>
	
	</div>

</div>

<? if( count( $controller->offices ) ): ?>

<table id='default-table' class="table table-striped table-condensed">

	<thead>
    	<tr>
        	<th><?=$functions->sortable( 'name', 'Name' ) ?></th>
        	<th><?=$functions->sortable( 'office_type', 'Type' ) ?></th>
        	<th><?=$functions->sortable( 'city', 'City / State' ) ?></th>
        	<th>Doctors</th>
        	<th>Users</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    
    <tbody>
    
    	<? foreach( $controller->offices as $r ): ?>
                
        <tr>
        	<td><?=$r['name'] ?></td>
        	<td><?=$controller->office_types[ $r['office_type'] ] ?></td>
        	<td><?=$controller->office_addresses[ $r['id'] ] ?></td>
        	<td><a href='/doctors?office=<?=$r['id'] ?>'><?=$controller->doctors_by_office[ $r['id'] ]?$controller->doctors_by_office[ $r['id'] ]:0 ?></a></td>
        	<td><a href='/users?office=<?=$r['id'] ?>'><?=$controller->users_by_office[ $r['id'] ]?$controller->users_by_office[ $r['id'] ]:0 ?></a></td>
            <td><a href='/office?id=<?=$r['id'] ?>'>edit</a> - <a href='/delete?id=<?=$r['id'] ?>&model=offices' onclick="return confirm( 'Are you sure?' )">delete</a></td>
        </tr>
        
        <? endforeach; ?>
        
    </tbody>

</table>

<? else: ?>

	<p>No offices are in the system.</p>
    
<? endif; ?>