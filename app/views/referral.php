<form action='/referral' id='data-form' method='post' enctype="multipart/form-data">

<div class='row'>

	<div class='col-md-12'>
	
		<ol>
			<li><h2>Doctor Info</h2>
				
				<div class="row">
				
					<div class='col-md-6'>
  
				  		<div class="row">
				
					  		<div class='col-md-6'>
					  		
					  			<h3>Referred To</h3>
				  
				  				<?=$form->custom_select( 'referred_to', array( $controller->doctor_info['id'] => $controller->doctor_name_list[ $controller->doctor_info['id'] ] ), array( 'default' => $controller->doctor_info['id'] ) ) ?>
				  								  				
					  		</div>
					  		
					  		<div class='col-md-6'>
					  		
					  			<h3>Location</h3>
					  		
					  			<? 
					  			
					  				$default_location = 0;
					  				
					  				$location_list = array();
					  			
					  				foreach( $controller->doctor_locations as $dl ) {
					  				
					  					if( !$default_location ) {
						  					
						  					$default_location = $dl['id'];
						  					
					  					}
						  			
						  				$location_list[ $dl['id'] ] = $dl['name'] . '<span>' . $dl['phone'] . '</span>';
						  			
					  				}
					  				
					  			?>
				  
				  				<?=$form->custom_select( 'referred_to_location', $location_list, array( 'default' => $default_location ) ) ?>
				  								  				
					  		</div>
					  		
				  		</div>

					</div>
					
					<div class='col-md-6'>
  
				  		<h3>Referred From</h3>
				  		
				  		<div class="row">
				
					  		<div class='col-md-6'>
				  
						  		<? if( $_SESSION['logged_in_user']['office_id'] ): ?>
						  		
						  			<? $dr_list = $controller->doctors_by_office_list; ?>
				
								<? else: ?>
								
									<? $dr_list = $controller->doctor_list; ?>
																					
								<? endif; ?>
								
								<? if( count( $dr_list ) ): ?>
								
									<? 		
										$default_referred_by = 0;
																		
										foreach( $dr_list as $value => $label ) {
											
											if( !$default_referred_by ) {
												
												$default_referred_by = $value;
												
											}
											
										}
																				
									?>
					  			
					  				<?=$form->custom_select( 'referred_by', $dr_list, array( 'default' => ($controller->result['referred_by']?$controller->result['referred_by']:$default_referred_by) ) ) ?>
					  			
					  			<? endif; ?>
					  			
					  		</div>
					  		
				  		</div>
						
					</div>
	  
				</div>
			</li>
			<li><h2>Patient Info</h2>
			
				<div class="row">
				
					<div class='col-md-6'>
					
						<?=$form->textbox( 'patient_name', array( 'placeholder' => 'Patient Name', 'default' => $controller->result['patient_name'], 'class' => 'required', 'form_group' => true ) ) ?>

						<?=$form->textbox( 'patient_email', array( 'placeholder' => 'Email', 'default' => $controller->result['patient_email'], 'class' => 'required', 'form_group' => true ) ) ?>
						
						<?=$form->textbox( 'patient_phone', array( 'placeholder' => 'Mobile Phone', 'default' => $controller->result['patient_phone'], 'class' => '', 'form_group' => true ) ) ?>
						
						<? //$form->textbox( 'patient_dob', array( 'placeholder' => 'DOB', 'default' => $controller->result['patient_dob'], 'class' => '', 'form_group' => true ) ) ?>
						<div class="form-group form-inline patient_dob">  
						
							<label for="input-patient_dob">DOB</label>
							
							<?=$form->textbox( 'patient_dob', array( 'label' => false, 'default' => '', 'placeholder' => 'mm / dd / yyyy', 'form_group' => true ) ) ?>

							              
			              	<!--
<?=$form->textbox( 'dob_month', array( 'label' => false, 'default' => '', 'placeholder' => 'mm', 'form_group' => true ) ) ?>
			              	<span> / </span>
			              	
			              	<?=$form->textbox( 'dob_day', array( 'label' => false, 'default' => '', 'placeholder' => 'dd', 'form_group' => true ) ) ?>
			              	<span> / </span>
			              	
			              	<?=$form->textbox( 'dob_year', array( 'label' => false, 'default' => '', 'placeholder' => 'yy', 'form_group' => true ) ) ?>
-->
		              </div>
					
					</div>
					
					<div class='col-md-6'>
					
						<?=$form->textbox( 'patient_address1', array( 'placeholder' => 'Street', 'default' => $controller->result['patient_address1'], 'class' => '', 'form_group' => true ) ) ?>

						<?=$form->textbox( 'patient_address2', array( 'placeholder' => 'Street 2', 'default' => $controller->result['patient_address2'], 'class' => '', 'form_group' => true ) ) ?>
						
						<div class='form-group'>
						
							<?=$form->textbox( 'patient_city', array( 'placeholder' => 'City', 'default' => $controller->result['patient_city'], 'class' => '', 'form_group' => false ) ) ?>
							
							<?=$form->textbox( 'patient_state', array( 'placeholder' => 'State', 'default' => $controller->result['patient_state'], 'class' => '', 'form_group' => false ) ) ?>
							
							<?=$form->textbox( 'patient_zipcode', array( 'placeholder' => 'Zipcode', 'default' => $controller->result['patient_zipcode'], 'class' => '', 'form_group' => false ) ) ?>
					
						</div>
						
					</div>
					
				</div>
			
			</li>
			<li><h2>Prescription Info</h2>
			
				<div class='row'>
				
					<div class='col-md-6'>
					
						<h3>Concerns</h3>
						
						<? if( count( $controller->dr_q_list ) ): ?>
						
							<? $selected = $controller->result['referral_questions']?explode(',',$controller->result['referral_questions']):array(); ?>
							
							<div class='row'>
													
								<? foreach( $controller->dr_q_list as $id => $question ): ?>
								
									<div class='col-sm-6'>
									
									<?=$form->custom_select( ('referral_questions['.$id.']'), array( $id => $question ), array( 'default' => ( in_array( $id, $selected )?$id:'' ) ) ) ?>
								
									</div>
									
								<? endforeach; ?>
							
							</div>
								
						<? endif; ?>
						
						<?=$form->textarea( 'notes', array( 'label' => 'Notes', 'default' => $controller->result['notes'], 'class' => '' ) ) ?>
						
					</div>
						
					<div class="col-md-6">
					
						<? if( $controller->doctor_info['tooth_chart'] == 1 ): ?>
									
							<h3>Tooth Chart</h3>
					
							<?=$form->tooth_chart( 'tooth_chart', $controller->result['tooth_chart'] ) ?>
						
						<? endif; ?>
						
						<h3>Attachment(s)</h3>

						<?=$controller->attachments->listAttachments( $controller->result['attachments'] ) ?>
						
						<? for( $i=1;$i<4;$i++ ): ?>
						
							<?=$form->file_input( 'attachments[' . $i . ']', array( 'label' => ( 'Attachment #' . $i ) ) ) ?>
						
						<? endfor; ?>
					
					</div>
					
				</div>
				
			</li>
			<li><h2>Scheduling</h2>
			
				<div class="row">
				
					<div class='col-md-2'>
					
						<div class="form-group has-feedback">                
			              	<input id="appointment-date" name="appointment_date" class="form-control datepicker" type="text" placeholder="Date" value="">
			              	<i class="glyphicon glyphicon-calendar form-control-feedback" style='top: 0;'></i>
		              </div>
					
					</div>
					
					<div class='col-md-6'>
					
						<div class="form-group form-inline">  
						
							<label for="input-appointment-hour">Time</label>
							              
			              	<?=$form->select( 'appointment_hour', $controller->appointment_hours, array( 'label' => false, 'default' => '', 'empty' => 'hr', 'form_group' => true ) ) ?>
			              	<span> : </span>
			              	<?=$form->select( 'appointment_minute', $controller->minute_list, array( 'label' => false, 'default' => '', 'empty' => 'min', 'form_group' => true ) ) ?>
		              </div>
					
					</div>
				
				</div>
			
			</li>
		</ol>
	
	</div>
	
</div>

<? if( $_GET['id'] ): ?>

	<input type='hidden' id='id' name='id' value='<?=$_GET['id'] ?>'/>
	
	<? //$form->select( 'referral_status', $controller->referral_statuses, array( 'label' => 'Status', 'default' => $controller->result['referral_status'], 'class' => 'required', 'empty' => ' ( Choose Status ) ' ) ) ?>
    
<? endif; ?>

<div class='row'>

	<? if( $_GET['id'] ): ?>
	
		<div class="col-md-6">
		
			<button type='submit' name='submit' class="btn btn-info" value='Completed'>Completed</button>
			
		</div>
		
	<? else: ?>

		<div class="col-md-3">
		
			<button type='submit' name='submit' class="btn btn-info" value='Completed'>Completed</button>
			
		</div>
		
		<div class="col-md-3">
			
			<button type='submit' name='submit' class="btn btn-primary dark-blue" value='Schedule Up Front'>Schedule Up Front</button>
		
		</div>
		
	<? endif; ?>
	
	<div class="col-md-6">
	
		<? if( $_GET['id'] ): ?>
	
			<button id='delete-referral' class="btn btn-danger pull-right delete-item">Delete</button>
	        	        
	    <? endif; ?>
	
	</div>

</div>

</form>