<h1>Referrals</h1>

<div class='row'>

	<form class="navbar-form pull-right" action='/referrals'>
	                    
	  	<?=$form->select( 'unread', array( 'unread' => 'Unread Only', 'everything' => 'Everything' ), array( 'label' => false, 'default' => $_GET['unread'] , 'class' => 'action-search' ) ) ?>
	  	
	  	<?=$form->select( 'source', array( 'from' => 'From My Office', 'to' => 'To My Office' ), array( 'label' => false, 'default' => $_GET['source'] , 'class' => 'action-search' ) ) ?>
	  
	  	<?=$form->select( 'referral_status', $controller->referral_statuses, array( 'label' => false, 'default' => $_GET['referral_status'], 'empty' => ' ( Choose Status ) ', 'class' => 'action-search' ) ) ?>
	  
	  <input name='search' class="span2" type="text" placeholder="Search" value="<?=$_GET['search'] ?>">
	  
	</form>

</div>

<? if( count( $controller->referrals ) ): ?>

	<? $shown = 0; ?>
    
    	<? foreach( $controller->referrals as $r ): ?>
    	
    	<? $unread = $controller->referral_views->isUnread( $r ); ?>
    	
    	<? if( $_GET['unread'] != 'unread' || $unread ): ?>
    	
    	<? $shown++; ?>
                
        <div class='row referral-list'>
			<div class='span2'><span class="referral-label <?=strtolower( str_replace( ' ', '_', $controller->referral_statuses[ $r['referral_status'] ] ) ) ?> label-info"><span class="unread-indicator <?=$unread?'unread':'' ?>"></span><?=$controller->referral_statuses[ $r['referral_status'] ] ?></span></div>
            <div class='span2'>
            	<strong>ID#</strong> <?=$r['patient_id'] ?><br>
            	<a href='/view_referral?id=<?=$r['id'] ?>'><?=$r['patient_name'] ?></a><br>
            	<span>e:</span> <a href='mailto:<?=$r['patient_email'] ?>'><?=$r['patient_email'] ?></a>
            </div>
            <div class='span8'>
            	<span>Referred By:</span> <?=$controller->doctor_list[ $r['referred_by'] ] ?><br>
            	<span>Referred To:</span> <?=$controller->doctor_list[ $r['referred_to'] ] ?><br>
            	
            	<? $appt = $functions->formatDateTime(  $r['appointment'] ); ?>
            
				<? if( $appt ): ?>
				
            		<span>Appointment:</span> <?=$appt ?>
            	
            	<? endif; ?>
            </div>
        </div>
        
        <? endif; ?>
        
        <? endforeach; ?>
        
        <? if( $shown == 0 ): ?>
        
        	<p>There are no unread referrals at this time. To see all past referrals click <a href='/referrals?unread=everything'>here</a>.
        
        <? endif; ?>

<? else: ?>

	<p>No referrals are in the system.</p>
    
<? endif; ?>