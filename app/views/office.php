<form action='/office' id='data-form' method='post' enctype="multipart/form-data" role='form'>

<? if( $_GET['id'] ): ?>

	<input type='hidden' id='id' name='id' value='<?=$_GET['id'] ?>'/>

<? endif; ?>

 <div class='row'>
    
    	<div class='col-md-6'>
    	
    		<h1><?=$_GET['id']?'Edit':'Add' ?> Office Profile</h1>
    		
    	</div>

<? if( $controller->result['image'] ): ?>
    
   		<div class='col-md-6'>
    
			<img src="/assets/images/uploads/thumbnails/<?=$controller->result['image'] ?>" class="img-rounded img-responsive pull-right" />
		</div>
    
<? endif; ?>

 </div>
 
 <? if( $_GET['id'] ): ?>
 
 	<h2>Office Locations</h2>
 	
 	<div class='row'>
    
    	<div class='col-md-12'>
    	
    		<a class='btn btn-info' href='/office_location?office_id=<?=$_GET['id'] ?>'>Add Location</a><br><br>
    		
    	</div>
    	
 	</div>
 	
 	<? if( count( $controller->result_locations ) ): ?>
 	
 		<div class='row'>
 		
 			<? foreach( $controller->result_locations as $index => $location ): ?>
 			
 				<div class='col-md-3'>
 				
 					<address>
 					
 						<b><?=$location['name'] ?></b><br>
 						<?=$location['address'] ?><br>
 						<?=$location['address2']?$location['address2'].'<br>':'' ?>
 						<?=$location['city'] ?>, <?=$location['state'] ?>, <?=$location['zipcode'] ?><br>
 						<?=$location['phone']?'p: '.$location['phone'].'<br>':'' ?>
 						<?=$location['fax']?'f: '.$location['fax'].'<br>':'' ?>
 						<a href='/office_location?id=<?=$location['id'] ?>'>edit</a>
 						
 						<? if( $index != 0 ): ?>
 						
 							 - <a href='/office_location?id=<?=$location['id'] ?>'>delete</a>
 							 
 					    <? endif; ?>
 					
 					</address>
 				
 				</div>
 			
 			<? endforeach; ?>
 		
 		</div>
 	
 	<? endif; ?>
 	
 <? endif; ?>
 
 

<?=$form->select( 'office_type', $controller->office_types, array( 'label' => 'Type', 'default' => $controller->result['office_type'], 'empty' => ' ( Choose Office Type ) ', 'form_group' => true ) ) ?>

<?=$form->textbox( 'name', array( 'label' => 'Name', 'default' => $controller->result['name'], 'class' => 'required', 'form_group' => true ) ) ?>
<?=$form->textbox( 'email', array( 'label' => 'Email', 'default' => $controller->result['email'], 'class' => 'required email', 'form_group' => true ) ) ?>
<?=$form->textbox( 'website', array( 'label' => 'Website', 'default' => $controller->result['website'], 'class' => '', 'form_group' => true ) ) ?>
<?=$form->textbox( 'facebook', array( 'label' => 'Facebook', 'default' => $controller->result['facebook'], 'class' => '', 'form_group' => true ) ) ?>
<?=$form->textbox( 'twitter', array( 'label' => 'Twitter', 'default' => $controller->result['twitter'], 'class' => '', 'form_group' => true ) ) ?>

<?=$form->textarea( 'office_introduction', array( 'label' => 'Office Introduction', 'default' => $controller->result['office_introduction'], 'class' => '' ) ) ?>

<?=$form->file_input( 'image', array( 'label' => 'Image', 'help' => '( The image will automatically be resized. )', 'title' => 'Please choose an image to upload.' ) ) ?>
		
<?=$form->file_input( 'health_history', array( 'label' => 'Health History Form', 'help' => '( Please restrict to pdf. )' ) ) ?>

<p class='action-buttons'>

	<input type='submit' class="btn btn-info pull-left" value='Save'/>

</p>

</form>