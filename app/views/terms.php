<form action='/terms' id='data-form' method='post' enctype="multipart/form-data" role='form'>

<input type='hidden' name='id' value='<?=$_SESSION['logged_in_user']['id'] ?>'>
<input type='hidden' name='terms_and_conditions' value='<?=$controller->result['id'] ?>'>

 <div class='row'>
    
    	<div class='col-md-12'>
    	
    		<h1>We have updated our terms and conditions.</h1>
    		<h2>Please review and accept to continue using The Referral Pad.</h2>
    		
    	</div>
    	
    	<div class='col-md-12'>
    	
    		<p><?=nl2br( $controller->result['content'] ) ?></p>
    		
    	</div>

 </div>


 <div class='row'>
    
    	<div class='col-md-12 text-right'>


			<input type='submit' class="btn btn-primary dark-blue" value='Agree'/>

		</div>

 </div>

</form>