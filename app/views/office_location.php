<form action='/office_location' id='data-form' method='post' enctype="multipart/form-data" role='form'>

<? if( $_GET['id'] ): ?>

	<input type='hidden' id='id' name='id' value='<?=$_GET['id'] ?>'/>
	<input type='hidden' id='office_id' name='office_id' value='<?=$controller->result['office_id'] ?>'/>

<? endif; ?>

<? if( $_GET['office_id'] ): ?>

	<input type='hidden' id='office_id' name='office_id' value='<?=$_GET['office_id'] ?>'/>

<? endif; ?>


 <div class='row'>
    
    	<div class='col-md-6'>
    	
    		<h1><?=$_GET['id']?'Edit':'Add' ?> Office Location</h1>
    		
    	</div>

 </div>

<?=$form->textbox( 'name', array( 'label' => 'Name', 'default' => $controller->result['name'], 'class' => 'required', 'form_group' => true ) ) ?>
<?=$form->textbox( 'address', array( 'label' => 'Address', 'default' => $controller->result['address'], 'class' => 'required', 'form_group' => true ) ) ?>
<?=$form->textbox( 'address2', array( 'label' => '', 'default' => $controller->result['address2'], 'class' => '', 'form_group' => true ) ) ?>
<?=$form->textbox( 'city', array( 'label' => 'City', 'default' => $controller->result['city'], 'class' => 'required', 'form_group' => true ) ) ?>
<?=$form->textbox( 'state', array( 'label' => 'State', 'default' => $controller->result['state'], 'class' => 'required', 'form_group' => true ) ) ?>
<?=$form->textbox( 'zipcode', array( 'label' => 'Zipcode', 'default' => $controller->result['zipcode'], 'class' => 'required', 'form_group' => true ) ) ?>
<?=$form->textbox( 'phone', array( 'label' => 'Phone', 'default' => $controller->result['phone'], 'class' => 'required phone', 'form_group' => true ) ) ?>
<?=$form->textbox( 'fax', array( 'label' => 'Fax', 'default' => $controller->result['fax'], 'class' => 'phone', 'form_group' => true ) ) ?>

<p class='action-buttons'>

	<input type='submit' class="btn btn-info pull-left" value='Save'/>

	<? if( $_GET['id'] ): ?>

		<button id='delete-office_location' class="btn btn-danger pull-right delete-item">Delete</button>
        
    <? else: ?>
    
    	<button id='cancel-office_location' class="btn btn-danger pull-right cancel-item">Cancel</button>
        
    <? endif; ?>

</p>

</form>