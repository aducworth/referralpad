

<form class="form-signin" role="form" action='/signup' method='post'>
	<h1><strong>Referral</strong>Pad</h1>
	
	<fieldset>
		<legend><h2 class="form-signin-heading">Sign Up</h2></legend>
		<?=$form->textbox( 'fname', array( 'label' => false, 'placeholder' => 'First Name', 'class' => 'required', 'required' => true, 'autofocus' => true, 'type' => 'text', 'class' => 'required', 'required' => true, 'default' => '' ) ) ?>
		<?=$form->textbox( 'lname', array( 'label' => false, 'placeholder' => 'Last Name', 'class' => 'required', 'required' => true, 'autofocus' => true, 'type' => 'text', 'class' => 'required', 'required' => true, 'default' => '' ) ) ?>
		<?=$form->textbox( 'email', array( 'label' => false, 'placeholder' => 'Email', 'class' => 'required', 'required' => true, 'autofocus' => true, 'type' => 'email', 'class' => 'required', 'required' => true, 'default' => $_COOKIE['email'] ) ) ?>
		<?=$form->textbox( 'password', array( 'label' => false, 'class' => 'required', 'type' => 'password', 'required' => true, 'placeholder' => 'Password' ) ) ?>
		<?=$form->textbox( 'password_confirm', array( 'label' => false, 'class' => 'required', 'type' => 'password', 'required' => true, 'placeholder' => 'Confirm Password' ) ) ?>
		<button class="btn btn-primary btn-block" type="submit">Sign Up</button>
		
	</fieldset>
	
</form>