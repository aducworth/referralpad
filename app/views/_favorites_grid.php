<? if( count( $controller->favorites ) ): ?>

<div class='row doctor-list'>

<? foreach( $controller->favorites as $r ): ?>
        
		<div class='col-md-3 make-referral'>
		
			<div class='row make-referral-header'>
		
					<?=$functions->getAvatar( $controller->doctor_name_list[ $r['doctor_id'] ], $controller->doctor_image_list[ $r['doctor_id'] ] ) ?>
					
					<h3><?=$controller->doctor_name_list[ $r['doctor_id'] ] ?></h3>
					<h4><?=$controller->office_list[ $controller->offices_by_doctor[ $r['doctor_id'] ] ] ?></h4>
				
			</div>
			
			<div class='row'>
			
				<div class='col-xs-3'><label>office</label></div>
				<div class='col-xs-9'>
					<? if( $controller->office_location_counts[ $controller->offices_by_doctor[ $r['doctor_id'] ] ] > 1 ): ?>
					
						<button class="btn btn-link" data-toggle="tooltip" data-placement="bottom" title="<?=$controller->office_locations->getLocationsTooltip( $controller->offices_by_doctor[ $r['doctor_id'] ] ) ?>" style='margin: 0; padding: 0; color: #000;'>Multiple Locations <span class="glyphicon caret" aria-hidden="true"></span></button>
						
					<? else: ?>
					
						<address>
							<?=$controller->office_address_list[ $controller->offices_by_doctor[ $r['doctor_id'] ] ] ?>
						</address>
					
					<? endif; ?>
					
				</div>
				
			</div>
			
			<div class='row'>
			
				<div class='col-xs-3'><label>phone</label></div>
				<div class='col-xs-9'><?=$controller->office_phones[ $controller->offices_by_doctor[ $r['doctor_id'] ] ]?$controller->office_phones[ $controller->offices_by_doctor[ $r['doctor_id'] ] ]:'none' ?></div>
				
			</div>
			
			<div class='row'>
			
				<div class='col-xs-3'><label>fax</label></div>
				<div class='col-xs-9'><?=$controller->office_faxes[ $controller->offices_by_doctor[ $r['doctor_id'] ] ]?$controller->office_faxes[ $controller->offices_by_doctor[ $r['doctor_id'] ] ]:'none' ?></div>
				
			</div>
			
			<div class='row'>
			
				<div class='col-xs-3'><label>email</label></div>
				<div class='col-xs-9'><?=$controller->office_email_list[ $controller->offices_by_doctor[ $r['doctor_id'] ] ]?$controller->office_email_list[ $controller->offices_by_doctor[ $r['doctor_id'] ] ]:'none' ?></div>
				
			</div>
			
			<div class='row'>
			
				<div class='col-xs-3'><label>web</label></div>
				<div class='col-xs-9'>
					<?=AppFunctions::formatUrl( $controller->office_websites[ $controller->offices_by_doctor[ $r['doctor_id'] ] ] ) ?>					
				</div>
				
			</div>

			<div class='row'>
			
				<div class='col-xs-12 text-center'>
				
					<a href='/referral?doctor=<?=$r['doctor_id'] ?>' class='btn btn-info'>Refer Patient</a>
					
				</div>
				
			</div>

		</div>

<? endforeach; ?>

</div>

<? else: ?>

	<p>No favorites are in the system. Use the form above to search for doctors.</p>
    
<? endif; ?>