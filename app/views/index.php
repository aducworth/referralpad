<? if( $_SESSION['logged_in_user']['id'] ): ?>

	<? if( $_SESSION['logged_in_user']['admin'] == 1 ): ?>
	
		<? include( 'app/views/dashboard.php' ) ?>
		
	<? else: ?>
	
		<? if( $_SESSION['logged_in_user']['homepage'] == 'schedule' ): ?>
		
			<? $controller->schedule() ?>
			
			<? include( 'app/views/schedule.php' ) ?>
			
		<? elseif( $_SESSION['logged_in_user']['homepage'] == 'list' ): ?>
		
			<? $controller->referrals() ?>
			
			<? include( 'app/views/referrals.php' ) ?>
			
		<? else: ?>
		
			<? $controller->favorites() ?>
			
			<? include( 'app/views/favorites.php' ) ?>
			
		<? endif; ?>
	
	<? endif; ?>
	
<? else: ?>

	<? include( 'app/views/login.php' ) ?>
	
<? endif; ?>