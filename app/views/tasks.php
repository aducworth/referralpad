<h1>Tasks</h1>

<? if( count( $controller->results ) ): ?>

	<div class="table-responsive">

		<table class='table'>
		
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>Name</th>
					<th>ID#</th>
					<th>Task</th>
					<th>Referred By</th>
					<th>Referred To</th>
					<th>Status</th>
					<th>Appointment</th>
				</tr>
			</thead>
			
			<tbody>
    
    	<? foreach( $controller->results as $r ): ?>
    	    	
    	    	<? $appt = $functions->formatDateTime(  $r['appointment'] ); ?>
    	    	
    			<tr>
					<td><input type='checkbox' name='read' value='1' class='check-read-task' data-value='<?=$r['id'] ?>'></td>
					<td><?=$r['patient_name'] ?></td>
					<td><a href='/<?=$r['completed']?'view_referral':'referral' ?>?id=<?=$r['id'] ?>'><?=$r['patient_id'] ?></a></td>
					<td><?=($r['task_status'] == 'new')?'New Pt':'New Comment/Xray' ?></td>
					<td><?=$controller->doctor_list[ $r['referred_by'] ] ?></td>
					<td><?=$controller->doctor_list[ $r['referred_to'] ] ?></td>
					<td><?=$controller->referral_statuses[ $r['referral_status'] ] ?></td>
					<td>	
            
						<? if( $appt ): ?>
						
		            		<?=$appt ?>
		            		
		            	<? else: ?>
		            	
		            		<span>----</span>
		            	
		            	<? endif; ?>
		            </td>
				</tr>
                        
        <? endforeach; ?>
        
			</tbody>
			
		</table>
		
	</div>
        
<? else: ?>

	<p>No tasks are in the system.</p>
    
<? endif; ?>