<?

	$categories = array();
	$series = array();
	$pie_totals = array();

	foreach( $controller->overtime as $month_year => $data ) {
		
		$categories[] = $month_year;
		
		foreach( $data as $specialist => $count ) {
			
			$series[ $specialist ][] = $count;
			$pie_totals[ $specialist ] += $count;
			
		}
		
	}
	
?>

<script type="text/javascript">
$(function () {
    $('#pie-chart').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,//null,
            plotShadow: false
        },
        title: {
            text: 'Referrals By Specialist'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Specialist share',
            data: [
            	<? foreach( $pie_totals as $specialist => $count ): ?>
            		<? if( $count > 0 ): ?>
						['<?=$specialist ?>', <?=$count ?>],
					<? endif; ?>
                <? endforeach; ?>
            ]
        }]
    });
});


		</script>
		
<script type="text/javascript">
$(function () {
    $('#line-chart').highcharts({
        title: {
            text: 'Referrals Over Time',
            x: -20 //center
        },
        subtitle: {
            text: 'By Specialist',
            x: -20
        },
        xAxis: {
            categories: ['<?=implode( "','", $categories ) ?>']
        },
        yAxis: {
            title: {
                text: 'Referrals'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' Referrals'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [
        <? foreach( $series as $specialist => $data ): ?>
        {
            name: '<?=$specialist ?>',
            data: [<?=implode( ',', $data ) ?>]
        },
        <? endforeach; ?>]
    });
});
		</script>

<div class='row'>

	<div id="line-chart" class='col-md-12'></div>
</div>
<div class='row'>

	<div id="pie-chart" class='col-md-12'></div>

</div>

<script src="/js/highcharts/js/highcharts.js"></script>
<script src="/js/highcharts/js/modules/exporting.js"></script>