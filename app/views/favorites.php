<h1>Make Referral</h1>

<form id='doctor-search' class="form-inline well" role="form">
	<div class="form-group">
	    <label>Search For Doctors</label>
	</div>
	<?=$form->textbox( 'name', array( 'placeholder' => 'Doctor Name', 'default' => $_GET['name'], 'form_group' => true ) ) ?>
	<?=$form->textbox( 'location', array( 'placeholder' => 'City, State, or Zip', 'default' => $_GET['location'], 'form_group' => true ) ) ?>
	<?=$form->select( 'specialty', $controller->doctor_type_list, array( 'label' => false, 'default' => $_GET['specialty'], 'empty' => 'Specialty', 'form_group' => true ) ) ?>
  <button type="submit" class="btn btn-search">Search</button>
</form>

<div id='favorites-grid'>

	<? include( 'app/views/_favorites_grid.php' ) ?>

</div>

<script>

	$(document).ready(function(){
	
		$('#doctor-search').submit(function(e) {
			
			e.preventDefault();
			
			// fill in modal form data
			$('#modal-name').val( $('#name-id').val() );
			$('#modal-location').val( $('#location-id').val() );
			$('#modal-specialty').val( $('#specialty-id').val() );
			
			var searchData = $('#doctor-search').serialize() + '&ajax=true';
			
			$.ajax({
			  url: '/search',
			  data: searchData
			}).done(function(data) {
			  $('#search-results').html( data );
			  $('#myModal').modal('show');
			});
			
		});
		
		$('#modal-search-form').submit(function(e) {
			
			e.preventDefault();
			
			var searchData = $('#modal-search-form').serialize() + '&ajax=true';
			
			$.ajax({
			  url: '/search',
			  data: searchData
			}).done(function(data) {
			  $('#search-results').html( '<p>Loading...</p>' );
			  $('#search-results').html( data );
			});
			
		});
			
	});

</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Modal title</h4> -->
        
        <form id='modal-search-form' class="form-inline" role="form">
			<div class="form-group">
			    <label>Search For Doctors</label>
			</div>
			<?=$form->textbox( 'name', array( 'id' => 'modal-name', 'placeholder' => 'Doctor Name', 'default' => $_GET['name'], 'form_group' => true ) ) ?>
			<?=$form->textbox( 'location', array( 'id' => 'modal-location', 'placeholder' => 'City, State, or Zip', 'default' => $_GET['location'], 'form_group' => true ) ) ?>
			<?=$form->select( 'specialty', $controller->doctor_type_list, array( 'id' => 'modal-specialty', 'label' => false, 'default' => $_GET['specialty'], 'empty' => 'Specialty', 'form_group' => true ) ) ?>
		  <button type="submit" class="btn btn-search" data-toggle="modal" data-target="#myModal">Search</button>
		</form>
      </div>
      <div id='search-results' class="modal-body">
      
		<? if( count( $controller->doctors ) ): ?>
		
		<table id='default-table' class="table table-striped table-condensed">
		
			<thead>
		    	<tr>
		    		<th>&nbsp;</th>
		        	<th>Name</th>
		        	<th>Office</th>
		        	<th>Referrals</th>
		            <th>&nbsp;</th>
		        </tr>
		    </thead>
		    
		    <tbody>
		    
		    	<? foreach( $controller->doctors as $r ): ?>
		                
		        <tr>
		        	<td>
		        		<?=$functions->getAvatar( $controller->doctor_name_list[ $r['id'] ], $controller->doctor_image_list[ $r['id'] ] ) ?>
		        	</td>
		        	<td><?=$r['fname'] ?> <?=$r['lname'] ?><?=$r['credentials']?(', ' . $r['credentials']):'' ?></td>
		        	<td><?=$controller->office_list[ $r['office_id'] ] ?></td>
					<td><?=$controller->referrals_by_doctor[ $r['id'] ]?$controller->referrals_by_doctor[ $r['id'] ]:0 ?></td>
		            <td><a href='/doctor?id=<?=$r['id'] ?>'>edit</a> - <a href='/delete?id=<?=$r['id'] ?>&model=doctors' onclick="return confirm( 'Are you sure?' )">delete</a></td>
		        </tr>
		        
		        <? endforeach; ?>
		        
		    </tbody>
		
		</table>
		
		<? else: ?>
		
			<p>No doctors are in the system.</p>
		    
		<? endif; ?>     

      </div>
      <!--
<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
-->
    </div>
  </div>
</div>