

<form class="form-signin" role="form" action='/login' method='post'>
	<h1><strong>Referral</strong>Pad</h1>
	
	<fieldset>
		<legend><h2 class="form-signin-heading">Sign in</h2></legend>
		<?=$form->textbox( 'email', array( 'label' => false, 'placeholder' => 'Username', 'class' => 'required', 'required' => true, 'autofocus' => true, 'type' => 'email', 'class' => 'required', 'required' => true, 'default' => $_COOKIE['email'] ) ) ?>
		<?=$form->textbox( 'password', array( 'label' => false, 'class' => 'required', 'type' => 'password', 'required' => true, 'placeholder' => 'Password' ) ) ?>
		<button class="btn btn-primary btn-block" type="submit">Sign in</button>
		<a href='/forgot' class='pull-right'>Forgot Password</a>
		<label class="checkbox">
		  <input type="checkbox" name="remember" value="1" <?=$_COOKIE['email']?"checked='true'":'' ?>> Remember me
		</label>
		
	</fieldset>
	
</form>