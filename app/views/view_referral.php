<div id='patient-card' class="row">

	<div class="col-md-6">
	
		<div class="row">
	
			<div class="col-md-12 well">
			
				  <h1><?=$controller->result['patient_name'] ?> <span><?=$functions->getAge( $controller->result['patient_dob'] ) ?></span></h1>
				  
				  <div class="row">
		
					  <div class="col-md-6">
					  
					  		<div class="row">
					  		
					  			<div class="col-md-12">
					  
						  				Referred By:
						  				
					  			</div>
					  			
					  		</div>
					  		<div class="row">
					  		
					  			<div class="col-md-12 big-details"> 
					  			
						  			<?=$controller->doctor_list[ $controller->result['referred_by'] ] ?>
						  			
					  			</div>
					  			
					  		</div>
					  	    <div class="row">
					  		
					  			<div class="col-md-12"> 
					  					
					  					Referred To:
					  					
					  			</div>
					  			
					  	    </div>
					  	    <div class="row">
					  		
					  			<div class="col-md-12 big-details"> 
					  				
					  				<?=$controller->doctor_list[ $controller->result['referred_to'] ] ?>
					  			
					  			</div>
					  			
					  	    </div>
					  	    <div class="row">
					  		
					  			<div class="col-md-12"> 
					  					
					  					Preferred Location:
					  					
					  			</div>
					  			
					  	    </div>
					  	    <div class="row">
					  		
					  			<div class="col-md-12 big-details"> 
					  				
					  				<?=$controller->preferred_location['name'] ?>
					  			
					  			</div>
					  			
					  	    </div>

					  	    
					  	    <? if( $controller->result['referral_questions'] ): ?>
					  	    
					  	    <div class="row">
					  		
					  			<div class="col-md-12"> 
					  					
					  					Prescription:
					  					
					  			</div>
					  			
					  	    </div>
					  	    
					  	    <div class="row">
					  		
					  			<div class="col-md-12 big-details"> 
								  
								  	<? 
								  		$selected = explode( ',', $controller->result['referral_questions'] );
								  		$tolist = array();
								  		
								  		foreach( $selected as $s ) {
									  		
									  		$tolist[] = $controller->question_list[ $s ];
									  		
								  		}
								  		
								  	?>
								  	
								  	<?=implode( '<br>', $tolist ); ?>
					  			
					  			</div>
					  			
					  	    </div>
					  	    
					  	    <? endif; ?>
				  
					  </div> <!-- end of left side of well -->
					  
					  <div class="col-md-6">
					  
					  	<div class="row">
					  		
					  			<div class="col-md-12"><label>DOB</label><?=( $controller->result['patient_dob'] && $controller->result['patient_dob'] != '0000-00-00' )?date( 'm/d/Y', strtotime( $controller->result['patient_dob'] ) ):'' ?></div>
					  			
					  	</div>
					  	
					  	<div class="row">
					  		
					  			<div class="col-md-12"><label>Phone</label><?=$controller->result['patient_phone']?$controller->result['patient_phone']:'' ?></div>
					  			
					  	</div>
					  	
					  	<div class="row">
					  		
					  			<div class="col-md-12"><label>Email</label><?=$controller->result['patient_email']?$controller->result['patient_email']:'' ?></div>
					  			
					  	</div>
					  	
					  	<div class="row">
					  		
					  			<div class="col-md-12"><label>Address</label><address><?=$controller->result['patient_address1'] ?><br><?=$controller->result['patient_city'] ?>, <?=$controller->result['patient_state'] ?> <?=$controller->result['patient_zipcode'] ?></address></div>
					  			
					  	</div>
					  	
					  	<? $appointment = $functions->formatDateTime( $controller->result['appointment'] ); ?>
						  
						  <? if( $appointment ): ?>
						  
						  	<? $appointment = date( 'F d, Y g:ia', strtotime( $appointment ) ); ?>
							
							<div class="row">
					  		
						  			<div class="col-md-10 scheduled-appointment-block">
						  				Selected Appointment:
						  				<span><?=$appointment ?></span>
						  			</div>
						  			
						  	</div>
							
							<!-- <?=$appointment ?> <a href='/schedule_appointment?referral=<?=$controller->result['id'] ?>' class='icon-edit'> </a> -->
						
						  <? endif; ?>
					  	
					  </div> <!-- end of right side of well -->
					  
					</div> <!-- end of top row of details, left and right sides -->
					  
					  <? if( $controller->result['tooth_chart'] ): ?>
					  
					  <div class="row">
					  		
					  		<div class="col-md-12">
					  
							  	<?=$form->tooth_chart('tooth_chart', $controller->result['tooth_chart'] ) ?>
							  	
					  		</div>
					  		
					  </div> <!-- end of tooth chart -->
							  
					  <? endif; ?>
					  
					  <? if( $controller->result['notes'] ): ?>
					  
					  <div class="row">
					  		
					  			<div class="col-md-2"><strong>notes</strong></div>
					  			<div class="col-md-10"><?=$controller->result['notes'] ?></div>
					  			
					  	</div> <!-- end of notes -->
					  	
					  <? endif; ?>
					  	
					  <? //if( $_SESSION['logged_in_user']['id'] == $controller->result['referred_by'] ): ?>
					  
					  	<div class="row patient-card-actions">
					  		
					  		<div class="col-md-9 actions">
					  		
					  			<p>
								    <a href='/referral?id=<?=$controller->result['id'] ?>'><span class="glyphicon glyphicon-edit"</span></a> <a href='/delete?id=<?=$controller->result['id'] ?>&model=referrals&redirect=patients' onclick="return confirm( 'Are you sure you want to delete this referral?' )"><span class="glyphicon glyphicon-trash"</span></a>
								  </p>
								  
					  		</div>
					  		
					  		<div class="col-md-3">
					  		
					  			<p>
					  			
					  				<a href='/email_patient?id=<?=$controller->result['id'] ?>&ajax=true' class='btn btn-primary btn-block dark-blue ' target='_blank'>Print</a>
					  				<a href='/download?id=<?=$controller->result['id'] ?>' class='btn btn-primary btn-block dark-blue '>Download</a>
					  				
					  			</p>
					  			
					  		</div>
					  		
					  	</div> <!-- end of edit / delete options -->
					  	
					  	<? //endif; ?>
						  
			</div> <!-- end of well -->
			
			<? if( count( $controller->result['attachments'] ) ): ?>
			
			<div class="row">
	
				<div class="col-md-12 attachments">
				
					<? //print_r( $controller->result ) ?>
					<?=$controller->attachments->listAttachments( $controller->result['attachments'] ) ?>
				
				</div>
				
			</div>
			
			<? endif; ?>
			
		</div>
		
	</div>
	
	<div class="col-md-6">
	
		<? if( count( $controller->comments ) ): ?>
		
			<? $index = 1; ?>
		
			<? foreach( $controller->comments as $c ): ?>
			
				<div class='row comment-row'>
				
					<div class="col-md-2 avatar-holder">
					
					<?=$functions->getAvatar( $controller->doctor_name_list[ $c['doctor_id'] ], $controller->doctor_image_list[ $c['doctor_id'] ] ) ?>
					
					</div>
					
					<div class="col-md-10">
					
						<p><strong><?=$controller->doctor_name_list[ $c['doctor_id'] ] ?></strong> <span> | </span> <?=$functions->formatDateTime( $c['created'] ) ?></p>
						
						<?=$c['notes'] ?>
				
						<? if( $controller->comment_attachments[ $c['id'] ] ): ?>
						
							<p><br><strong>Uploaded this attachment(s):</strong></p>
						
							<?=$controller->attachments->listAttachments( $controller->comment_attachments[ $c['id'] ] ) ?>
						
						<? endif; ?>
					
					</div>
				
				</div>
				
				<? $index++; ?>
			
			<? endforeach; ?>
		
		<? endif; ?>
		
		<div class='row leave-comment'>
		
			<div class="col-md-12">
		
			<form action='/view_referral' id='data-form' method='post' enctype="multipart/form-data">
			
				<? if( count( $controller->doctors_by_office_list ) > 1 ): ?>
		
					<?=$form->select( 'doctor_id', $controller->doctors_by_office_list, array( 'label' => false, 'default' => '', 'class' => 'required', 'empty' => ' ( Choose Doctor ) ' ) ) ?>
				
				<? else: ?>
				
					<? foreach( $controller->doctors_by_office_list as $id => $name ): ?>
				
						<input type='hidden' id='doctor_id' name='doctor_id' value='<?=$id ?>'/>
						
					<? endforeach; ?>
					
				<? endif; ?>
	
				<input type='hidden' name='referral_id' value='<?=$controller->result['id'] ?>'>
				
				<?=$form->textarea( 'notes', array( 'label' => false, 'default' => '', 'class' => 'required', 'placeholder' => 'Leave a comment...' ) ) ?>
				
				<div class='row'>
				
					<div class="col-md-6">
				
						<?=$form->file_input( 'attachments['.$i.']', array( 'label' => false ) ) ?>
						
					</div>
					<div class="col-md-6">
					
						<input type="submit" class="btn btn-info pull-right" value="Submit">
						
					</div>
					
				</div>
	
			</form>
			
			</div>
		
		</div>
		
	</div>
	
</div>
	

