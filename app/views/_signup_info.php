<? //session_start(); print_r( $_SESSION ); ?>

<li class='signup-info' style='display: none;'><h2>Primary Doctor</h2>
				
	<div class="row">

		<div class='col-md-6'>
		
			<? if( $_SESSION['signup']['doctor']['id'] ): ?>
			
				<input type='hidden' name='signup[doctor][id]' value='<?=$_SESSION['signup']['doctor']['id'] ?>'>
			
			<? endif; ?>
		
			<?=$form->select( 'signup[doctor][doctor_type_id]', $controller->doctor_type_list, array( 'label' => 'Doctor Type', 'default' => $_SESSION['signup']['doctor']['doctor_type_id'], 'empty' => ' ( Choose Doctor Type ) ', 'class' => 'required' ) ) ?>
			<?=$form->textbox( 'signup[doctor][fname]', array( 'label' => 'First Name', 'default' => $_SESSION['signup']['doctor']['fname'], 'class' => 'required' ) ) ?>
			<?=$form->textbox( 'signup[doctor][lname]', array( 'label' => 'Last Name', 'default' => $_SESSION['signup']['doctor']['lname'], 'class' => 'required' ) ) ?>
			<?=$form->textbox( 'signup[doctor][credentials]', array( 'label' => 'Credentials', 'default' => $_SESSION['signup']['doctor']['credentials'], 'class' => '' ) ) ?>
			
		</div>
		
	</div>
	
</li>

<li class='signup-info' style='display: none;'><h2>Office Information</h2>
				
	<div class="row">
		
		<? //print_r( $_SESSION ) ?>
		
		<? if( $_SESSION['signup']['office']['id'] ): ?>
		
			<? if( $_SESSION['signup']['doctor']['id'] ): ?>
			
				<input type='hidden' name='signup[office][id]' value='<?=$_SESSION['signup']['office']['id'] ?>'>
				
			<? else: ?>
			
				<label class="checkbox">
								  <input type="checkbox" id="claim_office" name='signup[office][id]' value="<?=$_SESSION['signup']['office']['id'] ?>" checked='true'>We found an office with your address that is already in The Referral Pad, "<?=$_SESSION['signup']['office']['name'] ?>". Uncheck the box if this is not your office.</label>
			
			<? endif; ?>
		
		<? endif; ?>
		
		<? if( is_array( $_SESSION['signup']['office_locations'] ) && count( $_SESSION['signup']['office_locations'] ) ): ?>
		
			<div id='office_locations'>
				
				<h3>Office Locations</h3>
			
				<? foreach( $_SESSION['signup']['office_locations'] as $location ): ?>
				
					<div class='col-md-3'>
						
						<label><?=$location['name'] ?></label><address><?=$location['address'] ?><br><?=$location['city']  ?>, <?=$location['state']  ?> <?=$location['zipcode']  ?></address>
						
					</div>
				
				<? endforeach; ?>
			
			</div>
			
			<div id='office_form' class='col-md-6' style='display: none;'>
		
		<? else: ?>
		
			<div id='office_form' class='col-md-6'>

		<? endif; ?>
						
				<?=$form->textbox( 'signup[office][name]', array( 'label' => 'Name', 'default' => $_SESSION['signup']['office']['name'] . ' Office', 'class' => 'required', 'form_group' => true ) ) ?>
				<?=$form->textbox( 'signup[office_location][address]', array( 'label' => 'Address', 'default' => $_SESSION['signup']['office_location']['address'], 'class' => 'required', 'form_group' => true ) ) ?>
				<?=$form->textbox( 'signup[office_location][address2]', array( 'label' => '', 'default' => $_SESSION['signup']['office_location']['address2'], 'class' => '', 'form_group' => true ) ) ?>
				<?=$form->textbox( 'signup[office_location][city]', array( 'label' => 'City', 'default' => $_SESSION['signup']['office_location']['city'], 'class' => 'required', 'form_group' => true ) ) ?>
				<?=$form->textbox( 'signup[office_location][state]', array( 'label' => 'State', 'default' => $_SESSION['signup']['office_location']['state'], 'class' => 'required', 'form_group' => true ) ) ?>
				<?=$form->textbox( 'signup[office_location][zipcode]', array( 'label' => 'Zipcode', 'default' => $_SESSION['signup']['office_location']['zipcode'], 'class' => 'required', 'form_group' => true ) ) ?>
				
			</div>
		
		
		
	</div>
	
</li>

<li class='signup-info' style='display: none;'><h2>Login Information</h2>
		
	<? if( $_SESSION['signup']['user']['id'] ): ?>
			
		<div class="row">
			
			<div class='col-md-6'>
			
				<label class="checkbox">
									  <input type="checkbox" id="create_account" value="1">Your office already has an account to login. Do you want to create another user account?</label>
									  
			</div>
			
			
		</div>
		
		<div id='login_info_form' class="row" style='display: none'>
		
	<? else: ?>
	
		<div id='login_info_form' class="row">
	
	<? endif; ?>
		
		<div class='col-md-6'>
			
			<?=$form->textbox( 'email', array( 'label' => 'Email', 'default' => '', 'class' => 'required', 'form_group' => true ) ) ?>
			
			<?=$form->textbox( 'password', array( 'label' => 'Password', 'default' => '', 'class' => 'required', 'form_group' => true ) ) ?>
			
		</div>
		
	
		
	</div>
	
</li>

<li class='signup-info' style='display: none;'><h2>Payment Information</h2>
				
	<div class="row">
		
		<div class='col-md-6'>
		
			<?=$form->textbox( 'card_no', array( 'label' => 'Card No', 'default' => '', 'class' => 'required', 'form_group' => true ) ) ?>
			
			<?=$form->textbox( 'exp_date', array( 'label' => 'Expiration', 'default' => '', 'class' => 'required', 'form_group' => true ) ) ?>
			
											  
		</div>
		
		
	</div>
	
	<div class="row">
		
		<div class='col-md-6'>
			
			<button class='btn'>Register</button>
			
		</div>
			
	</div>

	
</li>

<script>
	$('#claim_office').on('click', function (e) {
				
		if( $(this).is(':checked') ) {
			
			$('#office_locations').show();
			$('#office_form').hide();
			
		} else {
			
			$('#office_locations').hide();
			$('#office_form').show();
			
		}
		
	});
</script>

