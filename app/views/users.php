<h1>Users</h1>

<div class='row'>

	<div class='col-md-12'>
	
		<p><a href='/user' class='btn btn-default'>Add User</a></p>
	
	</div>
	
</div>

<? if( count( $controller->users ) ): ?>

<table id='default-table' class="table table-striped table-condensed">

	<thead>
    	<tr>
        	<th><?=$functions->sortable( 'lname', 'Name' ) ?></th>
            <th><?=$functions->sortable( 'email', 'Email' ) ?></th>
            <th>Type</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    
    <tbody>
    
    	<? foreach( $controller->users as $u ): ?>
                
        <tr>
        	<td><?=$u['fname'] ?> <?=$u['lname'] ?></td>
            <td><?=$u['email'] ?></td>
            <td><?=$controller->auth->user_types[ $u['admin'] ] ?></td>
            <td><a href='/admin_login?id=<?=md5( $u['id'] ) ?>'>login</a> - <a href='/user?id=<?=$u['id'] ?>'>edit</a> - <a href='/delete?id=<?=$u['id'] ?>&model=users' onclick="return confirm( 'Are you sure?' )">delete</a></td>
        </td>
        
        <? endforeach; ?>
        
    </tbody>

</table>

<? else: ?>

	<p>No users are in the system.</p>
    
<? endif; ?>