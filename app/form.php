<?

class Form {

	var $name_prefix = array( 
								'MR' => 'Mr.',
								'MRS' => 'Mrs.',
								'MS' => 'Ms.',
								'MISS' => 'Miss',
								'DR' => 'Dr.',
								'REV' => 'Rev.',
								'RET' => 'Ret.' );
								
	var $name_suffix = array( 'SR' => 'Sr.', 'JR' => 'Jr.' );
	
	var $states = array( 'AL' => 'AL', 'AK' => 'AK', 'AZ' => 'AZ', 'AR' => 'AR', 'CA' => 'CA', 'CO' => 'CO', 'CT' => 'CT', 'DE' => 'DE', 'DC' => 'DC', 'FL' => 'FL', 'GA' => 'GA', 'HI' => 'HI', 'ID' => 'ID', 'IL' => 'IL', 'IN' => 'IN', 'IA' => 'IA', 'KS' => 'KS', 'KY' => 'KY', 'LA' => 'LA', 'ME' => 'ME', 'MD' => 'MD', 'MA' => 'MA', 'MI' => 'MI', 'MN' => 'MN', 'MS' => 'MS', 'MO' => 'MO', 'MT' => 'MT', 'NE' => 'NE', 'NV' => 'NV', 'NH' => 'NH', 'NJ' => 'NJ', 'NM' => 'NM', 'NY' => 'NY', 'NC' => 'NC', 'ND' => 'ND', 'OH' => 'OH', 'OK' => 'OK', 'OR' => 'OR', 'PA' => 'PA', 'RI' => 'RI', 'SC' => 'SC', 'SD' => 'SD', 'TN' => 'TN', 'TX' => 'TX', 'UT' => 'UT', 'VT' => 'VT', 'VA' => 'VA', 'WA' => 'WA', 'WV' => 'WV', 'WI' => 'WI', 'WY' => 'WY' );
	
	var $card_types = array( 'Visa' => 'Visa', 'Mastercard' => 'Mastercard', 'Amex' => 'Amex', 'Discover' => 'Discover' );
	
	var $months = array( '01' => '01',
						'02' => '02',
						'03' => '03',
						'04' => '04',
						'05' => '05',
						'06' => '06',
						'07' => '07',
						'08' => '08',
						'09' => '09',
						'10' => '10',
						'11' => '11',
						'12' => '12' );
						
	var $days = array(  'Monday' 		=> 'Monday',
						'Tuesday' 		=> 'Tuesday',
						'Wednesday'		=> 'Wednesday',
						'Thursday'		=> 'Thursday',
						'Friday'		=> 'Friday' );
						
	function textbox( $name, $options=array() ) {
	
		$toreturn = '';
		
		if( $options['form_group'] ) {
			
			$form_group = true;
			
			$toreturn .= '<div class="form-group">';
			
			unset( $options['form_group']  );
			
		}
		
		if( $options['label'] ) {
		
			$toreturn .= "<label for='input-".$name."'>".$options['label'].":</label>";
		
		}
		
		if( $options['id'] ) {
		
			$id = $options['id'];
		
		} else {
			
			$id = $name."-id";
			
		}
		
		$toreturn .= "<input type='" . ( $options['type']?$options['type']:'text' ) . "' id='".$id."' name='".$name."' value=\"".($_POST[$name]?$_POST[$name]:$options['default'])."\" ";
		
		unset( $options['type'] );
		unset( $options['range'] );
		unset( $options['label'] );
		unset( $options['empty'] );
		unset( $options['default'] );
		unset( $options['id'] );
		
		// add in form control
		$options['class'] .= ' form-control';
		
		foreach( $options as $tag => $action ) {
		
			if( $tag == 'required' || $tag == 'autofocus' ) {
			
				$toreturn .= " " . $tag;
				
			} else {
			
				$toreturn .= " " . $tag . "=\"" . $action . "\"";
			
			}
		
		}
		
		$toreturn .= ">";
		
		if( $form_group ) {
			
			$toreturn .= '</div>';
			
		}
		
		return $toreturn;
	
	}
	
	function file_input( $name, $options=array() ) {
		
		$toreturn = '<div class="form-group">';
		
		if( $options['id'] ) {
			
			$id = $options['id'];
			
		} else {
			
			$id = $name . '-id';
			
		}
		
		if( $options['label'] ) {
		
			$toreturn .= "<label for='".$id."'>".$options['label'].":</label>";
		
		}
		
	    $toreturn .= '<input type="file" id="' . $id . '" name="' . $name . '">';
	    
	    if( $options['help'] ) {
		    
		    $toreturn .= '<p class="help-block">' . $options['help'] . '</p>';
		    
	    }
	    
		$toreturn .= '</div>';
		
		return $toreturn;
		
	}
	
	function textarea( $name, $options=array() ) {
	
		$toreturn = '<div class="form-group">';
		
		if( $options['label'] ) {
		
			$toreturn .= "<label for='input-".$name."'>".$options['label'].":</label>";
		
		}
		
		$toreturn .= "<textarea id='".$name."-id' name='".$name."' class='form-control'";
		
		$options['class'] .= ' form-control';
		
		foreach( $options as $tag => $action ) {
		
			if( $tag != 'range' && $tag != 'label' && $tag != 'empty' ) {
			
				$toreturn .= " " . $tag . "=\"" . $action . "\"";
			
			}
		
		}
		
		$toreturn .= ">".($_POST[$name]?$_POST[$name]:$options['default'])."</textarea></div>";
		
		return $toreturn;
	
	}
	
	function select( $name, $inputs, $options=array() ) {
	
		$toreturn = '<div class="form-group">';
				
		if( $options['label'] ) {
		
			$toreturn .= "<label for='input_".$name."'>".$options['label'].":</label>";
		
		}
		
		if( isset( $options['range']['lower'] ) && isset( $options['range']['upper'] ) ) {
		
			for( $i=$options['range']['lower'];$i<$options['range']['upper'];$i++ ) {
			
				$inputs[$i] = $i;
			
			}
		
		}
		
		if( $options['id'] ) {
		
			$id = $options['id'];
		
		} else {
			
			$id = $name."-id";
			
		}
		
		$toreturn .= "<select id='".$id."' name='".$name."' ";
		
		$options['class'] .= ' form-control';
		
		unset( $options['id'] );
		
		foreach( $options as $tag => $action ) {
		
			if( $tag != 'range' && $tag != 'label' && $tag != 'empty' ) {
			
				$toreturn .= " " . $tag . "=\"" . $action . "\"";
			
			}
		
		}
		
		$toreturn .= ">";
		
		if( $options['empty'] ) {
			
			$selected = ($_POST[$name]=='')?'selected':'';
		
			$toreturn .= "<option value='' ".$selected.">".$options['empty']."</option>";
		
		}
		
		foreach( $inputs as $value => $display ) {
		
			$selected = ($_POST[$name] == $value || $options['default'] == $value)?'selected':'';
			
			$toreturn .= "<option value='".$value."' ".$selected.">".$display."</option>";
		
		}
		
		$toreturn .= "</select></div>";
		
		if( $list_form ) {
			
			$toreturn .= $list_form;
			
		}
		
		return $toreturn;
	
	}
	
	function radio( $name, $inputs, $options = array( ) ) {
	
		$toreturn = '';
		
		$toreturn .= "<input type='hidden' id='".$name."-id' name='".$name."' value=''/>";
		
		foreach( $inputs as $value => $label ) {
		
			$checked = ($_POST[$name]==$value)?'checked':'';
		
			$toreturn .= "<li><label for='input_".$name."'>";
			$toreturn .= "<input type='radio' id='".$name."-id' class='radio_input' name='".$name."' value=\"".$value."\" ".$checked."> ";
			$toreturn .= $label."</label></li>";
			
		}
		
		return $toreturn;
	
	}
	
	function tooth_chart( $name, $selected ) {
	
		$selected_array = explode( ',', $selected );
	
		$deciduous_top = array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' );
	
		$toreturn .= "<div class='tooth-chart'>
			
				<div class='row deciduous-top'>
				
					<div class='col-xs-6 deciduous-top-left'>";
				
		foreach( $deciduous_top as $tooth ) {
		
			if( $tooth == 'F' ) {
				
				$toreturn .= "</div><div class='col-xs-6 deciduous-top-right'>";
				
			}
				
			$toreturn .= "<a id='" . $tooth . "' href='#' class='tooth deciduous-tooth " . (in_array( $tooth, $selected_array )?'selected':'') . "'>" . $tooth . "</a>";
			
		}
				
		$toreturn .= "</div></div>
				
				<div class='row permanent-top'>
				
					<div class='col-xs-6 permanent-top-left'>";
				
					for( $i=1;$i<17;$i++ ) {
					
						if( ( $i > 6 && $i < 11 ) || ( $i > 22 && $i < 27 ) ) {
							
							$shape = 'frisbee';
							
						} else if( $i == 6 || $i == 11 ) {
							
							$shape = 'circle';
							
						} else {
							
							$shape = 'square';
							
						}
						
						if( $i == 9 ) {
				
							$toreturn .= "</div><div class='col-xs-6 permanent-top-right'>";
							
						}
					
						$toreturn .= "<a id='" . $i . "' href='#' class='tooth permanent-tooth " . $shape . " " . (in_array( $i, $selected_array )?'selected':'') . "'>" . $i . "</a>";
					}
					
		$toreturn .= "</div>
				
				<div class='row permanent-bottom'>
				
					<div class='col-xs-6 permanent-bottom-left'>";
				
					for( $i=32;$i>16;$i-- ) {
					
							if( ( $i > 6 && $i < 11 ) || ( $i > 22 && $i < 27 ) ) {
								
								$shape = 'frisbee';
								
							} else if( $i == 22 || $i == 27 ) {
								
								$shape = 'circle';
								
							} else {
								
								$shape = 'square';
								
							}
							
							if( $i == 24 ) {
				
								$toreturn .= "</div><div class='col-xs-6 permanent-bottom-right'>";
								
							}
							
					
						$toreturn .= "<a id='" . $i . "' href='#' class='tooth permanent-tooth " . $shape . " " . (in_array( $i, $selected_array )?'selected':'') . "'>" . $i . "</a>";
					
					}
				
		$toreturn .= "</div></div>";
				
		$deciduous_bottom = array( 'T', 'S', 'R', 'Q', 'P', 'O', 'N', 'M', 'L', 'K' );
				
		$toreturn .= "<div class='row deciduous-bottom'>
						<div class='col-xs-6 deciduous-bottom-left'>";
		
		foreach( $deciduous_bottom as $tooth ) {
		
			if( $tooth == 'O' ) {
				
				$toreturn .= "</div><div class='col-xs-6 deciduous-bottom-right'>";
				
			}
				
			$toreturn .= "<a id='" . $tooth . "' href='#' class='tooth deciduous-tooth " . (in_array( $tooth, $selected_array )?'selected':'') . "'>" . $tooth . "</a>";
			
		}
				
		$toreturn .= "</div><input type='hidden' name='" . $name . "' class='tooth-chart-val' value='" . $selected . "'>
			
			</div></div></div>";
			
		return $toreturn;
	
	}
	
	function custom_select( $name, $inputs, $options ) {
	
		$toreturn = '';
		
		$default = $options['default']?$options['default']:0;
		
		$name_id = str_replace( array( '[', ']' ), '', $name );
		
		if( count( $inputs ) ) {
			
			foreach( $inputs as $value => $label ) {
							
				$toreturn .= "<a href='javscript:void();' id='multi-select-" . $name_id . "-" . $value . "' class='multi-selector multi-select-" . $name . " " . ( ( $default == $value )?'selected':'') . " " . ( $options['class']?$options['class']:'' ) . "' data-value='" . $value . "'>" . $label . "</a>";
				
			}
			
		}
		
		
		$toreturn .= ( "<input type='hidden' name='" . $name . "' id='multi-select-value-" . $name_id . "' class='" . ( $options['required']?'required':'' ) . "' value='" . $default . "'>" );
		
		return $toreturn;
		
	}
	
	function doctor_picker( $name, $doctor_options, $doctor_pics, $selected = '' ) {
		
		$toreturn = '<ul id="' . $name . '" class="doctor-picker"><label for="input_' . $name . '"></label>';
		
		foreach( $doctor_options as $id => $dr_name ) {
		
			if( count( $doctor_options ) == 1 ) {
				
				$selected = $id;
				
			}
			
			$toreturn .= '<li class="' . ( ( $selected == $id )?'selected':'' ) . '"><a href="#" data-attr="' . $id . '">' . AppFunctions::getAvatar( $dr_name, $doctor_pics[ $id ] ) . '<span>' . $dr_name . '</span></a><li>';
			
		}
		
		$toreturn .= '<input type="hidden" name="' . $name . '" class="doctor-picker-value required" value="' . $selected . '"></ul>';
		
		return $toreturn;
		
	}
	
	function checkbox( $name, $inputs=array(), $options=array() ) {
	
		$toreturn = '';
		
		if( $options['label'] ) {
		
			$toreturn = "<span class='help-block'><strong>" . $options['label'] . "</strong></span>";
		
		}
		
		foreach( $inputs as $value => $label ) {
			
			$toreturn .= "<label class='checkbox'>
							  <input type='checkbox' id='".$name.((count($inputs)>1)?$value:'')."-id' value='" . $value. "' name='".$name.((count($inputs)>1)?'[]':'')."' " . (in_array( $value, $options['default'] )?'checked':'') . "/>" . $label .
						  "</label>";
			
		}
				
		return $toreturn;
	
	}
	

}

?>