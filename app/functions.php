<?

class AppFunctions {

	public function setSession( $page, $fields = array() ) {
		
		// loop through and cache get / session variables
		foreach( $fields as $field ) {
			
			if( isset( $_GET[ $field ] ) ) {
				
				$_SESSION[ $page ][ $field ] = $_GET[ $field ];
				
			}
			
			if( isset( $_SESSION[ $page ][ $field ] ) ) {
				
				$_GET[ $field ] = $_SESSION[ $page ][ $field ];
				
			}
			
		}
		
	}
	
	public function generateRandomPassword() {
		
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
	    
	}

	public function sortable( $name, $label ) {
		
		$query = array();
		
		$exclude = array( 'sort', 'url' );
		
		if( is_array( $_GET ) ) {
		
			foreach( $_GET as $field => $value ) {
				
				if( !in_array( $field, $exclude ) ) {
					
					$query[] = $field . '=' . $value;
					
				}
				
			}	
			
		}
		
		if( $_GET['sort'] == $name ) {
			
			$caret = "<b class='caret'></b>";
			$query[] =  'sort=' . $name . ' desc';
			
		} elseif( $_GET['sort'] == ( $name . ' desc' ) ) {
			
			$caret = "<b class='caret caret-reversed'></b>";
			$query[] = 'sort=' . $name;
			
		} else {
			
			$query[] = 'sort=' .$name;
			
		}
		
		return "<a href='?" . implode( '&', $query ) . "'>" . $label . " " . $caret . "</a>";
		
	}
	
	public function formatDateTime( $datetime ) {
	
		if( !$datetime || $datetime == '1970-01-01 01:00:00' || $datetime == '1969-12-31 19:00:00' ) {
			
			return '';
			
		}
		
		return date( 'm/d/Y g:ia', strtotime( $datetime ) );
		
	}
	
	public function getAge( $birthdate ) {
	
		if( $birthdate == '0000-00-00' || !$birthdate ) {
			
			return '';
			
		}
		
		return floor( ( time() - strtotime( $birthdate ) ) / ( 60 * 60 * 24 * 365 ) );
		
	}
	
	public function getAvatar( $name, $image ) {
	
		if( $image ) {
			
			return '<img src="/assets/images/uploads/thumbnails/' . $image . '" class="avatar">';
			
		}
		
		$name_parts = explode( ' ', $name );
		
		return '<div class="doctor-avatar"><span>' . substr( $name_parts[0], 0, 1 ) . ' ' .  substr( $name_parts[1], 0, 1 ) . '</span></div>';
		
	}
	
	public function formatUrl( $url ) {
		
		$url = str_replace( 'http://', '', $url );
		
		return "<a href='http://" . $url . "' target='_blank'>" . $url . "</a>";
		 
	}
	
	public function formatName( $string ) {
		
		return ucwords( strtolower( $string ) );
		
	}

	public function formatTime( $numeric ) {
		
		$formatted = floor( $numeric ) % 12;
		
		//echo( $numeric . ': ' . $formatted . '<br>' );
		
		$formatted = ( $formatted == 0 )?'12':$formatted;
		
		$formatted .= (floor( $numeric ) == $numeric)?':00':':30';
			
		$formatted .= ( $numeric < 12 )?'am':'pm';
		
		return $formatted;
		
	}
	
	public function generateRandomColor() {
	
		$white = 255;
	
	    $red 	= ( rand( 0, 256 ) + $white ) / 2;
	    $green 	= ( rand( 0, 256 ) + $white ) / 2;
	    $blue 	= ( rand( 0, 256 ) + $white ) / 2;
		
	    return $this->rgb2html( $red, $green, $blue );
	    
	}
	
	public function rgb2html($r, $g=-1, $b=-1) {
	
	    if ( is_array($r) && sizeof( $r ) == 3 ) {
		    
		    list($r, $g, $b) = $r;
		    
	    }       
	
	    $r = intval($r); 
	    $g = intval($g);
	    $b = intval($b);
	
	    $r = dechex($r<0?0:($r>255?255:$r));
	    $g = dechex($g<0?0:($g>255?255:$g));
	    $b = dechex($b<0?0:($b>255?255:$b));
	
	    $color = (strlen($r) < 2?'0':'').$r;
	    $color .= (strlen($g) < 2?'0':'').$g;
	    $color .= (strlen($b) < 2?'0':'').$b;
	    
	    return '#'.$color;
	    
	}
	
	public function convertText( $text ) {
	
		$text = nl2br( $text );
		
		return ereg_replace("[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]","<a href=\"\\0\" target=\"_blank\">\\0</a>", $text);
				
	}
	
	public function send_mail($to, $body, $subject, $fromaddress, $fromname, $attachments=false) {
	
		try {
		
			$to_addresses = array();
			
			$to_list = explode( ',', $to );
			
			foreach( $to_list as $address ) {
				
				$to_addresses[] = array(
						                'email' => trim( $address ),
						                'type' => 'to'
						            );
			}
		
			//$api_key = '5W1vO_bQNFAYs_vcvEQUkQ'; // Reese
			$api_key = 'DBhajpNWtA2pA4ETHfn8MA'; // Austin Test			
			
		    $mandrill = new Mandrill( $api_key );
		    
		    $message = array(
		        'html' => $body,
		        'text' => strip_tags( $body, '<p><a>' ),
		        'subject' => $subject,
		        'from_email' => $fromaddress,
		        'from_name' => $fromname,
		        'to' => $to_addresses,
		        'headers' => array('Reply-To' => $fromaddress),
		        'important' => false,
		        'track_opens' => null,
		        'track_clicks' => null,
		        'auto_text' => null,
		        'auto_html' => null,
		        'inline_css' => null,
		        'url_strip_qs' => null,
		        'preserve_recipients' => null,
		        'view_content_link' => null,
		        //'bcc_address' => 'reesebraces@gmail.com',
		        'tracking_domain' => null,
		        'signing_domain' => null,
		        'return_path_domain' => null,
		        'merge' => true );
		        
		     // add in any extra attachments
		     if( $attachments && count( $attachments ) ) {
		     
		     	foreach( $attachments as $attachment ) {
		     	
			     	$message['attachments'][] = array(
				                'type' => $attachment['content_type'],
				                'name' => $attachment['name'],
				                'content' => chunk_split( base64_encode( $attachment['file'] ) )
				            );
		     	}
			     
			     
		     }
		//        'global_merge_vars' => array(
		//            array(
		//                'name' => 'merge1',
		//                'content' => 'merge1 content'
		//            )
		//        ),
		//        'merge_vars' => array(
		//            array(
		//                'rcpt' => 'recipient.email@example.com',
		//                'vars' => array(
		//                    array(
		//                        'name' => 'merge2',
		//                        'content' => 'merge2 content'
		//                    )
		//                )
		//            )
		//        ),
		//        'tags' => array('password-resets'),
		//        'subaccount' => 'customer-123',
		//        'google_analytics_domains' => array('example.com'),
		//        'google_analytics_campaign' => 'message.from_email@example.com',
		//        'metadata' => array('website' => 'www.silo-beaufort.com'),
		//        'recipient_metadata' => array(
		//            array(
		//                'rcpt' => 'recipient.email@example.com',
		//                'values' => array('user_id' => 123456)
		//            )
		//        ),
		//        'images' => array(
		//            array(
		//                'type' => 'image/png',
		//                'name' => 'IMAGECID',
		//                'content' => 'ZXhhbXBsZSBmaWxl'
		//            )
		//        )

		    $async = false;

		    $result = $mandrill->messages->send($message, $async);
		    
		    //print_r($result);
		    
		} catch(Mandrill_Error $e) {
		    // Mandrill errors are thrown as exceptions
		    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		    throw $e;
		}
		
		return true;

	} // send_mail
	
//	public function send_mail($to, $body, $subject, $fromaddress, $fromname, $attachments=false) {
//
//	  $eol="\r\n";
//
//	  $mime_boundary=md5(time());
//
//	
//
//	  # Common Headers
//
//	  $headers .= "From: ".$fromname."<".$fromaddress.">".$eol;
//	  
//	  //if( !strstr( $subject, 'configured' ) ) {
//	  
//	  	$headers .= 'Bcc: reesebraces@gmail.com'.$eol;
//	  	//$headers .= 'Bcc: aducworth@gmail.com'.$eol;
//		
//	  //}
//
//	  $headers .= "Reply-To: ".$fromname."<".$fromaddress.">".$eol;
//
//	  $headers .= "Return-Path: ".$fromname."<".$fromaddress.">".$eol;    // these two to set reply address
//
//	  $headers .= "Message-ID: <".time()."-".$fromaddress.">".$eol;
//
//	  $headers .= "X-Mailer: PHP v".phpversion().$eol;          // These two to help avoid spam-filters
//
//	
//
//	  # Boundry for marking the split & Multitype Headers
//
//	  $headers .= "MIME-Version: 1.0".$eol;
//
//	  $headers .= "Content-Type: multipart/mixed; boundary=\"".$mime_boundary."\"".$eol.$eol;
//
//	
//
//	  # Open the first part of the mail
//
//	  $msg = "--".$mime_boundary.$eol;
//
//	  
//
//	  $htmlalt_mime_boundary = $mime_boundary."_htmlalt"; //we must define a different MIME boundary for this section
//
//	  # Setup for text OR html -
//
//	  $msg .= "Content-Type: multipart/alternative; boundary=\"".$htmlalt_mime_boundary."\"".$eol.$eol;
//
//	
//
//	  # Text Version
//
//	  $msg .= "--".$htmlalt_mime_boundary.$eol;
//
//	  $msg .= "Content-Type: text/plain; charset=iso-8859-1".$eol;
//
//	  $msg .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
//
//	  $msg .= strip_tags(str_replace("<br>", "\n", substr($body, (strpos($body, "<body>")+6)))).$eol.$eol;
//
//	
//
//	  # HTML Version
//
//	  $msg .= "--".$htmlalt_mime_boundary.$eol;
//
//	  $msg .= "Content-Type: text/html; charset=iso-8859-1".$eol;
//
//	  $msg .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
//
//	  $msg .= $body.$eol.$eol;
//
//	
//
//	  //close the html/plain text alternate portion
//
//	  $msg .= "--".$htmlalt_mime_boundary."--".$eol.$eol;
//
//	
//
//	  if ($attachments !== false)
//
//	  {
//
//		for($i=0; $i < count($attachments); $i++)
//
//		{
//
//
//
//			$f_contents = chunk_split(base64_encode($attachments[$i]["file"]));
//
//			
//
//			# Attachment
//
//			$msg .= "--".$mime_boundary.$eol;
//
//			$msg .= "Content-Type: ".$attachments[$i]["content_type"]."; name=\"".$attachments[$i]["name"]."\"".$eol;  // sometimes i have to send MS Word, use 'msword' instead of 'pdf'
//
//			$msg .= "Content-Transfer-Encoding: base64".$eol;
//
//			$msg .= "Content-Description: ".$attachments[$i]["name"].$eol;
//
//			$msg .= "Content-Disposition: attachment; filename=\"".$attachments[$i]["name"]."\"".$eol.$eol; // !! This line needs TWO end of lines !! IMPORTANT !!
//
//			$msg .= $f_contents.$eol.$eol;
//
//		  //}
//
//		}
//
//	  }
//
//	  # Finished
//	  $msg .= "--".$mime_boundary."--".$eol.$eol;  // finish with two eol's for better security. see Injection.
//
//	  # SEND THE EMAIL
//	  
//	  ini_set(sendmail_from,$fromaddress);  // the INI lines are to force the From Address to be used !
//
//	 $mail_sent = mail('aducworth@gmail.com', $subject, $msg, $headers);	  
//	 // $mail_sent = mail($to, $subject, $msg, $headers,"-f$to");	  
//
//	  ini_restore(sendmail_from);
//
//	  return $mail_sent;
//
//	} // send_mail

}

?>