<?

class Auth {
	
	var $menu = array( 
						'offices' => array( 'offices' => 'List Offices', 'office' => 'Add Office' ),
						'doctors' => array( 'doctors' => 'List Doctors', 'doctor' => 'Add Doctor', 'import_doctors' => 'Import Doctors' ),
						//'referrals' => array( 'referrals' => 'List Referrals', 'schedule' => 'View Schedule' ),
						'reports' => array( 'site_activity' => 'Site Activity'
							//, 'report_by_location' => 'By Location', 'report_by_specialty' => 'By Specialty', 'report_by_doctor' => 'By Doctor' 
						),
						'users' => array( 'users' => 'List Users', 'user' => 'Add User' ),
						'account' => array( 'logout' => 'Logout' ) );
						
	var $office_menu = array( 
						'tasks' => 'Tasks',
						'patients' => 'Patients',
						'reports' => 'Reports',
						'favorites' => 'Make Referral',
						//'referrals' => array( 'referrals' => 'List Referrals', 'schedule' => 'View Schedule', 'favorites' => 'Favorites' ),
						'account' => array( 'office' => 'Office Profile', 'user' => 'User Profile', 'doctors' => 'My Doctors', 'logout' => 'Logout' ) );
						
	var $always_allow = array( 'login', 'logout', 'permission', 'index', 'forgot' );
	
	var $admin_access = array( 'users', 'offices' );
	
	var $user_types = array( 0 => 'Office User', 1 => 'Administrator' );
	
	var $show_search = array( 'offices', 'doctors', 'users', 'tasks', 'patients', 'favorites' );
	
	var $allow = array( 'login', 'db_setup', 'index', 'email_new_referral', 'referral_email', 'email_patient', 'signup', '_signup_info', '_search_npi', 'forgot' );
	
	var $homepage_views = array( 'tasks'			=> 'Task List',
								 'favorites'	=> 'Favorite Doctors' );

	function __construct() {
	
		$action = $_GET['url']?$_GET['url']:'index';
		
		//echo( $action );
	
		if( !$this->logged_in() && !in_array( $action, $this->allow ) ) {
		
			header( 'Location: /login' );
			exit;
		
		}
		
		$this->db = new DB;
		$this->db->connect();
		
		// check the database connection
		if( !$this->db->connection && $action != 'db_setup' ) {
			
			header( 'Location: /db_setup' );
			exit;
			
		}
		
		if( !$this->agreedToTerms() && $action != 'terms' && !in_array( $action, $this->allow ) ) {
			
			header( 'Location: /terms' );
			exit;
			
		}
		
		// if this is a new user, we need to set them up with an office
		if( $this->newUser() && ( $action != 'claim' && !in_array( $action, $this->allow ) ) ) {
			
			header( 'Location: /claim' );
			exit;
			
		}
		
		if( !$this->hasPermission( $action ) ) {
			
			header( 'Location: /' );
			exit;
			
		}
		
	}
	
	public function hasPermission( $url ) {
		
		if( in_array( $url, $this->admin_access ) && $_SESSION['logged_in_user']['admin'] == 0 ) {
			
			return false;
			
		}
		
		return true;
		
	}
	
	public function newUser() {
		
		if( $_SESSION['logged_in_user']['admin'] == 0 && !$_SESSION['logged_in_user']['office_id'] ) {
			
			return true;
			
		}
		
		return false;
		
	}
	
	public function agreedToTerms() {
		
		$t = new TermsAndConditions;
		
		$latest = $t->retrieve('one','*',' order by id desc');
		
		if( $_SESSION['logged_in_user']['terms_and_conditions'] == $latest['id'] ) {
			
			return true;
			
		}
		
		return false;
	}
	
	public function navigation( $type = 'regular' ) {
		
		$toreturn = '';
		
		if( $_SESSION['logged_in_user']['admin'] ) {
			
			$nav_menu = $this->menu;
			
		} else {
			
			$nav_menu = $this->office_menu;
			
		}
		
		foreach( $nav_menu as $name => $items ) {
		
			if( ( $name != 'account' && $type != 'account' ) || ( $name == 'account' && $type == 'account' ) ) {
			
				// if this has a submenu
				if( is_array( $items ) ) {
					
					// build the submenu first to make sure permissions allow for the top level
					$submenu = '';
					
					foreach( $items as $url => $value ) {
							
						if( $this->hasPermission( $url ) ) {
							
							$submenu .= '<li><a href="/' . $url . '">' . $value . '</a></li>';
							
						}
						
					}
					
					// include top menu if there is a submenu
					if( $submenu ) {
					
						// fill in logged in info
						if( $name == 'account' ) {
							
							$name = $_SESSION['logged_in_user']['fname'] . ' ' . $_SESSION['logged_in_user']['lname'];
							
						} else {
							
							$name = ucwords( str_replace( '_', ' ', $name ) );
							
						}
						
						$toreturn .= '<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">' . $name . ' <b class="caret"></b></a>
										<ul class="dropdown-menu">'
											
											. $submenu .
											
										'</ul>
									  </li>';
								  
					}
					
				// if this is a single item
				} else {
					
					if( $this->hasPermission( $name ) ) {
						
						$toreturn .= '<li><a href="/' . $name . '" class="'. $name . '">' . ( ( $name == 'tasks' && $_SESSION['unread_count'] )?"<span id='task-count' class='badge danger pull-left'>" . $_SESSION['unread_count'] . "</span> ":'' ) . $items . '</a></li>';
						
					}
					
				}
			
			}
			
		}
		
		return $toreturn;
		
	}
	
	public function logged_in( ) {
	
		if( $_SESSION['logged_in_user'] ) {
		
			return true;
			
		}
		
		return false;
	
	}
	
	public function login() {
	
		$this->db->table = 'users';
		
		$user = $this->db->retrieve( 'one', '*', "where email='" . $_POST['email'] . "' and password='" . md5( $_POST['password'] ) . "'" );
		
		if( $_POST['remember'] ) {
				
			//echo( 'trying to set' );
			//$_COOKIE['email'] = $_POST['email'];
			setcookie("email", $_POST['email']);
			
		} else {
			
			//unset( $_COOKIE['email'] );
			setcookie ("email", "", time() - 3600);
			
		}
		
		return $this->set_login_data( $user );
	
	}
	
	public function set_login_data( $user ) {
		
		if( $user['id'] ) {
		
			$_SESSION['logged_in_user'] = $user;
			
			return true;
			
		} else {
		
			unset( $_SESSION['logged_in_user'] );
			return false;
		
		}
		
	}
	
	public function logout() {
	
		$_SESSION = array();
		
		return true;
	
	}

}

?>