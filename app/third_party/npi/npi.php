<?

class NPI {
	
	var $base_url = 'http://www.bloomapi.com/api/search/npi?limit=100';
	
	public function call( $parameters = array() ) {
		
		$call_url = $this->base_url;
		
		if( $parameters['offset'] ) {
			
			$call_url .= '&offset=' . $parameters['offset'];
			
			unset( $parameters['offset'] );
			
		} else {
			
			$call_url .= '&offset=0';
			
		}
		
		if( count( $parameters ) > 0 ) {
			
			$call_url .= '&' . $this->formatParameters( $parameters );
			
		}
		
		//echo( $call_url );
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $call_url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$json_data = curl_exec($ch);
		curl_close($ch);
		
		return json_decode( $json_data );
		
	}
	
	public function formatParameters( $parameters ) {
		
		$toreturn = array();
		
		$index = 1;
		
		foreach( $parameters as $field => $value ) {
			
			$toreturn[] = 'key' . $index . '=' . $field;
			$toreturn[] = 'op' . $index . '=eq';
			$toreturn[] = 'value' . $index . '=' . $value;
			
			$index++;
			
		}
		
		return implode( '&', $toreturn );
		
	}
	
}

?>