<?

session_start();

include 'npi.php';

// reese: 1841510401
// jim: 1346463866

//$_POST['npi'] = '1841510401';

$npi = new NPI;

$toreturn = 'false';

if( $_POST['npi'] ) {
	
	$data = $npi->call( array( 'npi' => $_POST['npi'] ) );

	if( $data->result ) {
		
		// save info in session 
		foreach( $data->result as $doctor ) {
		
			//print_r( $doctor );
			
			$_SESSION['signup_first_name'] = formatInfo( $doctor->first_name );
			$_SESSION['signup_last_name'] = formatInfo( $doctor->last_name );
			$_SESSION['signup_credentials'] = $doctor->credential;
			
			$_SESSION['signup_address'] = formatInfo( $doctor->practice_address->address_line );
			$_SESSION['signup_address2'] = formatInfo( $doctor->practice_address->address_details_line );
			$_SESSION['signup_city'] = formatInfo( $doctor->practice_address->city );
			$_SESSION['signup_state'] = $doctor->practice_address->state;
			$_SESSION['signup_zipcode'] = formatInfo( $doctor->practice_address->zip );
			
			$toreturn = 'true';
			break;
			
		}
		
	}
	
}

//print_r( $_SESSION );

echo( $toreturn );

function formatInfo( $data ) {
	
	return ucwords( strtolower( $data ) );
	
}

//                    [credential] => D.M.D., M.S.
//                    [enumeration_date] => 2010-06-07T00:00:00.000Z
//                    [first_name] => JOSEPH
//                    [gender] => male
//                    [last_name] => MCELVEEN
//                    [last_update_date] => 2012-04-02T00:00:00.000Z
//                    [middle_name] => REESE
//                    [name_prefix] => DR.
//                    [npi] => 1841510401
//                    [practice_address] => stdClass Object
//                        (
//                            [address_details_line] => SUITE D
//                            [address_line] => 2057 CHARLIE HALL BLVD
//                            [city] => CHARLESTON
//                            [country_code] => US
//                            [phone] => 8433237281
//                            [state] => SC
//                            [zip] => 294146164
//                        )
//
//                    [provider_details] => Array
//                        (
//                            [0] => stdClass Object
//                                (
//                                    [healthcare_taxonomy_code] => 1223X0400X
//                                    [license_number] => 4594
//                                    [license_number_state] => SC
//                                    [taxonomy_switch] => yes
//                                )
//
//                        )


?>