<?

include('models/referrals.php');
include('models/users.php');
include('models/doctors.php');
include('models/offices.php');
include('models/favorites.php');
include('models/comments.php');
include('models/attachments.php');
include('models/referral_questions.php');
include('models/doctor_types.php');
include('models/doctor_hours.php');
include('models/doctor_referral_questions.php');
include('models/referral_views.php');
include('models/terms_and_conditions.php');
include('models/office_locations.php');
include('models/invitations.php');

class AppController {

	var $referral_statuses = array( 
									1		=> 'Pending',
									2		=> 'Appointment Scheduled',
									3		=> 'Patient Seen' );
	
	var $appointment_lengths = array( 
								30 	=> '30 Minutes', 
								60 	=> '1 Hour',
								90	=> '1.5 Hours',
								120 => '2 Hours' );
								
	var $appointment_hours = array( 
								7 	=> '7am', 
								8 	=> '8am',
								9	=> '9am',
								10  => '10am',
								11  => '11am',
								12  => '12pm',
								13  => '1pm',
								14  => '2pm',
								15  => '3pm',
								16  => '4pm',
								17  => '5pm',
								18  => '6pm',
								19  => '7pm');
									 
	function __construct() {
	
		$this->auth	= new Auth;
		$this->db = $this->auth->db;
		
		$this->filepath = getcwd();
		//$this->site_url = 'http://' . str_replace( 'admin.', '', $_SERVER['SERVER_NAME'] );
		$this->site_url = 'http://localhost:8888/';
		
		if( $this->db->connection && $_GET['url'] != 'db_setup' ) {
		
			$this->referrals = new Referrals;
			$this->users = new Users;
			$this->doctors = new Doctors;
			$this->offices = new Offices;
			$this->favorites = new Favorites;
			$this->comments = new Comments;
			$this->attachments = new Attachments;
			$this->doctor_types = new DoctorTypes;
			$this->doctor_hours = new DoctorHours;
			$this->referral_questions = new ReferralQuestions;
			$this->doctor_referral_questions = new DoctorReferralQuestions;
			$this->referral_views = new ReferralViews;
			$this->terms_and_conditions = new TermsAndConditions;
			$this->office_locations = new OfficeLocations;
			$this->invitations = new Invitations;

			$this->doctor_list = $this->doctors->retrieve('pair',"id,concat( fname, ' ', lname, ', ', credentials )",' order by lname, fname'); 
			$this->doctor_name_list = $this->doctors->retrieve('pair',"id,concat( fname, ' ', lname )",' order by lname, fname'); 
			$this->doctor_image_list = $this->doctors->retrieve('pair',"id,image"); 
			$this->doctors_by_office = $this->doctors->retrieve('pair',"office_id, count( id )",' group by office_id');
			$this->users_by_office = $this->users->retrieve('pair',"office_id, count( id )",' group by office_id');
			$this->offices_by_doctor = $this->doctors->retrieve('pair',"id, office_id");  
			
			$this->office_list = $this->offices->retrieve('pair','id,name',' order by name'); 
			$this->office_email_list = $this->offices->retrieve('pair','id,email'); 
			$this->recently_added_offices = $this->offices->retrieve('all','*',' order by created desc limit 5');
			$this->office_address_list = $this->office_locations->retrieve('pair',"office_id,concat( address, '<br>', city, ', ', state, ' ', zipcode )", ' group by office_id'); 
			$this->office_websites = $this->offices->retrieve('pair',"id,website"); 
			$this->office_faxes = $this->office_locations->retrieve('pair',"office_id,fax"," group by office_id order by id asc"); 
			$this->office_phones = $this->office_locations->retrieve('pair',"office_id,phone", " group by office_id order by id asc"); 
			$this->office_location_counts = $this->office_locations->retrieve('pair','office_id, count( id )',' group by office_id');
			
			$this->recently_added_referrals = $this->referrals->retrieve('all','*',' order by created desc limit 5'); 
			$this->referrals_by_doctor = $this->referrals->retrieve('pair',"referred_to, count( id )",' group by referred_to');
			
			$this->doctor_type_list = $this->doctor_types->retrieve('pair','id,name',' order by name'); 
			$this->office_types = $this->doctor_type_list;
			
			// set up referral questions
			$this->referral_questions = $this->referral_questions->retrieve('all','*',' order by doctor_type_id, name'); 
			$this->referral_question_list = array();
			$this->question_list = array();
			
			foreach( $this->referral_questions as $rq ) {
				
				$this->question_list[ $rq['id'] ] = $rq['name'];
				$this->referral_question_list[ $rq['doctor_type_id'] ][ $rq['id'] ] = $rq['name'];
				
			}
			
			$this->hour_list = array();
			
			for( $i=1;$i<13;$i++ ) {
				
				$value = ( $i < 10 )?('0'.$i):$i;
				
				$this->hour_list[ $value ] = $value;
				
			}
			
			$this->minute_list = array();
			
			for( $i=0;$i<60;$i=$i+5 ) {
				
				$value = ( $i < 10 )?('0'.$i):$i;
				
				$this->minute_list[ $value ] = $value;
				
			}
			
		
		}
		
		if( $_SESSION['logged_in_user']['office_id'] ) {
			
			$this->doctors_by_office_list = $this->doctors->retrieve('pair',"id,concat( fname, ' ', lname, ', ', credentials )",' where office_id = ' . $_SESSION['logged_in_user']['office_id'] . ' order by lname, fname'); 
			
			$this->office_referrals_by_doctor = $this->referrals->retrieve('pair',"referred_to, count( id )",' where referred_by = ' . $_SESSION['logged_in_user']['office_id'] . ' group by referred_to');
			
			$this->my_favorites = $this->favorites->retrieve('pair',"doctor_id, id",' where office_id = ' . $_SESSION['logged_in_user']['office_id']); 
			
			// if this isn't tasks the action, get the tasks action so we can load in the numbe of tasks
			if( $this->auth->logged_in() && $_GET['url'] != 'tasks' ) {
				
				$this->tasks();
				
			}
						
		}
			
	}
	
	public function index() {
	
		if( $_SESSION['logged_in_user']['id'] ) {
	
			if( $_SESSION['logged_in_user']['admin'] == 1 ) {
				
				header( 'Location: /dashboard' );
				exit;
				
			} else {
				
				if( $_SESSION['logged_in_user']['homepage'] == 'tasks' ) {
					
					header( 'Location: /tasks' );
					exit;
					
				} else {
					
					header( 'Location: /favorites' );
					exit;
					
				}
				
			}
		
		}
			
	}
	
	public function _mark_as_read() {
		
		// update last viewed timestamp
		echo( json_encode( array( 'result' => $this->referral_views->updateView( $_POST['id'] ) ) ) );
		exit;
		
	}
	
	public function import_doctors() {
		
		if( $_POST['zipcode'] ) {
			
			$npi = new NPI;

			$data = $npi->call( array( 'practice_address.zip' => $_POST['zipcode'] ) );
			
			$this->_process_import_results( $data );
			
			// see if there are multiple pages and process
			$count = $data->meta->rowCount;
			$pages = ceil( $count / 100 );
			
			for( $i=1; $i<$pages; $i++ ) {
				
				$data = $npi->call( array( 'practice_address.zip' => $_POST['zipcode'], 'offset' => ( $i * 100 ) ) );
				
				$this->_process_import_results( $data );
				
			}
							
			exit;
			
		}
		
	}
	
	public function _process_import_results( $data ) {
		
		if( $data->result ) {
				
			// get dr info
			foreach( $data->result as $doctor ) {
				
				$creds[] = $doctor->credential;
				
				$credential = str_replace( '.', '', $doctor->credential );
			
				// make sure this is an individual and a dentist
				//if( $doctor->type == 'individual' && ( strstr( $credential, 'DDS' ) || strstr( $credential, 'DMD' ) ) ) {
				if( 1 == 1 ) {
					
					
				
					// find the dr locally
					$local_doctor = $this->doctors->retrieve( 'one', '*', " where npi='" . trim( $doctor->npi ) . "'" );
					
					echo( "<p> where npi='" . trim( $doctor->npi ) . "'</p>" );
					
					// if already imported, do nothing
					if( $local_doctor['id'] ) {
						
						echo( 'already imported' );
						
						print_r( $doctor );
						
					} else { // otherwise, see if this office has already been imported
						
						$local_office = $this->office_locations->retrieve( 'one', '*', " where address='" . $doctor->practice_address->address_line . "' and address2='" . $doctor->practice_address->address_details_line . "' and city='" . $doctor->practice_address->city . "' and state='" . $doctor->practice_address->state . "' and zipcode='" . substr( $doctor->practice_address->zip, 0, 5 ) . "'");
						
						// get the id if already imported
						if( $local_office['id'] ) {
							
							echo( 'office already imported' );
							
						} else {
							
							$office_data = array( 
													'address' 	=> AppFunctions::formatName( $doctor->practice_address->address_line ),
													'address2'	=> AppFunctions::formatName( $doctor->practice_address->address_line ),
													'city'		=> AppFunctions::formatName( $doctor->practice_address->city ),
													'state'		=> AppFunctions::formatName( $doctor->practice_address->state ),
													'zipcode'	=> AppFunctions::formatName( substr( $doctor->practice_address->zip, 0, 5 ) ),
													'fax'		=> AppFunctions::formatName( $doctor->practice_address->fax ),
													'phone'		=> AppFunctions::formatName( $doctor->practice_address->phone )  );
						}
						
						print_r( $doctor );
						
					}
					
					
				}
								
								
			}
			
		}
		
		echo( 'c r e d' );
		print_r( $creds );
		
	}
	
	public function download() {
	
		if( $_GET['id'] ) {
		
			$this->result = $this->referrals->retrieve( 'one', '*', ' where id = ' . $_GET['id'] ); 
			
			// set the doctor info
			$this->doctor_info = $this->doctors->retrieve( 'one', '*', ' where id = ' . $this->result['referred_to'] );
			
			$this->doctor_questions = $this->doctor_referral_questions->retrieve( 'all', '*', ' where doctor_id = ' . $this->result['referred_to'] ); 
			
			$this->doctor_locations = $this->office_locations->retrieve('all','*', ' where office_id = ' . $this->doctor_info['office_id'] );
			
			$this->dr_q_list = array();
		
			foreach( $this->doctor_questions as $q ) {
				
				$this->dr_q_list[ $q['referral_question_id'] ] = $this->question_list[ $q['referral_question_id'] ];
				
			}
			
			$this->result['attachments'] = $this->attachments->retrieve( 'all', '*', ' where referral_id = ' . $_GET['id'] ); 
																
			//require_once('tcpdf_include.php');
			require_once( 'app/third_party/tcpdf/tcpdf.php' );
			
			// create new PDF document
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			
			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('The Referrad Pad');
			$pdf->SetTitle('Patient Referral Card');
			$pdf->SetSubject('Patient Referral Card');
			
			// set default header data
			$pdf->SetHeaderData( '/assets/images/email-logo.png', 80, '', '', array(0,0,0), array(255,255,255));
			$pdf->setFooterData(array(0,0,0), array(255,255,255));
			
			// set header and footer fonts
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			
			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			
			// set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			
			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			
			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
			
			// add a page
			$pdf->AddPage();
			
			// set color for background
			$pdf->SetFillColor(255, 255, 127);
			
			// set default widths
			$column_1_width = 45;
			$column_2_width = 85;
			
			// Multicell test
			$pdf->MultiCell($column_1_width, 5, 'Patient Id:', 0, 'L', 0, 0, '', '', true);
			$pdf->MultiCell($column_2_width, 5, $this->result['patient_id'], 0, 'L', 0, 1, '', '', true);
			
			$pdf->MultiCell($column_1_width, 5, 'Patient Name:', 0, 'L', 0, 0, '', '', true);
			$pdf->MultiCell($column_2_width, 5, $this->result['patient_name'], 0, 'L', 0, 1, '', '', true);
			
			if( $this->result['patient_dob'] != '0000-00-00' && $this->result['patient_dob'] != '1969-12-31' ) {
				
				$pdf->MultiCell($column_1_width, 5, 'Patient DOB:', 0, 'L', 0, 0, '', '', true);
				$pdf->MultiCell($column_2_width, 5, ( date( 'm/d/Y', strtotime( $this->result['patient_dob'] ) ) . ' ( ' . AppFunctions::getAge( $this->result['patient_dob'] ) . ' )' ), 0, 'L', 0, 1, '', '', true);
				
			}
			
			$pdf->MultiCell($column_1_width, 5, 'Patient Phone:', 0, 'L', 0, 0, '', '', true);
			$pdf->MultiCell($column_2_width, 5, $this->result['patient_phone'], 0, 'L', 0, 1, '', '', true);
			
			$pdf->MultiCell($column_1_width, 5, 'Patient Email:', 0, 'L', 0, 0, '', '', true);
			$pdf->MultiCell($column_2_width, 5, $this->result['patient_email'], 0, 'L', 0, 1, '', '', true);
			
			$pdf->MultiCell($column_1_width, 5, 'Patient Address:', 0, 'L', 0, 0, '', '', true);
			$pdf->MultiCell($column_2_width, 5, $this->result['patient_address1'], 0, 'L', 0, 1, '', '', true);
			$pdf->MultiCell($column_1_width, 5, '', 0, 'L', 0, 0, '', '', true);
			$pdf->MultiCell($column_2_width, 5, ( $this->result['patient_city'] . ', ' . $this->result['patient_state'] . ' ' . $this->result['patient_zipcode'] ), 0, 'L', 0, 1, '', '', true);

			
			$pdf->MultiCell($column_1_width, 5, 'Referred By:', 0, 'L', 0, 0, '', '', true);
			$pdf->MultiCell($column_2_width, 5, $this->doctor_list[ $this->result['referred_by'] ] , 0, 'L', 0, 1, '', '', true);
			
			$pdf->MultiCell($column_1_width, 5, 'Referred To:', 0, 'L', 0, 0, '', '', true);
			$pdf->MultiCell($column_2_width, 5, $this->doctor_list[ $this->result['referred_to'] ] , 0, 'L', 0, 1, '', '', true);
			
			if( $this->result['tooth_chart'] ) {
				
				$pdf->MultiCell($column_1_width, 5, 'Tooth Chart:', 0, 'L', 0, 0, '', '', true);
				$pdf->MultiCell($column_2_width, 5, $this->result['tooth_chart'] , 0, 'L', 0, 1, '', '', true);
				
			}
			
			$selected = explode( ',', $this->result['referral_questions'] );
	  		$tolist = array();
	  		
	  		foreach( $selected as $s ) {
		  		
		  		$tolist[] = $this->question_list[ $s ];
		  		
	  		}
			
			if( count( $tolist ) ) {
				
				$pdf->MultiCell($column_1_width, 5, 'Prescription:', 0, 'L', 0, 0, '', '', true);
				$pdf->MultiCell($column_2_width, 5, implode( ', ', $tolist ) , 0, 'L', 0, 1, '', '', true);
				
			}					  		
			
			$appointment = AppFunctions::formatDateTime( $this->result['appointment'] );
			
			if( $appointment ) {
				
				$appointment = date( 'F d, Y g:ia', strtotime( $appointment ) );
				
				$pdf->MultiCell($column_1_width, 5, 'Appointment:', 0, 'L', 0, 0, '', '', true);
				$pdf->MultiCell($column_2_width, 5, $appointment , 0, 'L', 0, 1, '', '', true);
				
			}
			
			$pdf->MultiCell($column_1_width, 5, 'Notes:', 0, 'L', 0, 0, '', '', true);
			$pdf->MultiCell($column_2_width, 25, $this->result['notes'] , 0, 'L', 0, 1, '', '', true, 1);
			
			if( count( $this->result['attachments'] ) ) {
			
				$pdf->MultiCell($column_1_width, 5, "\nFiles:\n\n", 0, 'L', 0, 1, '', '', true);
				
				foreach( $this->result['attachments'] as $attachment ) {
					
					if( strstr( $attachment['filename'], '.jpg' ) || strstr( $attachment['filename'], '.png' ) ) {
						
						$pdf->Image('/assets/images/uploads/thumbnails/'.$attachment['filename'], '', '', 0, 0, '', 'http://' . $_SERVER['SERVER_NAME'] . '/assets/images/uploads/tmp/'.$attachment['filename'], 'T', false, 300, '', false, false, 0, false, false, false);
						
					} else {
						
						$pdf->addHtmlLink( 'http://' . $_SERVER['SERVER_NAME'] . '/assets/images/uploads/tmp/'.$attachment['filename'], $attachment['name'] );	
						$pdf->Ln();
					}
					
					
				}
				
			}
						  			
			$pdf->Output($this->result['patient_id'].'.pdf', 'D');
			exit;
		
		}
		
		
	}
	
	public function schedule() {
	
		$where = " where 1=1";
		
		if( $_GET['search'] ) {
			
			$where .= " and patient_name like '%" . $_GET['search'] . "%'";
			
		}
		
		// do restriction for office user
		if( $_SESSION['logged_in_user']['office_id'] ) {
			
			// filter for referrals made to drs in this office
			$dr_filter = array();
			
			foreach( $this->doctors_by_office_list as $id => $name ) {
				
				$dr_filter[] = 'referred_to=' . $id;
				
			}
			
			// and referrals made to this office
			if( count( $dr_filter ) ) {
				
				$where .= " and (  " . implode( ' or ', $dr_filter ) . " ) ";
				
			} else {
				
				// or nothing
				$where .= " and 1=2 ";
				
			}
			
		}
			
		$this->referrals = $this->referrals->retrieve('all','*',$where . ' order by appointment'); 
	
	}
	
	public function permission() {
			
		$this->message = "Sorry, but you do not have permission to access this area.";
	
	}
	
	public function db_setup() {
		
		// do the initial check to make sure tables are set up
		$this->db->initial_setup();
		
	}
		
	public function login() {
	
		if( count( $_POST ) > 0 ) {
		
			if( $this->auth->login() ) {
			
				header( 'Location: /' );
				exit;
				
			} else {
			
				$this->message = "Error logging in.";
			
			}
		
		}
	
	}
	
	public function forgot() {
	
		if( count( $_POST ) > 0 ) {
		
			if( $this->users->reset_password( $_POST['email'] ) ) {
			
				$this->success = "Your password has been reset, and we have sent you an email with your new password. Please check your email and login.";
				
			} else {
			
				$this->message = "Sorry, we couldn't find your account. Please make sure you are using the correct email and try again.";
			
			}
		
		}
	
	}
	
	public function logout() {
	
		$this->auth->logout();
		header( 'Location: /' );
		exit;
	
	}
	
	public function users() {
	
		$sort = $_GET['sort']?$_GET['sort']:'lname';
		$_GET['sort'] = $sort;
		
		$where = " where 1=1";
	
		if( $_GET['search'] ) {
			
			$where .= " and ( fname like '%" . $_GET['search'] . "%' or lname like '%" . $_GET['search'] . "%' or email like '%" . $_GET['search'] . "%' )";
			
		}
		
		// do restriction for office user
		if( $_GET['office'] ) {
			
			$where .= " and office_id = " . $_GET['office'];
			
		}
			
		$this->users = $this->users->retrieve('all','*',$where.' order by '.$sort); 
			
	}
	
	public function offices() {
	
		$sort = $_GET['sort']?$_GET['sort']:'name';
		$_GET['sort'] = $sort;
	
		if( $_GET['search'] ) {
			
			$where = " where name like '%" . $_GET['search'] . "%' or address like '%" . $_GET['search'] . "%' or city like '%" . $_GET['search'] . "%' or state like '%" . $_GET['search'] . "%'";
			
		}
			
		$this->offices = $this->offices->retrieve('all','*',$where.' order by ' . $sort); 
		
		$this->office_addresses = $this->office_locations->retrieve('pair',"office_id, concat( city, ', ', state )"); 
			
	}
						
	public function referrals() {
	
		$where = " where 1=1";
		
		$_GET['unread'] = $_GET['unread']?$_GET['unread']:'everything';
		$_GET['source'] = $_GET['source']?$_GET['source']:'to';
		
		if( $_GET['search'] ) {
			
			$where .= " and patient_id like '%" . $_GET['search'] . "%'";
			
		}
		
		if( $_GET['referral_status'] ) {
			
			$where .= " and referral_status = '" . $_GET['referral_status'] . "'";
			
		}
		
		// do restriction for office user
		if( $_SESSION['logged_in_user']['office_id'] ) {
			
			// filter for referrals made by this office
			$dr_filter = array();
			
			foreach( $this->doctors_by_office_list as $id => $name ) {
				
				if( $_GET['source'] == 'from' ) {
					
					$dr_filter[] = 'referred_by=' . $id;
					
				} else {
					
					$dr_filter[] = 'referred_to=' . $id;
					
				}
				
			}
			
			// and referrals made to this office
			if( count( $dr_filter ) ) {
				
				$where .= " and ( " . implode( ' or ', $dr_filter ) . " ) ";
				
			}
			
		}
			
		$this->referrals = $this->referrals->retrieve('all','*',$where . ' order by modified desc'); 
			
	}
	
	public function site_activity() {
		
		$where = " where 1=1";
		
		if( $_GET['from_date'] ) {
			
			$where .= " and created >= '" . date( 'Y-m-d 00:00:00', strtotime( $_GET['from_date'] ) ) . "'";
			
		}
		
		if( $_GET['to_date'] ) {
			
			$where .= " and created <= '" . date( 'Y-m-d 11:59:59', strtotime( $_GET['to_date'] ) ) . "'";
			
		}
		
		$this->referrals = $this->referrals->retrieve('all','*',$where . ' order by created asc'); 
		
	}
	
	public function tasks() {
	
		$where = " where 1=1";
		
		if( $_GET['search'] ) {
			
			$where .= " and patient_id like '%" . $_GET['search'] . "%'";
			
		}
		
		if( $_GET['referral_status'] ) {
			
			$where .= " and referral_status = '" . $_GET['referral_status'] . "'";
			
		}
		
		// do restriction for office user
		if( $_SESSION['logged_in_user']['office_id'] ) {
			
			// filter for referrals made by this office
			$dr_filter = array();
			
			foreach( $this->doctors_by_office_list as $id => $name ) {
				
				// updating this to show referral to or from this office
				$dr_filter[] = 'referred_by=' . $id;
				$dr_filter[] = 'referred_to=' . $id;
				
			}
			
			// and referrals made to this office
			if( count( $dr_filter ) ) {
				
				$where .= " and ( " . implode( ' or ', $dr_filter ) . " ) ";
				
			}
			
		}
			
		$this->results = $this->referrals->getUnread( $where ); 
			
	}
	
	public function reports() {
		
		$this->overtime = $this->referrals->referralsOverTime();
		
		print_r( $overtime );
	}
	
	public function doctors() {
	
		$sort = $_GET['sort']?$_GET['sort']:'lname';
		$_GET['sort'] = $sort;
	
		$where = " where 1=1";
		
		if( $_GET['search'] ) {
			
			$where .= " and ( fname like '%" . $_GET['search'] . "%' or lname like '%" . $_GET['search'] . "%' or credentials like '%" . $_GET['search'] . "%' ) ";
			
		}
		
		// do restriction for office user
		if( $_GET['office'] ) {
			
			$where .= " and office_id = " . $_GET['office'];
			
		}
		
		// do restriction for office user
		if( $_SESSION['logged_in_user']['office_id'] ) {
			
			$where .= " and office_id = " . $_SESSION['logged_in_user']['office_id'];
			
		}
			
		$this->doctors = $this->doctors->retrieve('all','*',$where . ' order by ' . $sort); 
			
	}
	
	public function admin_login() {
		
		$user = $this->users->retrieve('one','*'," where md5( id ) = '" . $_GET['id'] . "'");
		
		// otherwise, set the login data and forward
		if( $this->auth->set_login_data( $user ) ) {
			
			header( 'Location: /users' );
			exit;
			
		}
		
		header( 'Location: /users' );
		exit;
		
	}
	
	public function patients() {
	
		AppFunctions::setSession( 'patients', array( 'source', 'search', 'referral_status', 'specialist', 'sort' ) ); 
	
		// not setting a default for this, so we can do all
//		if( $_SESSION['logged_in_user']['office_id'] ) {
//			
//			$office_info = $this->offices->retrieve( 'one','*',' where id = ' . $_SESSION['logged_in_user']['office_id'] );
//			
//			// if this is a dentist office, set to from by default
//			if( $office_info['office_type'] == 1 ) {
//				
//				$_GET['source'] = $_GET['source']?$_GET['source']:'from';
//				
//			} else { // opposite for specifialists
//				
//				$_GET['source'] = $_GET['source']?$_GET['source']:'to';
//				
//			}
//			
//		}
	
		$sort = $_GET['sort']?$_GET['sort']:'id';
	
		$where = " where 1=1";
		
		if( $_GET['search'] ) {
			
			$where .= " and ( patient_id like '%" . $_GET['search'] . "%' ) ";
			
		}
		
		if( $_GET['referral_status'] ) {
			
			$where .= " and referral_status = '" . $_GET['referral_status'] . "'";
			
		}
		
		if( $_GET['specialist'] ) {
		
			$dr_filter = array();
			
			$specialists = $this->doctors->retrieve( 'all','*',' where doctor_type_id=' . $_GET['specialist'] );
			
			foreach( $specialists as $s ) {
				
				if( $_GET['source'] == 'to' ) {
					
					$dr_filter[] = 'referred_by=' . $s['id'];
					
				} elseif( $_GET['source'] == 'from' ) {
					
					$dr_filter[] = 'referred_to=' . $s['id'];
					
				} else {
					
					// adding both if there is no filter
					$dr_filter[] = 'referred_by=' . $s['id'];
					$dr_filter[] = 'referred_to=' . $s['id'];
					
				}
				 
			}
			
			// and referrals made to this office
			if( count( $dr_filter ) ) {
				
				$where .= " and ( " . implode( ' or ', $dr_filter ) . " ) ";
				
			} else {
				
				$where .= " and 1 = 2 ";
				
			}
			
		}
		
		// do restriction for office user
		if( $_SESSION['logged_in_user']['office_id'] ) {
			
			// filter for referrals made by this office
			$dr_filter = array();
			
			foreach( $this->doctors_by_office_list as $id => $name ) {
				
				if( $_GET['source'] == 'to' ) {
					
					$dr_filter[] = 'referred_to=' . $id;
					
				} elseif( $_GET['source'] == 'from' ) {
					
					$dr_filter[] = 'referred_by=' . $id;
					
				} else {
					
					$dr_filter[] = 'referred_to=' . $id;
					$dr_filter[] = 'referred_by=' . $id;
					
				}
				
			}
			
			// and referrals made to this office
			if( count( $dr_filter ) ) {
				
				$where .= " and ( " . implode( ' or ', $dr_filter ) . " ) ";
				
			}
			
		}
		
		$this->results = $this->referrals->retrieve('all','*',$where . ' order by ' . $sort); 
			
	}
	
	public function search() {
		
		$where = " where 1=1";
		
		if( $_GET['name'] ) {
		
			$tofilter = array();
		
			$parts = explode( ' ', $_GET['name'] );
			
			foreach( $parts as $part ) {
			
				$part = trim( $part );
				
				if( $part != '' ) {
					
					$tofilter[] = " ( fname like '%" . $part . "%' or lname like '%" . $part . "%' ) ";
					
				}
				
			}
			
			if( count( $tofilter ) ) {
				
				$where .= " and ( " . implode( " and ", $tofilter ) . " ) ";
				
			}
			
		}
		
		if( $_GET['type'] ) {
			
			$where .= " and ( doctor_type_id = '" . $_GET['type'] . "' ) ";
			
		}
		
		if( $_GET['location'] ) {
			
			$tofilter = array();
			
			$office_filter = array();
		
			$parts = explode( ',', $_GET['location'] );
			
			foreach( $parts as $part ) {
			
				$part = trim( $part );
				
				if( $part != '' ) {
					
					$office_filter[] = " ( address like '%" . $part . "' or city like '%" . $part . "' or state like '%" . $part . "' or zipcode like '%" . $part . "' ) ";
					
				}
				
			}
			
			if( count( $office_filter ) ) {
				
				$office_where = " where " . implode( " and ", $office_filter );
			
				$office_list = $this->offices->retrieve('all','*',$office_where);
				
				if( count( $office_list ) ) {
					
					foreach( $office_list as $ol ) {
						
						$tofilter[] = "office_id=" . $ol['id'];
						
					}
					
					$where .= " and ( " . implode( ' or ', $tofilter ) . " ) ";
					
				} else {
					
					$where .= " and 1=2";
					
				}
				
			}
			
		}
		
		// do restriction for office user
		if( $_SESSION['logged_in_user']['office_id'] ) {
			
			// removing this for now, so dr's can refer within their practice
			//$where .= " and office_id != " . $_SESSION['logged_in_user']['office_id'];
			
		}
		
		$this->results = $this->doctors->retrieve('all','*',$where . ' order by lname, fname'); 
			
	}
	
	public function _favorites_grid() {
		
		$this->favorites();
		
	}
	
	public function favorites() {
	
		$where = " where 1=1";
		
		if( $_GET['search'] ) {
			
			$where .= " and ( fname like '%" . $_GET['search'] . "%' or lname like '%" . $_GET['search'] . "%' ) ";
			
		}
		
		// do restriction for office user
		if( $_SESSION['logged_in_user']['office_id'] ) {
			
			$where .= " and office_id = " . $_SESSION['logged_in_user']['office_id'];
			
		}
			
		$this->favorites = $this->favorites->retrieve('all','*',$where . ' order by created desc'); 
			
	}
	
	public function initialize_npi() {
		
		$npi = new NPI;
	
		$doctors = $this->doctors->retrieve( 'all','*'," where npi is null" );
				
		foreach( $doctors as $doctor ) {
		
			$data = $npi->call( array( 'last_name' => $doctor['lname'], 'first_name' => $doctor['fname'], 'practice_address.state' => 'SC' ) );
			
			if( $data->result ) {
		
				// save info in session 
				foreach( $data->result as $provider ) {
				
					//print_r( $doctor );
					
					if( $provider->first_name && $provider->last_name ) {
						
						echo( '<h2>Matched ' . $provider->first_name . ' ' . $provider->last_name . '</h2>' );
						
						echo( '<pre>' );
						
						print_r( $provider );
						
						echo( '</pre>' );
						
						$this->doctors->save( array( 'id' => $doctor['id'], 'npi' => $provider->npi ) );
						
					} 
					
//					$_SESSION['signup_first_name'] = formatInfo( $doctor->first_name );
//					$_SESSION['signup_last_name'] = formatInfo( $doctor->last_name );
//					$_SESSION['signup_credentials'] = $doctor->credential;
//					
//					$_SESSION['signup_address'] = formatInfo( $doctor->practice_address->address_line );
//					$_SESSION['signup_address2'] = formatInfo( $doctor->practice_address->address_details_line );
//					$_SESSION['signup_city'] = formatInfo( $doctor->practice_address->city );
//					$_SESSION['signup_state'] = $doctor->practice_address->state;
//					$_SESSION['signup_zipcode'] = formatInfo( $doctor->practice_address->zip );
//					
//					$toreturn = 'true';
//					break;
					
				}
				
			} else {
		
				echo( '<p>Could not find ' . $doctor['fname'] . ' ' . $doctor['lname'] . '</p>' );
				
			}
			
		} 
		
		
		//print_r( $data );
		
		exit;
	
	}
	
	public function _search_npi() {
	
		$npi = new NPI;

		$toreturn = 'false';
		
		//$_POST['npi'] = $_GET['npi'];
		
		if( $_POST['npi'] ) {
		
			// reset the signup info to start off
			$_SESSION['signup'] = array();
		
			// start off by search our local database for the doctor
			$doctor = $this->doctors->retrieve( 'one','*'," where npi = '" . $_POST['npi'] . "'" );
			
			if( $doctor['id'] ) {
			
				$office = $this->offices->retrieve( 'one','*'," where id = '" . $doctor['office_id'] . "'" );
				
				$office_locations = $this->office_locations->retrieve( 'all','*'," where office_id = " . $doctor['office_id'] );
			
				$_SESSION['signup']['doctor'] = $doctor;
				
				$_SESSION['signup']['office'] = $office;
				
				$_SESSION['signup']['office_locations'] = $office_locations;
				
				$_SESSION['signup']['user'] = $this->users->retrieve( 'one','*'," where office_id = " . $doctor['office_id'] );;
								
				$toreturn = 'local';
				
				
			} else { // otherwise, search the npi api
			
				$data = $npi->call( array( 'npi' => $_POST['npi'] ) );
				
				if( $data->result ) {
					
					// save info in session 
					foreach( $data->result as $doctor ) {
					
						//print_r( $doctor );
						
						$_SESSION['signup']['doctor']['fname'] = AppFunctions::formatName(  $doctor->first_name );
						$_SESSION['signup']['doctor']['lname'] = AppFunctions::formatName( $doctor->last_name );
						$_SESSION['signup']['doctor']['credentials'] = $doctor->credential;
						
						$office_location = $this->office_locations->retrieve( 'one','*'," where address = '" . $doctor->practice_address->address_line . "' and city = '" . $doctor->practice_address->city . "' and state = '" . $doctor->practice_address->state . "' and zipcode = '" . substr( $doctor->practice_address->zip, 0, 5 ) . "'" );
						
						// if we found this office location, load in the office and any other locations
						if( $office_location['id'] ) {
							
							$office = $this->offices->retrieve( 'one','*'," where id = '" . $office_location['office_id'] . "'" );
				
							$office_locations = $this->office_locations->retrieve( 'all','*'," where office_id = " . $office_location['office_id'] );
							
							$_SESSION['signup']['office'] = $office;
				
							$_SESSION['signup']['office_locations'] = $office_locations;
							
						}
						
						$_SESSION['signup']['office_location']['address'] = AppFunctions::formatName( $doctor->practice_address->address_line );
						$_SESSION['signup']['office_location']['address2'] = AppFunctions::formatName( $doctor->practice_address->address_details_line );
						$_SESSION['signup']['office_location']['city'] = AppFunctions::formatName( $doctor->practice_address->city );
						$_SESSION['signup']['office_location']['state'] = $doctor->practice_address->state;
						$_SESSION['signup']['office_location']['zipcode'] = AppFunctions::formatName( $doctor->practice_address->zip );
						
						$toreturn = 'remote';
						break;
						
					}
					
				}
				
			}
			
		}

		echo( $toreturn );
		exit;
		
	}
		
	public function initialize_office_locations() {
		
		$offices = $this->offices->retrieve();
		
		foreach( $offices as $office ) {
			
			$data = array( 'office_id' => $office['id'], 'name' => $office['name'], 'address' => $office['address'], 'address2' => $office['address2'], 'city' => $office['city'], 'state' => $office['state'], 'zipcode' => $office['zipcode'], 'phone' => $office['phone'], 'fax' => $office['fax'], 'email' => $office['email'] );
			
			//$this->office_locations->save( $data );
		}
		
		exit;
		
	}
	
	public function office() {
	
		// do restriction for office user
		if( $_SESSION['logged_in_user']['office_id'] ) {
			
			$_GET['id'] = $_SESSION['logged_in_user']['office_id'];
			
		}
	
		if( count( $_POST ) > 0 ) {
								
			if( is_uploaded_file( $_FILES['image']['tmp_name'] ) ) {
				
//				if( $_FILES['image']['type'] == 'image/jpeg' ) {
					
					$_POST['image'] = time() . $this->attachments->clean_filename( $_FILES['image']['name'] );
					
					$path = str_replace( array( 'admin', 'app' ), array( '', 'assets' ), getcwd() ) . '/assets/images/uploads/';
					
					//echo( $path );
					
					move_uploaded_file( $_FILES['image']['tmp_name'], $path . 'tmp/' . $_POST['image'] ); 
					
					$resizeObj = new resize( $path . 'tmp/' . $_POST['image'] );
					$resizeObj -> resizeImage( 300, 473, 'landscape' );
					$resizeObj -> saveImage( $path . 'resized/' . $_POST['image'], 100 );
									
					$resizeObj = new resize( $path . 'tmp/' . $_POST['image'] );
					$resizeObj -> resizeImage( 100, 100, 'landscape' );
					$resizeObj -> saveImage( $path . 'thumbnails/' . $_POST['image'], 100 );
				
//				} else {
//					
//					$this->message = 'Invalid type of image. Please use a jpg.';
//					$_GET['id'] = $_POST['id'];
//					
//					echo( $this->message );
//					
//				}
			
			}
			
			if( is_uploaded_file( $_FILES['health_history']['tmp_name'] ) ) {
				
				$_POST['health_history'] = time() . $this->attachments->clean_filename( $_FILES['health_history']['name'] );
				
				$path = str_replace( array( 'admin', 'app' ), array( '', 'assets' ), getcwd() ) . '/assets/images/uploads/';
				
				//echo( $path );
				
				move_uploaded_file( $_FILES['health_history']['tmp_name'], $path . 'tmp/' . $_POST['health_history'] ); 				
			
			}
						
			if( $this->offices->save( $_POST ) ) {
			
				if( !$_POST['id'] ) {
				
					$_POST['id'] = mysql_insert_id();
					
					header( 'Location: /office_location?office_id=' . $_POST['id'] );
					exit;
				
				}
																						
				header( 'Location: /offices' );
				exit;
			
			}
		
		}
		
		if( $_GET['id'] ) {
		
			$this->result = $this->offices->retrieve( 'one', '*', ' where id = ' . $_GET['id'] ); 
			
			$this->result_locations = $this->office_locations->retrieve( 'all', '*', ' where office_id = ' . $_GET['id'] ); 
														
		}
		
	}
	
	public function office_location() {
	
		if( count( $_POST ) > 0 ) {
														
			if( $this->office_locations->save( $_POST ) ) {
			
				if( !$_POST['id'] ) {
				
					$_POST['id'] = mysql_insert_id();
				
				}
																						
				header( 'Location: /office?id=' . $_POST['office_id'] );
				exit;
			
			}
		
		}
		
		if( $_GET['id'] ) {
		
			$this->result = $this->office_locations->retrieve( 'one', '*', ' where id = ' . $_GET['id'] ); 
			
		}
		
	}
	
	public function terms() {
	
		if( count( $_POST ) > 0 ) {
		
			if( $this->users->save( $_POST ) ) {
			
				$_SESSION['logged_in_user']['terms_and_conditions'] = $_POST['terms_and_conditions'];																						
				header( 'Location: /' );
				exit;
			
			}
		
		}
		
		$this->result = $this->terms_and_conditions->retrieve( 'one', '*', ' order by id desc' ); 
		
	}
	
	public function doctor() {
	
		if( count( $_POST ) > 0 ) {
								
			if( is_uploaded_file( $_FILES['image']['tmp_name'] ) ) {
				
				if( $_FILES['image']['type'] == 'image/jpeg' ) {
					
					$_POST['image'] = time() . $this->attachments->clean_filename( $_FILES['image']['name'] );
					
					$path = str_replace( array( 'admin', 'app' ), array( '', 'assets' ), getcwd() ) . '/assets/images/uploads/';
					
					//echo( $path );
					
					move_uploaded_file( $_FILES['image']['tmp_name'], $path . 'tmp/' . $_POST['image'] ); 
					
					$resizeObj = new resize( $path . 'tmp/' . $_POST['image'] );
					$resizeObj -> resizeImage( 300, 473, 'landscape' );
					$resizeObj -> saveImage( $path . 'resized/' . $_POST['image'], 100 );
									
					$resizeObj = new resize( $path . 'tmp/' . $_POST['image'] );
					$resizeObj -> resizeImage( 100, 100, 'crop' );
					$resizeObj -> saveImage( $path . 'thumbnails/' . $_POST['image'], 100 );
				
				} else {
					
					$this->message = 'Invalid type of image. Please use a jpg.';
					$_GET['id'] = $_POST['id'];
					
					echo( $this->message );
					
				}
			
			}
			
			$referral_questions = $_POST['referral_questions'];
			unset( $_POST['referral_questions'] );
						
			if( $this->doctors->save( $_POST ) ) {
			
				if( !$_POST['id'] ) {
				
					$_POST['id'] = mysql_insert_id();
				
				}
				
				$this->db->query( 'delete from doctor_referral_questions where doctor_id = ' . $_POST['id'] );
				
				// try to save referral questions
				if( is_array( $referral_questions ) ) {
				
					foreach( $referral_questions as $question_id ) {
						
						if( $question_id ) {
							
							$this->doctor_referral_questions->save( array( 'referral_question_id' => $question_id, 'doctor_id' => $_POST['id'] ) );
							
						}
						
					}
				
				}
																						
				header( 'Location: /doctors' );
				exit;
			
			}
		
		}
		
		if( $_GET['id'] ) {
		
			$this->result = $this->doctors->retrieve( 'one', '*', ' where id = ' . $_GET['id'] ); 
			
			$this->result['doctor_hours'] = $this->doctor_hours->retrieve( 'all', '*', ' where doctor_id = ' . $_GET['id'] ); 
			
			$this->result['referral_questions'] = $this->doctor_referral_questions->retrieve( 'pair', 'id, referral_question_id', ' where doctor_id = ' . $_GET['id'] ); 
														
		}
		
	}
	
	public function patient() {
	
		if( count( $_POST ) > 0 ) {
								
			if( is_uploaded_file( $_FILES['image']['tmp_name'] ) ) {
				
				if( $_FILES['image']['type'] == 'image/jpeg' ) {
					
					$_POST['image'] = time() . $this->attachments->clean_filename( $_FILES['image']['name'] );
					
					$path = str_replace( 'admin', '', getcwd() ) . '/images/uploads/';
					
					//echo( $path );
					
					move_uploaded_file( $_FILES['image']['tmp_name'], $path . 'tmp/' . $_POST['image'] ); 
					
					$resizeObj = new resize( $path . 'tmp/' . $_POST['image'] );
					$resizeObj -> resizeImage( 300, 473, 'landscape' );
					$resizeObj -> saveImage( $path . 'resized/' . $_POST['image'], 100 );
									
					$resizeObj = new resize( $path . 'tmp/' . $_POST['image'] );
					$resizeObj -> resizeImage( 100, 100, 'crop' );
					$resizeObj -> saveImage( $path . 'thumbnails/' . $_POST['image'], 100 );
				
				} else {
					
					$this->message = 'Invalid type of image. Please use a jpg.';
					$_GET['id'] = $_POST['id'];
					
					echo( $this->message );
					
				}
			
			}
			
			if( $this->patients->save( $_POST ) ) {
			
				if( !$_POST['id'] ) {
				
					$_POST['id'] = mysql_insert_id();
				
				}
																				
				header( 'Location: /patients' );
				exit;
			
			}
		
		}
		
		if( $_GET['id'] ) {
		
			$this->result = $this->patients->retrieve( 'one', '*', ' where id = ' . $_GET['id'] ); 
															
		}
		
	}

	
	public function schedule_appointment() {
	
		$this->referral_info = $this->referrals->retrieve('one','*',' where id = ' . $_GET['referral']);
		
		//print_r( $this->referral_info );
	
		$hours = $this->doctor_hours->retrieve( 'all', '*', ' where doctor_id = ' . $this->referral_info['referred_to'] ); 
		
		if( is_array( $hours ) ) {
			
			foreach( $hours as $hour ) {
				
				$this->result[ $hour['day_of_week'] ][] = $this->appointment_hours[ $hour['starting_hour'] ] . ' - ' . $this->appointment_hours[ $hour['ending_hour'] ];
				
			}
			
		}
	
	}
	
	public function _choose_appointment_day() {
	
		$this->referral_info = $this->referrals->retrieve('one','*',' where id = ' . $_GET['referral']);
	
		$doctor_info = $this->doctors->retrieve( 'one', '*', ' where id = ' . $_GET['doctor'] ); 
		
		$hours = $this->doctor_hours->retrieve( 'all', '*', ' where doctor_id = ' . $_GET['doctor'] . " and day_of_week='" . date( 'l', strtotime( $_GET['day'] ) ) . "' order by starting_hour asc" ); 
		
		$this->time_slots = array();
		
		if( is_array( $hours ) ) {
			
			foreach( $hours as $hour ) {
			
				//$length = $hour['ending_hour'] - $hour['starting_hour'];
				
				//$appointments = floor( ( $length * 60 ) / $doctor_info['appointment_length'] );
				
				$i = $hour['starting_hour'];
				
				while( $i < $hour['ending_hour'] ) {
				
					//echo( $i . ': ' . $i . '<br>');
					
					$this->time_slots[ "$i" ] = AppFunctions::formatTime( $i );
					
					$i += ( $doctor_info['appointment_length'] / 60 );
					
				}
				
			}
			
		}
		
	}
	
	public function doctor_hour() {
	
		if( count( $_POST ) > 0 ) {
								
			if( $this->doctor_hours->save( $_POST ) ) {
			
				if( !$_POST['id'] ) {
				
					$_POST['id'] = mysql_insert_id();
				
				}
																						
				header( 'Location: /doctor?id=' . $_POST['doctor_id'] );
				exit;
			
			}
		
		}
		
		if( $_GET['id'] ) {
		
			$this->result = $this->doctor_hours->retrieve( 'one', '*', ' where id = ' . $_GET['id'] ); 
			
			$_GET['doctor_id'] = $this->result['doctor_id'];
														
		}
		
	}
	
	public function signup() {
	
		if( count( $_POST ) > 0 ) {
								
			if( $this->users->save( $_POST ) ) {
			
				if( !$_POST['id'] ) {
				
					$_POST['id'] = mysql_insert_id();
				
				}
																						
				$this->login();
			
			} else {
				
				unset( $_POST['password'] );
				unset( $_POST['password_confirm'] );
				
				$this->message = $this->users->error;
				
			}
		
		}
		
	}
					
	public function user() {
	
		// do restriction for office user
		if( $_SESSION['logged_in_user']['office_id'] ) {
			
			$_GET['id'] = $_SESSION['logged_in_user']['id'];
			
		}
	
		if( count( $_POST ) > 0 ) {
			
			if( $this->users->save( $_POST ) ) {
			
				if( !$_POST['id'] ) {
				
					$_POST['id'] = mysql_insert_id();
				
				}
																						
				header( 'Location: /users' );
				exit;
			
			} else {
				
				$this->message = $this->users->error;
				
			}
		
		}
		
		if( $_GET['id'] ) {
		
			$this->result = $this->users->retrieve( 'one', '*', ' where id = ' . $_GET['id'] ); 
														
		}
		
	}
	
	public function invite() {
	
		if( count( $_POST ) > 0 ) {
					
			if( $this->invitations->save( $_POST ) ) {
			
				if( !$_POST['id'] ) {
					
					$this->success = "Your invitation has been sent.";
				
					$_POST['id'] = mysql_insert_id();
					unset( $_POST['email'] );
				
				}	
				
			}
		
		}
		
		if( $_GET['doctor'] ) {
		
			$this->doctor = $this->doctors->retrieve( 'one', '*', ' where id = ' . $_GET['doctor'] ); 
			$this->office = $this->offices->retrieve( 'one', '*', ' where id = ' . $this->doctor['office_id'] ); 
														
		}
		
	}
	
	public function _update_favorites() {
	
		if( $_POST['doctor'] ) {
		
			$favorite = $this->favorites->retrieve( 'one', '*', ' where doctor_id = ' . $_POST['doctor'] . ' and office_id = ' . $_SESSION['logged_in_user']['office_id'] ); 
			
			// it exists, so remove it
			if( $favorite['id'] ) {
				
				$this->favorites->remove( $favorite['id'] );
				
			} else { // it doesn't, so add it
				
				if( $this->favorites->save( array( 'doctor_id' => $_POST['doctor'], 'office_id' => $_SESSION['logged_in_user']['office_id'] ) ) ) {
					
					// do something, if necessary
					
				}
				
			}
														
		}
		
		exit;
	
	}
	
	public function save_appointment() {
	
		if( $_POST['day'] && $_POST['time_slot'] ) {
			
			$data = array( 'id' => $_POST['referral'], 'referral_status' => 2 );
		
			$data['appointment'] = date( 'm/d/Y', strtotime( $_POST['day'] ) ) . ' ' . AppFunctions::formatTime( $_POST['time_slot'] );
			
			$this->referrals->save( $data );
			
		}
		
		header( 'Location: /view_referral?id=' . $_POST['referral'] );
		exit;
		
	}
	
	public function referral() {
		
		if( count( $_POST ) > 0 ) {
		
			if( !$this->message ) {
				
				if( $this->referrals->save( $_POST ) ) {
				
					if( !$_POST['id'] ) {
					
						$_POST['id'] = mysql_insert_id();
												
											
					}
						
					header( 'Location: /patients' );
					exit;
								
				}
			
			}
		
		}
		
		// if this is a new referral
		if( $_GET['doctor'] ) {
		
			$this->doctor_info = $this->doctors->retrieve( 'one', '*', ' where id = ' . $_GET['doctor'] ); 
			
			$this->doctor_questions = $this->doctor_referral_questions->retrieve( 'all', '*', ' where doctor_id = ' . $_GET['doctor'] ); 
			
			$this->doctor_locations = $this->office_locations->retrieve('all','*', ' where office_id = ' . $this->doctor_info['office_id'] );
			
			//print_r( $this->doctor_questions );
			
			$this->dr_q_list = array();
		
			foreach( $this->doctor_questions as $q ) {
				
				$this->dr_q_list[ $q['referral_question_id'] ] = $this->question_list[ $q['referral_question_id'] ];
				
			}
														
		}
		
		if( $_GET['id'] ) {
		
			$this->result = $this->referrals->retrieve( 'one', '*', ' where id = ' . $_GET['id'] ); 
			
			// set the doctor info
			$this->doctor_info = $this->doctors->retrieve( 'one', '*', ' where id = ' . $this->result['referred_to'] );
			
			$this->doctor_questions = $this->doctor_referral_questions->retrieve( 'all', '*', ' where doctor_id = ' . $this->result['referred_to'] ); 
			
			$this->doctor_locations = $this->office_locations->retrieve('all','*', ' where office_id = ' . $this->doctor_info['office_id'] );
			
			$this->dr_q_list = array();
		
			foreach( $this->doctor_questions as $q ) {
				
				$this->dr_q_list[ $q['referral_question_id'] ] = $this->question_list[ $q['referral_question_id'] ];
				
			}
			
			$this->result['attachments'] = $this->attachments->retrieve( 'all', '*', ' where referral_id = ' . $_GET['id'] ); 
														
		}
	
	}
	
	public function email_new_referral() {
				
		if( $_GET['id'] ) {
		
			$this->result = $this->referrals->retrieve( 'one', '*', ' where id = ' . $_GET['id'] ); 
			
			$this->doctor_questions = $this->doctor_referral_questions->retrieve( 'all', '*', ' where doctor_id = ' . $this->result['referred_to'] ); 
			
			$this->dr_q_list = array();
		
			foreach( $this->doctor_questions as $q ) {
				
				$this->dr_q_list[ $q['referral_question_id'] ] = $this->question_list[ $q['referral_question_id'] ];
				
			}
														
		} else {
			
				echo( 'Invalid ID' );
				exit;
				
		}
		
	
	}
	
	public function referral_email() {
	
		if( $_GET['id'] ) {
		
			$this->result = $this->referrals->retrieve( 'one', '*', ' where id = ' . $_GET['id'] );
			
			$this->doctor_info = $this->doctors->retrieve( 'one', '*', ' where id = ' . $this->result['referred_to'] ); 
			
			$this->office_info = $this->offices->retrieve( 'one', '*', ' where id = ' . $this->doctor_info['office_id'] );  
			
			$this->doctor_questions = $this->doctor_referral_questions->retrieve( 'all', '*', ' where doctor_id = ' . $this->result['referred_to'] ); 
			
			$this->dr_q_list = array();
		
			foreach( $this->doctor_questions as $q ) {
				
				$this->dr_q_list[ $q['referral_question_id'] ] = $this->question_list[ $q['referral_question_id'] ];
				
			}		
			
			$this->result['attachments'] = $this->attachments->retrieve( 'all', '*', ' where referral_id = ' . $_GET['id'] );
			
			if( $_GET['comment'] ) {
			
				$this->comment_info = $this->comments->retrieve( 'one', '*', ' where id = ' . $_GET['comment'] ); 	
			
				$this->comment_attachments = array();
			
				foreach( $this->result['attachments'] as $a ) {
					
					if( $a['comment_id'] ) {
						
						$this->comment_attachments[ $a['comment_id'] ][] = $a;
						
					}
					
				}
				
			}
			
			
														
		}
	
	}
	
	public function email_patient() {
	
		if( $_GET['id'] ) {
		
			$this->result = $this->referrals->retrieve( 'one', '*', ' where id = ' . $_GET['id'] );
			
			$this->doctor_info = $this->doctors->retrieve( 'one', '*', ' where id = ' . $this->result['referred_to'] ); 
			
			$this->office_info = $this->offices->retrieve( 'one', '*', ' where id = ' . $this->doctor_info['office_id'] );  
			
			$this->office_location_info = $this->office_locations->retrieve( 'one', '*', ' where id = ' . $this->result['referred_to_location'] );
			
			$this->doctor_questions = $this->doctor_referral_questions->retrieve( 'all', '*', ' where doctor_id = ' . $this->result['referred_to'] ); 
			
			$this->dr_q_list = array();
		
			foreach( $this->doctor_questions as $q ) {
				
				$this->dr_q_list[ $q['referral_question_id'] ] = $this->question_list[ $q['referral_question_id'] ];
				
			}		
			
			$this->result['attachments'] = $this->attachments->retrieve( 'all', '*', ' where referral_id = ' . $_GET['id'] );
			
			if( $_GET['comment'] ) {
			
				$this->comment_info = $this->comments->retrieve( 'one', '*', ' where id = ' . $_GET['comment'] ); 	
			
				$this->comment_attachments = array();
			
				foreach( $this->result['attachments'] as $a ) {
					
					if( $a['comment_id'] ) {
						
						$this->comment_attachments[ $a['comment_id'] ][] = $a;
						
					}
					
				}
				
			}
			
			
														
		}
	
	}
	
	public function view_referral() {
		
		if( count( $_POST ) > 0 ) {
			
			if( !$this->message ) {
				
				if( $this->comments->save( $_POST ) ) {
																							
					header( 'Location: /view_referral?id=' . $_POST['referral_id'] );
					exit;
								
				}
			
			}
		
		}
		
		if( $_GET['id'] ) {
		
			$this->result = $this->referrals->retrieve( 'one', '*', ' where id = ' . $_GET['id'] ); 
			
			$this->preferred_location = $this->office_locations->retrieve('one','*',' where id = ' . $this->result['referred_to_location'] );
			
			$this->doctor_questions = $this->doctor_referral_questions->retrieve( 'all', '*', ' where doctor_id = ' . $this->result['referred_to'] ); 
			
			$this->dr_q_list = array();
		
			foreach( $this->doctor_questions as $q ) {
				
				$this->dr_q_list[ $q['referral_question_id'] ] = $this->question_list[ $q['referral_question_id'] ];
				
			}
			
			$this->comments = $this->comments->retrieve( 'all', '*', ' where referral_id = ' . $this->result['id'] ); 			
			
			$this->result['attachments'] = $this->attachments->retrieve( 'all', '*', ' where referral_id = ' . $_GET['id'] ); 
			
			$this->comment_attachments = array();
			
			foreach( $this->result['attachments'] as $a ) {
				
				if( $a['comment_id'] ) {
					
					$this->comment_attachments[ $a['comment_id'] ][] = $a;
					
				}
				
			}
														
		}
		
		if( $_GET['doctor'] ) {
		
			$this->doctor_info = $this->doctors->retrieve( 'one', '*', ' where id = ' . $_GET['doctor'] ); 
			
			$this->doctor_questions = $this->doctor_referral_questions->retrieve( 'all', '*', ' where doctor_id = ' . $_GET['doctor'] ); 
			
			$this->dr_q_list = array();
		
			foreach( $this->doctor_questions as $q ) {
				
				$this->dr_q_list[ $q['referral_question_id'] ] = $this->question_list[ $q['referral_question_id'] ];
				
			}
														
		}
		
	
	}
			
	public function delete() {
		
		$table = $_GET['model'];
		$this->$table->remove( $_GET['id'] );
		
		if( $_GET['redirect'] ) {
			
			header( 'Location: /' . $_GET['redirect'] );
			
		} else {
			
			header( 'Location: /' . $_GET['model'] );
			
		}
		exit;		
	
	}
	
	public function update_display_order() {
		
		$update_parts = explode( '-order-', $_GET['update_id'] );
		
		$this->db->table = $update_parts[0];
		
		$this->db->save( array( 'id' => str_replace( '-id', '', $update_parts[1] ), 'display_order' => $_GET['order'] ) );
	
		exit;		
	
	}
	
	public function add_to_list() {
		
		$model = $_GET['model'];
		$pair = $_GET['pair'];
		
		unset( $_GET['model'] );
		unset( $_GET['pair'] );
		unset( $_GET['url'] );
		unset( $_GET['ajax'] );
		
		// set the model
		$this->db->table = $model;
		
		$tocheck = array();
		
		// check to see if this one is already in the system
		foreach( $_GET as $field => $value ) {
			
			$tocheck[] = $field . "='" . addslashes( $value ) . "'";
			
			// also check to make sure there is a value for each option
			if( !$value ) {
				
				echo( json_encode( array( 'error' => $field . ' is required.' ) ) );
				exit;
				
			}
			
		}
		
		$already_there = $this->db->retrieve('one','*'," where 1=1 and ( " . implode( ' and ', $tocheck ) . " )");
		
		if( $already_there['id'] ) {
			
			echo( json_encode( array( 'error' => 'The item is already in the database.' ) ) );
			
		} else {
			
			$this->db->save( $_GET );
			
			$new_id = mysql_insert_id();
			
			$new_info = $this->db->retrieve('pair',$pair);
			
			$toreturn = '';
			
			foreach( $new_info as $id => $label ) {
				
				$selected = ( $new_id == $id )?'selected':'';
				
				$toreturn .= "<option value='" . $id . "' " . $selected . ">" . $label . "</option>";
				
			}
			
			echo( json_encode( array( 'data' => $toreturn ) ) );
			
		}
		
		exit;		
	
	}
			
}

?>